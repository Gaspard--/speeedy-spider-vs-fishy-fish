![img](https://static.jam.vg/content/6c2/2/z/3806b.png)

## Usage

*Unix*

1. Clone the project `git clone git@gitlab.com:Gaspard--/speeedy-spider-vs-fishy-fish.git` or [download as zip](https://gitlab.com/Gaspard--/speeedy-spider-vs-fishy-fish/-/archive/master/speeedy-spider-vs-fishy-fish-master.zip)
2. Install the [GLFW](http://www.glfw.org/), the [SFML](https://www.sfml-dev.org/), the [PNG](http://www.libpng.org/pub/png/libpng.html) libraries, and [Vulkan](https://developer.nvidia.com/vulkan-driver)
3. `cd` into the project directory
4. Make the release : `mkdir release && cd release && cmake .. -DCMAKE_BUILD_TYPE=Release && cd .. && make -C release`
5. Launch the game: `./speedy-spider-vs-fishy-fish`
6. Enjoy!

*Windows*

*Coming soon*

## Story

Fishy escaped one more time! It's your job to take him back to his jail.
Every now and again Fishy fish finds a way...but at least we got Speedy spider!

## Gameplay

* Speedy is moving only in circle
* change your rotating direction using left/right or space
* Surf on the lilypads to move further and avoid Fishy's attacks
* Stop Fishy using your bullets
* Enjoy our game :)

## Developers
* [Jakob Kellendonk](https://github.com/Gaspard--)
* [Matthias Rigaud](https://github.com/matthiasrigaud)

## Arts
* Sara Kellendonk [Artstation](https://sasadraw.artstation.com/) && [Insta](https://www.instagram.com/sasadraw.4u/)

## Music
* Jakob Kellendonk [Soundcloud](https://soundcloud.com/gaspard-4) && [Spotify](https://open.spotify.com/artist/4rKKEN0Y6LNjHuHEYEDD97?si=jYYo0YypTtC6zDIUNxUifQ)

## Ludum Dare Page

This game was developed during the LD47.
You can visit our LD47 game page [here](https://ldjam.com/events/ludum-dare/47/speedy-spider-vs-fishy-fish).

## Note

To avoid having people install claws, portions of the code were taken and adpated from it.

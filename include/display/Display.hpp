#pragma once

#include "magma/DisplaySystem.hpp"
#include "magma/Fence.hpp"
#include "magma/RenderPass.hpp"
#include "magma/Buffer.hpp"
#include "magma/BufferView.hpp"
#include "magma/DeviceMemory.hpp"
#include "magma/DynamicBuffer.hpp"
#include "magma/DescriptorSets.hpp"
#include "magma/ShaderModule.hpp"
#include "magma/Framebuffer.hpp"
#include "magma/PipelineLayout.hpp"
#include "magma/Pipeline.hpp"
#include "magma/Image.hpp"
#include "magma/Sampler.hpp"
#include "magma/DescriptorSetLayout.hpp"

#include "RenderMode.hpp"

#include "rider/rider.hpp"

namespace logic
{
  class Logic;
  struct Transform2d;
}

namespace display
{
  struct RenderDataInfos;

  struct FrameUserData;

  struct RenderArea;

  class Image;
  enum class SpriteType;

  struct AnimImageList;
  struct ImageTransform;

  class FramebufferConfig
  {
    struct ImageConfig
    {
      vk::Format format;
      vk::ImageUsageFlags flags;
      magma::ImageView<claws::no_delete> imageView;
    };

    std::vector<ImageConfig> imageConfigs;

  public:
    FramebufferConfig() = default;
    FramebufferConfig(magma::RenderPassCreateInfo const &renderPassCreateInfo);

    FramebufferConfig &setImage(uint32_t index, magma::ImageView<claws::no_delete> imageView) noexcept;

    magma::Framebuffer<> createFrameBuffer(magma::RenderPass<claws::no_delete> renderPass, uint32_t width, uint32_t height, std::vector<Image> &imageStorage) const;
  };

  class Display
  {
    friend struct FrameUserData;

    struct UserData {
      magma::ShaderModule<> vertShaderModule;
      magma::ShaderModule<> positionOnlyVertShaderModule;
      magma::ShaderModule<> fragShaderModule;
      magma::ShaderModule<> fullscreenVertShaderModule;
      magma::ShaderModule<> fullscreenFragShaderModule;
      magma::DescriptorSetLayout<> descriptorSetLayout;
      magma::PipelineLayout<> fullscreenPipelineLayout;
      magma::PipelineLayout<> pipelineLayout;
      magma::CommandPool<> commandPool;
      magma::Semaphore<> imageAvailable;
      magma::Semaphore<> renderDone;
      magma::DynamicBuffer dynamicBuffer;
      magma::ShaderModule<> vertTextureShaderModule;
      magma::ShaderModule<> fragTextureShaderModule;
      magma::DescriptorSetLayout<> pictureDescriptorSetLayout;
      magma::PipelineLayout<> texturePipelineLayout;
      std::vector<AnimImageList> animImageList;
      magma::DescriptorPool<> pictureDescriptorPool;
      magma::DescriptorSets<> pictureDescriptorSets;
      magma::Sampler<> pictureSampler;
      magma::Sampler<> repeatSampler;

      void loadImages(uint32_t queueFamilyIndex);

      UserData(uint32_t selectedQueueFamily);
    };

    struct SwapchainUserData
    {
      magma::RenderPass<> renderPass;
      FramebufferConfig framebufferConfig;
      std::unordered_map<renderMode::RenderMode, magma::Pipeline<>> pipelines;
      magma::Pipeline<> fullscreenPipeline;
      magma::Pipeline<> stencilWritePipeline;
      magma::CommandBufferGroup<magma::PrimaryCommandBuffer> commandBuffers;
      magma::DescriptorPool<> descriptorPool;
      magma::DescriptorSets<> descriptorSets;
      magma::Pipeline<> texturePipeline;
      std::array<float, 2u> dim;

      magma::Pipeline<> createPipeline(magma::Swapchain<claws::no_delete> swapchain, UserData const &userData, renderMode::RenderMode const &renderMode);
      magma::Pipeline<> createTexturePipeline(magma::Swapchain<claws::no_delete> swapchain, UserData const &userData);
      magma::Pipeline<> createPostProcessPipeline(magma::Swapchain<claws::no_delete> swapchain, UserData const &userData);
      magma::Pipeline<> createStencilWritePipeline(magma::Swapchain<claws::no_delete> swapchain, UserData const &userData);

      SwapchainUserData() = default;
      SwapchainUserData(magma::Swapchain<claws::no_delete> swapchain, UserData const &perSurfaceData, uint32_t imageCount);
      SwapchainUserData(SwapchainUserData &&) = default;
      SwapchainUserData &operator=(SwapchainUserData &&other)
      {
	this->~SwapchainUserData();
	new (this)SwapchainUserData(std::move(other));
	return *this;
      };
    };

    magma::Instance instance;
    magma::Surface<> surface;
    magma::Device<> device;
    rider::Bind<magma::getDevice> bindDevice;
    rider::Bind<magma::getPhysicalDevice> bindPhysicalDevice;

    
    vk::Queue queue;

    magma::DisplaySystem<UserData, SwapchainUserData, FrameUserData> displaySystem;

    struct InitInfo
    {
      magma::Instance instance;
      magma::Surface<> surface;
      magma::Device<> device;
      std::optional<rider::Bind<magma::getDevice>> bindDevice;
      std::optional<rider::Bind<magma::getPhysicalDevice>> bindPhysicalDevice;
      uint32_t queueFamilyIndex;
      vk::PhysicalDevice physicalDevice;

      template<class SurfaceProvider>
      InitInfo(std::vector<const char *> &&requiredExtensions, SurfaceProvider surfaceProvider)
	: instance(std::move(requiredExtensions))
	, surface(magma::impl::SurfaceDeleter{instance.vkInstance}, magma::impl::Surface(surfaceProvider(instance)))
      {
	std::tie(physicalDevice, queueFamilyIndex) = instance.selectQueue([this](auto const &queueFamilyProperties, auto const &physicalDevice, auto queueIndex) {
									    return (queueFamilyProperties.queueFlags & vk::QueueFlagBits::eGraphics) && surface.isQueueFamilySuitable(physicalDevice, queueIndex);
									  },
	  [](auto const &, auto const &) { return true; });
	bindPhysicalDevice.emplace(vk::PhysicalDevice(physicalDevice));
	std::cout << "found suitable queue : " << physicalDevice.getProperties().deviceName << ", queue number " << queueFamilyIndex << std::endl;
	{
	  float priority[1]{1.0f};
	  vk::DeviceQueueCreateInfo deviceQueueCreateInfo{{}, queueFamilyIndex, 1, priority};
	  device = magma::Device<>(physicalDevice,
				   std::vector<vk::DeviceQueueCreateInfo>({deviceQueueCreateInfo}),
				   std::vector<char const *>({VK_KHR_SWAPCHAIN_EXTENSION_NAME}));
	  bindDevice.emplace(vk::Device(device));
	}
      }
    };

    explicit Display(InitInfo &&initInfo);

  public:
    template<class SurfaceProvider>
    explicit Display(std::vector<const char *> &&requiredExtensions, SurfaceProvider surfaceProvider)
      : Display(InitInfo(std::move(requiredExtensions), surfaceProvider))
    {
    }

    Display() = delete;
    Display(Display const &) = delete;
    Display(Display &&) = delete;
    ~Display(); // noexcept someday?

    magma::Pipeline<claws::no_delete> getOrCreatePipeline(renderMode::RenderMode const &renderMode);
    void renderFrame(logic::Logic const &logic, std::mutex &lock);
    void loadImages(uint32_t queueFamilyIndex);

  private:
    void recordCommandBuffer(uint32_t index, FrameUserData &frame, logic::Logic const &logic);

    void renderInArea(uint32_t index, FrameUserData &frame, logic::Logic const &logic, magma::RenderPassExecLock &lock, RenderArea const &renderArea);
    void recordTillingImageDisplay(FrameUserData &frame, magma::PrimaryCommandBuffer cmdBuffer, magma::RenderPassExecLock &lock, SpriteType sprite, uint32_t animFrame, logic::Transform2d const &transform, std::array<float, 2> size, std::array<float, 4u> const &mask = {1.0f, 1.0f, 1.0f, 1.0f});
    void recordSingleImageDisplay(FrameUserData &frame, magma::PrimaryCommandBuffer cmdBuffer, magma::RenderPassExecLock &lock, SpriteType sprite, uint32_t animFrame, logic::Transform2d const &transform, std::array<float, 2> size, std::array<float, 4u> const &mask = {1.0f, 1.0f, 1.0f, 1.0f});
    void recordMultiImageDisplay(FrameUserData &frame, magma::PrimaryCommandBuffer cmdBuffer, magma::RenderPassExecLock &lock, SpriteType sprite, uint32_t animFrame, std::vector<ImageTransform> const &imageTransforms, std::array<float, 4u> const &mask = {1.0f, 1.0f, 1.0f, 1.0f});

    void recordImageDisplay(FrameUserData &frame, logic::Logic const &logic, magma::PrimaryCommandBuffer cmdBuffer, magma::RenderPassExecLock &lock, RenderArea const &renderArea);
    RenderDataInfos uploadData(FrameUserData &frame, logic::Logic const &logic, std::vector<std::array<float, 2u>> const &bounds);
  };
}

#pragma once

namespace display
{
  namespace
  {
    vk::ImageAspectFlagBits getAspectFlagsFromFormat(vk::Format format)
    {
      switch (format)
	{
	case vk::Format::eS8Uint:
	  return vk::ImageAspectFlagBits::eStencil;
	case vk::Format::eR32G32B32A32Sfloat:
	case vk::Format::eR8G8B8A8Unorm:
	case vk::Format::eR8G8B8A8Uint:
	case vk::Format::eB8G8R8A8Srgb:
	case vk::Format::eR8G8B8A8Srgb:
	  return vk::ImageAspectFlagBits::eColor;
	default:
	  assert(!"Unknown format");
	  return {};
	}
    }
  }

  class Image
  {
  private:
    magma::DeviceMemory<> imageMemory;
    magma::Image<> image;
    magma::ImageView<> imageView;
    vk::Format format;
    uint32_t width;
    uint32_t height;

  public:
    Image(uint32_t width, uint32_t height, vk::Format format, vk::ImageUsageFlags usageFlags)
      : imageMemory()
      , image([&](){
		auto image(magma::createImage2D(vk::ImageCreateFlagBits::eMutableFormat,
						format,
					        std::array<uint32_t, 2u>{width, height},
						vk::SampleCountFlagBits::e1,
						vk::ImageTiling::eOptimal,
						usageFlags,
						vk::ImageLayout::eUndefined));

		auto memRequirements(magma::getDevice().getImageMemoryRequirements(image));

		this->imageMemory = magma::selectAndCreateDeviceMemory(memRequirements.size,
								       vk::MemoryPropertyFlagBits::eDeviceLocal,
								       memRequirements.memoryTypeBits);
		magma::getDevice().bindImageMemory(image, this->imageMemory, 0);
		return image;
	      }())
      , imageView(magma::createImageView({},
					 image,
					 vk::ImageViewType::e2D,
					 format,
					 {},
					 vk::ImageSubresourceRange{
								   getAspectFlagsFromFormat(format),
								   0,
								   VK_REMAINING_MIP_LEVELS,
								   0,
								   VK_REMAINING_ARRAY_LAYERS
					 }))
      , format(format)
      , width(width)
      , height(height)
    {
    }

    void transition(magma::PrimaryCommandBuffer commandBuffer, vk::PipelineStageFlags srcStageFlags, vk::PipelineStageFlags dstStageFlags, vk::ImageLayout oldLayout, vk::ImageLayout newLayout, vk::AccessFlags srcAccess, vk::AccessFlags destAccess)
    {
      vk::ImageMemoryBarrier barrier =
	{
	 srcAccess,
	 destAccess,
	 oldLayout,
	 newLayout,
	 VK_QUEUE_FAMILY_IGNORED,
	 VK_QUEUE_FAMILY_IGNORED,
	 image,
	 vk::ImageSubresourceRange{
				   getAspectFlagsFromFormat(format),
				   0,
				   VK_REMAINING_MIP_LEVELS,
				   0,
				   VK_REMAINING_ARRAY_LAYERS
	 }
	};
      commandBuffer.pipelineBarrier(srcStageFlags, dstStageFlags, {}, 0, nullptr, barrier);
    }

    void uploadData(magma::PrimaryCommandBuffer commandBuffer, magma::DynamicBuffer &dynBuffer, char const *data, vk::Queue queue)
    {
      auto range = dynBuffer.allocate(width * height * 4);
      {
	auto memory = dynBuffer.getMemory<char[]>(range);

	std::memcpy(&memory[0], data, width * height * 4);
      }
      transition(commandBuffer, vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTransfer, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal, {}, vk::AccessFlagBits::eTransferWrite);

      vk::BufferImageCopy region =
	{
 	 0,
	 0,
	 0,
	 vk::ImageSubresourceLayers{
				    getAspectFlagsFromFormat(format),
				    0,
				    0,
				    1,
	 },
	 {0, 0, 0},
	 {width, height, 1}
	};
      commandBuffer.copyBufferToImage(dynBuffer.getBuffer(range), image, vk::ImageLayout::eTransferDstOptimal, 1, &region);

      transition(commandBuffer, vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal, vk::AccessFlagBits::eTransferWrite, vk::AccessFlagBits::eShaderRead);
      commandBuffer.end();

      vk::PipelineStageFlags wait_dst_stage_mask(vk::PipelineStageFlagBits::eTransfer);
      magma::Fence<> fence(magma::createFence({vk::FenceCreateFlags{}}));

      queue.submit(magma::makeCreateInfo<vk::SubmitInfo>(magma::EmptyList{},
							 &wait_dst_stage_mask,
							 magma::asListRef(commandBuffer.raw()),
							 magma::EmptyList{}),
		   fence);
      magma::getDevice().waitForFences(1, &fence, true, 1000000000);
      commandBuffer.begin(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
    }

    magma::ImageView<claws::no_delete> getView() const noexcept
    {
      return imageView;
    }

    magma::ImageView<> getView(vk::Format format) const noexcept
    {
      return magma::createImageView({},
				    image,
				    vk::ImageViewType::e2D,
				    format,
				    {},
				    vk::ImageSubresourceRange{
				      getAspectFlagsFromFormat(format),
					0,
					VK_REMAINING_MIP_LEVELS,
					0,
					VK_REMAINING_ARRAY_LAYERS
					}
				    );
    }
  };
}

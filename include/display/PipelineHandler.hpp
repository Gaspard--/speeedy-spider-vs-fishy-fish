#pragma once

#include "magma/ShaderModule.hpp"
#include "magma/PipelineLayout.hpp"
#include "magma/RenderPass.hpp"
#include "magma/Pipeline.hpp"

#include "display/RenderMode.hpp"

namespace display
{
  class PipelineHandler
  {
    struct PipelineDesc
    {
      renderMode::RenderMode renderMode; // the render mode we want
      
      // uint8_t vertShader;
      // uint8_t fragShader;
      char const * vertShader;
      char const * fragShader;
      
      
      
#if 0
      bool operator==(PipelineDesc const &other) const
      {
	return renderMode == other.renderMode &&
	  vertShader == other.vertShader &&
	  fragShader == other.fragShader;
	  }
#endif
    };
    
    std::unordered_map<PipelineDesc, magma::Pipeline<>> pipelines;

    uint8_t getShader(char const *name);
    static magma::Pipeline<> createPipeline(PipelineDesc const &desc);

  public:
    void getPipeline(char const *vert, char const *frag, renderMode::RenderMode renderMode);
  };
}

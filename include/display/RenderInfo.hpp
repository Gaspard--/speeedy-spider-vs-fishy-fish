#pragma once

#include <type_traits>
#include <variant>
#include <tuple>
#include <cassert>

#include "claws/utils/type.hpp"
#include "claws/algorithm/constexpr_algorithm.hpp"

#include "logic/Transform2d.hpp"
#include "display/RenderMode.hpp"

using Pos2Color4 = std::tuple<std::array<float, 2>, std::array<float, 4>>;

using AnyFormat = std::variant<Pos2Color4>; // order decides order of rendering

template<class _RenderType, class Shape>
struct ConvexPolygonRenderInfo
{
  using RenderType = _RenderType;

  Shape shape;

  ConvexPolygonRenderInfo() noexcept = default;
  ConvexPolygonRenderInfo(ConvexPolygonRenderInfo const &) = default;
  ConvexPolygonRenderInfo(ConvexPolygonRenderInfo &&) = default;
  ConvexPolygonRenderInfo &operator=(ConvexPolygonRenderInfo const &) = default;
  ConvexPolygonRenderInfo &operator=(ConvexPolygonRenderInfo &&) = default;

  constexpr ConvexPolygonRenderInfo(Shape shape)
    : shape(shape)
  {
  }

  constexpr uint32_t vertexCount() const noexcept
  {
    return static_cast<uint32_t>(shape.size());
  }

  constexpr uint32_t indexCount(renderMode::PrimitiveType primitiveType) const noexcept
  {
    switch (primitiveType)
      {
      case renderMode::PrimitiveType::Line:
	return static_cast<uint32_t>(vertexCount() * 2ul);
      case renderMode::PrimitiveType::Triangle:
	if (!vertexCount())
	  return 0;
	return static_cast<uint32_t>((vertexCount() - 2ul) * 3ul);
      default:
	assert(!"Unhandled render mode!");
      }
  }

  constexpr uint32_t vertexSize() const noexcept
  {
    return static_cast<uint32_t>(vertexCount() * (4 + 2));
  }

  constexpr uint32_t indexSize(renderMode::PrimitiveType primitiveType) const noexcept
  {
    return indexCount(primitiveType);
  }

  template<class It, class IndexIt>
  constexpr void writeData(It it, IndexIt indexIt, unsigned int indexOffset, renderMode::PrimitiveType primitiveType) const noexcept
  {
    for (auto const &corner : shape)
      {
	it = claws::copy(corner.getPosition().begin(),
			 corner.getPosition().end(),
			 it);
	it = claws::copy(corner.getColor().begin(),
			 corner.getColor().end(),
			 it);
      }
    switch (primitiveType)
      {
      case renderMode::PrimitiveType::Line:
	if (shape.size())
	  {
	    for (auto i(0u); i < shape.size() - 1u; ++i)
	      for (auto j : std::array<unsigned int, 2u>{i, i + 1u})
		*indexIt++ = indexOffset + j;
	    *indexIt++ = vertexCount() - 1u + indexOffset;
	    *indexIt++ = indexOffset;
	  }
	break;
      case renderMode::PrimitiveType::Triangle:
	if (shape.size())
	  for (auto i(1u); i < shape.size() - 1u; ++i)
	    for (auto j : std::array<unsigned int, 3u>{0, i, i + 1u})
	      *indexIt++ = indexOffset + j;
	break;
      default:
	assert(!"Unhandled render mode!");
      }
  }
};

template<class RenderType, class Shape>
auto makeConvexPolygonRenderInfo(Shape shape)
{
  return ConvexPolygonRenderInfo<RenderType, Shape>(shape);
}

template<class T>
struct index_returning_container
{
  T max;

  struct iterator
  {
    T t;
    using difference_type = T;
    using reference = T const &;
    using value_type = T;
    using pointer = T const *;
    using iterator_category = std::random_access_iterator_tag;

    iterator() = default;
    iterator(iterator const &) = default;
    iterator(iterator &&) = default;
    iterator& operator=(iterator const &) = default;
    iterator& operator=(iterator &&) = default;

    constexpr T operator*() const noexcept
    {
      return t;
    }

    constexpr T operator[](T index) const noexcept
    {
      return t + index;
    }

    constexpr iterator &operator++() noexcept
    {
      ++t;
      return *this;
    }

    constexpr iterator operator+(T index) const noexcept
    {
      return iterator{t + index};
    }

    constexpr iterator &operator+=(T index) noexcept
    {
      t += index;
      return *this;
    }

    constexpr iterator operator-(T index) const noexcept
    {
      return iterator{t - index};
    }

    constexpr iterator &operator-=(T index) noexcept
    {
      t -= index;
      return *this;
    }

    constexpr bool operator!=(iterator const &other) const noexcept
    {
      return t != other.t;
    }

    constexpr bool operator==(iterator const &other) const noexcept
    {
      return t == other.t;
    }

    constexpr bool operator<(iterator const &other) const noexcept
    {
      return t < other.t;
    }
  };

  constexpr T operator[](T index) const noexcept
  {
    return index;
  }

  constexpr auto begin() const noexcept
  {
    return iterator{0};
  }

  constexpr auto end() const noexcept
  {
    return iterator{max};
  }

  constexpr T size() const noexcept
  {
    return max;
  }
};

template<class PositionShape, class Color>
constexpr auto makeShape(PositionShape shape, Color *color) noexcept
{
  return claws::container_view(index_returning_container<size_t>{size_t(shape.size())}, [shape, color](size_t index) noexcept
			       {
				 struct Point
				 {
				   std::array<float, 2u> position;
				   std::array<float, 4u> color;

				   constexpr auto const &getPosition() const noexcept
				   {
				     return position;
				   }

				   constexpr auto const &getColor() const noexcept
				   {
				     return color;
				   }
				 };
				 return Point{shape[index], *color};
			       });
}

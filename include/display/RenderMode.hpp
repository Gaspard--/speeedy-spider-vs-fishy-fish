#pragma once

#include <cstring>

namespace renderMode
{
  enum class PrimitiveType : uint32_t
    {
     Line = 0,
     Triangle = 1,
    };

  struct RenderMode
  {
    PrimitiveType primitiveType : 1;
    size_t padding : 63;

    RenderMode() noexcept
    {
      std::memset(this, 0, sizeof(*this));
    }

    bool operator==(RenderMode const &other) const noexcept
    {
      return !std::memcmp(this, &other, sizeof(*this));
    }

    size_t hash() const noexcept
    {
      static_assert(sizeof(*this) == sizeof(size_t));
      return reinterpret_cast<size_t const &>(*this);
    }
  };

}

template<>
struct std::hash<renderMode::RenderMode>
{
  size_t operator()(renderMode::RenderMode const &r) const noexcept
  {
    return r.hash();
  }
};

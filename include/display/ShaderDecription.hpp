#pragma once

#include <vector>

namespace display
{
  struct Binding
  {
    unsigned char binding;
    unsigned char size;
    bool floatingPoint;
  };

  struct ShaderDescription
  {
    char const *path;
    std::vector<Binding> inBindings;
    std::vector<Binding> outBindings;
    magma::ShaderModule<> shaderModule;

    ShaderDescription(char const *path, std::vector<Binding> &&inBindings, std::vector<Binding> &&outBindings) noexcept
      : path(path)
      , inBindings(std::move(inBindings))
      , outBindings(std::move(outBindings))
    {
    }
  };

  template<class... Types>
  std::vector<Binding> generateBindings()
  {
    unsigned char i = 0;
    return {Binding{i++, sizeof(Types), std::is_same_v<Types, float>}...};
  }
}

#pragma once

namespace display
{
  enum class SpriteType {
			 Sand,
			 Border,
			 Intro,
			 GameOver,
			 Win,
			 LilyPad01,
			 LilyPad02,
			 LilyPad03,
			 LilyPad04,
			 LilyPad05,
			 Bullet,
			 SpeedySpider,
			 FishyFish,
			 HpBar,
			 Wave,
			 None
  };
}

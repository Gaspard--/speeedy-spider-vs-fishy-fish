#pragma once

#include <memory>
#include <array>
#include <vector>

namespace loaders
{
  typedef std::pair<std::unique_ptr<char[]>, std::array<unsigned int, 2u>> loadedPic;

  loadedPic loadTextureFromBmp(char const *name);
  loadedPic loadTextureFromPng(char const *name);

  std::vector<loadedPic> loadPicsFromDirectory(char const *name);
}

#pragma once

#include <vector>

namespace logic
{
  class Entity;

  struct AI
  {
    AI() = default;
    AI(AI const &) = delete;
    AI(AI &&) = delete;
    virtual ~AI() = default;

    virtual void update(std::vector<Entity *> &entities) = 0;
  };

#if 0
  struct NoAI : public AI
  {
    NoAI() = default;
    NoAI(NoAI const &) = delete;
    NoAI(NoAI &&) = delete;
    virtual ~NoAI() = default;

    virtual void update(std::vector<Entity *> &entities) noexcept override
    {
    }
  };
#endif
}

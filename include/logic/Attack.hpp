#pragma once

#include "logic/Fixture.hpp"
#include "display/RenderInfo.hpp"

namespace logic
{
  struct Attack
  {
    SingleSubViewFixture<Fixture> fixture;
    std::array<float, 4u> color;
    unsigned int timeLeft;
    unsigned int damage;

    auto getFixtureViews() noexcept
    {
      struct FixtureViewList
      {
	Fixture *fixture;

	auto begin() noexcept
	{
	  return fixture;
	}

	auto end() noexcept
	{
	  return fixture + 1;
	}
      };

      return FixtureViewList{&fixture};
    }

    template<class Func>
    void forEachRenderInfo(Func &&func) const
    {
      auto shape(makeShape(fixture.getShape(), &color));

      func(makeConvexPolygonRenderInfo<Pos2Color4>(shape));
    }

    auto const &getTransform() const noexcept
    {
      return fixture.transform;
    }

    void update() noexcept
    {
      --timeLeft;
    }

    bool shouldBeRemoved() noexcept
    {
      return !timeLeft;
    }
  };
}

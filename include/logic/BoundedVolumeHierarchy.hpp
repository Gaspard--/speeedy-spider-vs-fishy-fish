#pragma once

#include <variant>
#include <iostream>

namespace logic
{
  namespace
  {
    constexpr bool constexpr_ternary(bool test, bool first, bool second)
    {
      if (test)
	return first;
      else
	return second;
    }
  }

  template<class Data>
  struct BoundedVolumeHierarchyNode
  {
    using Leaf = Data;
    using Branch = std::array<std::unique_ptr<BoundedVolumeHierarchyNode>, 2>;

    std::variant<Branch, Leaf> data;
    BoundedVolumeHierarchyNode *parent = nullptr;
    BoundingBox boundingBox;

    void dumpTree(uint32_t indent = 0) const
    {
      for (uint32_t i = 0; i < indent; ++i)
	std::cout << " ";
      std::cout << size_t(this) << ":" << std::endl;
      // if (data)
      //    std::cout << "leaf" << std::endl;
      if (auto *branch = std::get_if<Branch>(data))
	{
	  for (auto &child : *branch)
	    child->dumpTree(indent + 2);
	}
    }

    void recalculateBoundingBox() noexcept
    {
      auto &children(std::get<Branch>(data));
      boundingBox = children[0]->boundingBox | children[1]->boundingBox;
      if (parent)
	parent->recalculateBoundingBox();
    }

    bool isLeaf() const noexcept
    {
      return std::holds_alternative<Leaf>(data);
    }

 #if 0
    template<class Func0, class Func1>
    void doFuncIf(Func0 &&branchFunc, Func1 &&leafFunc) const noexcept(noexcept(branchFunc(std::declval<Branch &>())) && noexcept(leafFunc(std::declval<Leaf &>())))
    {
      std::visit([&](auto &_data) noexcept(std::is_same_v<std::decay_t<decltype(_data)>, Branch> ? noexcept(branchFunc(std::declval<Branch &>())) : noexcept(leafFunc(std::declval<Leaf &>())))
		 {
		   if constexpr(std::is_same_v<std::decay_t<decltype(_data)>, Branch>) {
		     static_assert(std::is_same_v<std::decay_t<decltype(_data)>, Branch>);
		     branchFunc(_data);
		   } else {
		     leafFunc(_data);
		   }
		 }, data);

    }
#endif

    template<class Func0, class Func1>
    void doFuncIf(Func0 &&branchFunc, Func1 &&leafFunc) const noexcept(noexcept(branchFunc(std::declval<Branch const &>())) && noexcept(leafFunc(std::declval<Leaf const &>())))
    {
      // removed noexcept spec because clang gets confused
      // noexcept(std::is_same_v<std::decay_t<decltype(_data)>, Branch> ? noexcept(branchFunc(std::declval<Branch const &>())) : noexcept(leafFunc(std::declval<Leaf const &>())))
      std::visit([&](auto const &_data)
		 {
		   if constexpr(std::is_same_v<std::decay_t<decltype(_data)>, Branch>) {
		     static_assert(std::is_same_v<std::decay_t<decltype(_data)>, Branch>);
		     branchFunc(_data);
		   } else {
		     leafFunc(_data);
		   }
		 }, data);

    }

    template<class Func>
    void forEachCollision(BoundingBox const &boundingBox, Func &&func) const noexcept(noexcept(func(std::declval<Leaf const &>())))
    {
      for (auto i : {0, 1})
	{
	  if (this->boundingBox.getMin()[i] > boundingBox.getMax()[i] ||
	      this->boundingBox.getMax()[i] < boundingBox.getMin()[i])
	    return ;
	}
      doFuncIf([&](auto &_data) noexcept(noexcept(func(std::declval<Leaf const &>())))
	       {
		 static_assert(std::is_same_v<std::decay_t<decltype(_data)>, Branch>);
		 for (auto &child : _data)
		   child->forEachCollision(boundingBox, std::forward<Func>(func));
	       },
	       [&](auto &_data) noexcept(noexcept(func(std::declval<Leaf const &>())))
	       {
		 static_assert(std::is_same_v<std::decay_t<decltype(_data)>, Leaf>);
		 func(_data);
	       });
    }

    template<class Func>
    void forEachInnerCollision_impl(BoundedVolumeHierarchyNode const &other, Func &&func) const noexcept(noexcept(func(std::declval<Leaf const &>(), std::declval<Leaf const &>())))
    {
      for (auto i : {0, 1})
	{
	  if (this->boundingBox.getMin()[i] > other.boundingBox.getMax()[i] ||
	      this->boundingBox.getMax()[i] < other.boundingBox.getMin()[i])
	    return ;
	}
      if (auto *branch = std::get_if<Branch>(&data))
	{
	  for (auto &child : *branch)
	    child->forEachInnerCollision_impl(other, std::forward<Func>(func));
	}
      else if (other.isLeaf())
	{
	  func(std::get<Leaf>(data), std::get<Leaf>(other.data));
	  return ;
	}
      else
	{
	  other.forEachCollision(this->boundingBox, [this, &func](auto const &otherData) noexcept(noexcept(func(otherData, otherData)))
				 {
				   func(std::get<Leaf>(data), otherData);
				 });
	  other.forEachInnerCollision_impl(*this, func);
	}
    }

    template<class Func>
    void forEachInnerCollision(Func &&func) const noexcept(noexcept(func(std::declval<Leaf const &>(), std::declval<Leaf const &>())))
    {
      if (auto *branch = std::get_if<Branch>(&data))
	{
	  auto &children(*branch);
	  children[0]->forEachInnerCollision_impl(*children[1], std::forward<Func>(func));
	  for (auto &child : children)
	    child->forEachInnerCollision(std::forward<Func>(func));
	}
    }
  };

  template<class Data>
  class BoundedVolumeHierarchyTree
  {
    using Branch = typename BoundedVolumeHierarchyNode<Data>::Branch;
    std::unique_ptr<BoundedVolumeHierarchyNode<Data>> root;

    BoundedVolumeHierarchyNode<Data> *addChild(std::unique_ptr<BoundedVolumeHierarchyNode<Data>> &dest, std::unique_ptr<BoundedVolumeHierarchyNode<Data>> &&newNode)
    {
      if (auto *branch = std::get_if<Branch>(&dest->data)) // we're not a leaf node, check if node should be inserted in a child instead
	{
	  auto &children(*branch);
	  std::unique_ptr<BoundedVolumeHierarchyNode<Data>> *bestChild = nullptr;
	  // our goal is to minimise area
	  float growth = area(newNode->boundingBox) + area(children[0]->boundingBox | children[1]->boundingBox);
	  for (auto i : {0, 1})
	    {
	      float val = area(children[i]->boundingBox | newNode->boundingBox) + area(children[!i]->boundingBox);

	      if (val < growth) {
		bestChild = &children[i];
		growth = val;
	      }
	    }
	  if (bestChild)
	    {
	      dest->boundingBox |= newNode->boundingBox;
	      return addChild(*bestChild, std::move(newNode)); // tail recursion
	    }
	}
      {

	///
	///  parent           parent		|
	///     \                \		|
	///     dest          newParent		|
	///                ->    / \		|
	///  other            dest  other	|
	///
	/// Note : we swap dest and newParent in the middle of th operation so the actual final diagramme is
	///       parent	|
	///          \		|
	///          dest	|
	///          / \	|
	///  newParent other	|
	///

	auto newParent = allocNode();

	std::swap(newParent->parent, dest->parent);
	newParent.swap(dest);

	dest->data.template emplace<Branch>
	  (Branch({std::move(newParent), // we swapped
		   std::move(newNode)})
	    );
	auto &children(std::get<Branch>(dest->data));
      	dest->boundingBox = children[0]->boundingBox | children[1]->boundingBox;
	for (auto &child : children)
	  child->parent = dest.get();
	return children[1].get();
      }
    }

    std::vector<std::unique_ptr<BoundedVolumeHierarchyNode<Data>>> recycleList;

    void deleteNode(std::unique_ptr<BoundedVolumeHierarchyNode<Data>> &&toRecycle)
    {
      toRecycle->~BoundedVolumeHierarchyNode<Data>();
      recycleList.emplace_back(std::move(toRecycle));
    }

    template<class... Params>
    std::unique_ptr<BoundedVolumeHierarchyNode<Data>> allocNode(Params &&... params)
    {
      if (recycleList.empty())
	{
	  return std::make_unique<BoundedVolumeHierarchyNode<Data>>(std::forward<Params>(params)...);
	}
      else
	{
	  std::unique_ptr<BoundedVolumeHierarchyNode<Data>> result = std::move(recycleList.back());

	  recycleList.pop_back();
	  new (result.get()) BoundedVolumeHierarchyNode<Data>(std::forward<Params>(params)...);
	  return result;
	}
    }

  public:
    template<class _Data>
    BoundedVolumeHierarchyNode<Data> *addChild(BoundingBox boundingBox, _Data &&data, BoundedVolumeHierarchyNode<Data> *hint = nullptr)
    {
      if (!root)
	{
	  root.reset(new BoundedVolumeHierarchyNode<Data>{{std::forward<_Data>(data)}, nullptr, boundingBox});
	  return root.get();
	}
      else
	{
	  if (hint)
	    {
	      while (hint != root.get())
		{
		  for (int i : {0, 1})
		    {
		      if (hint->boundingBox.getMin()[i] <= boundingBox.getMin()[i] ||
			  hint->boundingBox.getMax()[i] >= boundingBox.getMax()[i])
			{
			  auto newNode = allocNode(BoundedVolumeHierarchyNode<Data>{{std::forward<_Data>(data)}, nullptr, boundingBox});
			  // return addChild(hint, allocNode(BoundedVolumeHierarchyNode<Data>{{std::forward<_Data>(data)}, nullptr, boundingBox}));
			  auto *branch = std::get_if<Branch>(&hint->data);

			  assert(branch); // we're not a leaf node, check if node should be inserted in a child instead
			  auto &children(*branch);
			  std::unique_ptr<BoundedVolumeHierarchyNode<Data>> *bestChild = nullptr;
			  // our goal is to minimise area
			  float growth = area(newNode->boundingBox) + area(children[0]->boundingBox | children[1]->boundingBox);
			  for (auto i : {0, 1})
			    {
			      float val = area(children[i]->boundingBox | newNode->boundingBox) + area(children[!i]->boundingBox);

			      if (val < growth) {
				bestChild = &children[i];
				growth = val;
			      }
			    }
			  if (bestChild)
			    {
			      return addChild(*bestChild, std::move(newNode));
			    }
			}
		    }
		  hint = hint->parent;
		}
	    }
	  return addChild(root, allocNode(BoundedVolumeHierarchyNode<Data>{{std::forward<_Data>(data)}, nullptr, boundingBox}));
	}
    }

    ///      parent          other      ///
    ///       / \      ->               ///
    ///    this  other                  ///
    void remove(BoundedVolumeHierarchyNode<Data> *node) noexcept
    {
      if (!node->parent)
	root.reset(); // this is the last node
      else
	{
	  auto *parent = node->parent;
	  auto &parentChildren(std::get<Branch>(parent->data));
	  auto index = parentChildren[1].get() == node;

	  deleteNode(std::move(parentChildren[index]));
	  if (!node->parent->parent)
	    {
	      std::unique_ptr<BoundedVolumeHierarchyNode<Data>> other(std::move(parentChildren[!index])); // avoid potential use-after-free situation

	      other->parent = nullptr;
	      deleteNode(std::move(root));
	      root = std::move(other);
	    }
	  else
	    {
	      std::unique_ptr<BoundedVolumeHierarchyNode<Data>> other(std::move(parentChildren[!index])); // avoid potential use-after-free situation

	      auto &parentParentChildren(std::get<Branch>(parent->parent->data));
	      auto parentIndex = parentParentChildren[1].get() == node->parent;

	      other->parent = parent->parent;
	      deleteNode(std::move(parentParentChildren[parentIndex]));
	      parentParentChildren[parentIndex] = std::move(other);
	      parent->parent->recalculateBoundingBox();
	    }
	}
    }

    template<class Func>
    void forEachInnerCollision(Func &&func)
    {
      if (root)
	root->forEachInnerCollision(std::forward<Func>(func));
      return ;
    }

    template<class Func>
    void forEachCollision(BoundingBox const &boundingBox, Func &&func) const
    {
      if (root)
	root->forEachCollision(boundingBox, std::forward<Func>(func));
      return ;
    }

    /// Used for debug
    template<class Func>
    void forEachBoundingBox(BoundedVolumeHierarchyNode<Data> *node, Func &&func) const
    {
      if (!node)
	return ;
      func(node->boundingBox);
      if (auto *branch = std::get_if<Branch>(&node->data))
	for (auto &child : *branch)
	  {
	    assert(child);
	    forEachBoundingBox(child.get(), std::forward<Func>(func));
	  }
    }

    /// Used for debug
    template<class Func>
    void forEachBoundingBox(Func &&func) const
    {
      forEachBoundingBox(root.get(), std::forward<Func>(func));
    }

  };
}

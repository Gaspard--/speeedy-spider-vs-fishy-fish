#pragma once
#include "ComponentContainer.hpp"

namespace logic
{
  //
  // this component allow an entity to be recognize as a specific entity
  struct BubbleComponent : public SingleEntityComponent<true>
  {
    float time = 0.0f;

    BubbleComponent(Entity *entity)
    : SingleEntityComponent{entity}
    {}

    void update(TaggedIndex<BubbleComponent>);
  };
}

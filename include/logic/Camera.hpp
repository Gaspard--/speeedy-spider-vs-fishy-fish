#pragma once

#include <array>

struct Camera
{
  std::array<float, 2u> offset;
  std::array<float, 2u> rotation;
};

#pragma once

#include <cassert>

#include <claws/iterator/circular_iterator.hpp>

#include "Fixture.hpp"
#include "CollisionResult.hpp"

#include <iostream>

namespace logic
{

  // note that f(a, b) != f(b, a)
  // There's collision iff f(a, b) && f(b, a);
  template<class FixtureA, class FixtureB>
  inline bool checkCollisionImpl(FixtureA const &lh, FixtureB const &rh) noexcept
  {
    for (auto const [prev, current] : lh.getEdges())
      {
        auto const projectionDir(octogonal(current - prev));
        auto const minA(claws::scalar(projectionDir, current));

        for (auto const &corner : rh.getShape())
          if (claws::scalar(projectionDir, corner) > minA)
            goto nope;
        return false;
      nope:;
      }
    return true;
  }

  template<class FixtureA, class FixtureB>
  inline bool checkCollision(FixtureA const &lh, FixtureB const &rh) noexcept
  {
    return checkCollisionImpl(lh, rh) && checkCollisionImpl(rh, lh);
  }

  // note that f(a, b) != f(b, a)
  // There's collision iff f(a, b) && f(b, a);
  template<class FixtureA, class FixtureB, class Result>
  inline void minDisplacement(FixtureA const &lh, FixtureB const &rh, Result &result) noexcept
  {
    for (auto it(lh.getEdges().begin()); it != lh.getEdges().end(); ++it)
      {
	auto const [prev, current] = *it;

	std::array<float, 2u> const projectionDir(octogonal(current - prev));
        float const minA(claws::scalar(projectionDir, current));
        float maxDiff(0.0f);
	auto bestPoint(rh.getShape().end());

        for (auto rhIt(rh.getShape().begin()); rhIt != rh.getShape().end(); ++rhIt)
          {
            float val(claws::scalar(projectionDir, *rhIt) - minA);

            if (val >= maxDiff)
              {
                bestPoint = rhIt;
                maxDiff = val;
              }
          }
        maxDiff /= claws::length2(projectionDir);
	assert(bestPoint != rh.getShape().end());
        if (claws::length2(result.displacement) > claws::length2(projectionDir * maxDiff))
	  {
	    result.point = *bestPoint;
	    result.setDisplacement(-projectionDir * maxDiff);
	    result.setWhich(0);
	    if constexpr (!std::is_same_v<typename Result::LhInfo, None>)
	      result.getLhInfo() = lh.getInfo(it);
	    if constexpr (!std::is_same_v<typename Result::RhInfo, None>)
	      result.getRhInfo() = rh.getInfo(bestPoint);
	  }
      }
  }

  namespace impl
  {
    template<class T, class = void>
    struct GetInfoType
    {
      using type = None;
    };

    template<class T>
    struct GetInfoType<T, decltype(std::declval<T>().getInfo(std::declval<T>().getShape().begin()), void(0))>
    {
      using type = decltype(std::declval<T>().getInfo(std::declval<T>().getShape().begin()));
    };
  }

  template<class T, class = void>
  struct GetInfoType
  {
    using type = typename impl::GetInfoType<T>::type;
  };

  template<class T>
  using GetInfoType_t = typename GetInfoType<T>::type;

  // transform is applied to b, the second argument
  // return the smallest displacement necessary to push b out off a, or nothing if b is not in a
  // quick proof for the applied transgforms:
  // lh ^ rh = (lh.getShape() + lh.transform) ^ (rh.getShape() + rh.transform) = (lh.getShape()) ^ (rh.getShape() + rh.transform - lh.transform)
  template<class FixtureA, class FixtureB>
  inline auto minDisplacement(FixtureA const &lh, FixtureB const &rh)
  {
    CollisionResult<GetInfoType_t<FixtureA>, GetInfoType_t<FixtureB>> result;

    minDisplacement(rh, lh, result.inverseView());
    minDisplacement(lh, rh, result);
    return result;
  }

  template<class FixtureA, class FixtureB, class CollisionResult>
  inline void collisionResponse(FixtureA &a, FixtureB &b, CollisionResult &&collision) noexcept
  {
    constexpr float const bounciness(0.5f);
    auto const normal(collision.displacement / std::sqrt(claws::length2(collision.displacement)));
    a.transform.position -= (collision.displacement * a.getInverseMass()) / (a.getInverseMass() + b.getInverseMass()) * 0.5f;
    b.transform.position += (collision.displacement * b.getInverseMass()) / (a.getInverseMass() + b.getInverseMass()) * 0.5f;

    if (collision.getWhich())
      collision.point -= (collision.displacement * a.getInverseMass()) / (a.getInverseMass() + b.getInverseMass()) * 0.5f;
    else
      collision.point += (collision.displacement * b.getInverseMass()) / (a.getInverseMass() + b.getInverseMass()) * 0.5f;
    auto const relativeSpeed(claws::scalar(b.getSpeedAt(collision.point) - a.getSpeedAt(collision.point), normal));
    collision.displacement = {0.0f, 0.0f};
    if (relativeSpeed < 0.0f)
      {
	auto const pointOffsetA(collision.point - a.transform.position); // m
	auto const addedA(cross(pointOffsetA, normal) * cross(pointOffsetA, normal) * a.getInverseAngularMass()); // m / (g * m ^ 2) * m = 1 / g

	auto const pointOffsetB(collision.point - b.transform.position);
	auto const addedB(cross(pointOffsetB, normal) * cross(pointOffsetB, normal) * b.getInverseAngularMass());

	auto const j((-(1.0f + bounciness) * relativeSpeed) / (a.getInverseMass() + b.getInverseMass() + addedA + addedB)); // m / s / (1 / g + 1 / g + 1 / g) = m * g / s

	auto const impulse(normal * j);

	float energyPreHit = a.getEnergy() + b.getEnergy();
	if constexpr (std::is_same_v<typename std::decay_t<CollisionResult>::LhInfo, None>)
	  a.applyImpulseAt(collision.point, -impulse);
	else
	  a.applyImpulseAt(collision.point, -impulse, collision.getLhInfo(), !collision.getWhich());
	if constexpr (std::is_same_v<typename std::decay_t<CollisionResult>::RhInfo, None>)
	  b.applyImpulseAt(collision.point, impulse);
	else
	  b.applyImpulseAt(collision.point, impulse, collision.getRhInfo(), collision.getWhich());
	float energyPostHit = a.getEnergy() + b.getEnergy();
	float delta = (energyPostHit - energyPreHit) / energyPreHit;

	if (delta > 0.05f)
	  std::cout << "WARNING: Enregy change! energyPreHit: " << energyPreHit << ", \tenergyPostHit: " << energyPostHit << ", \tdelta:" << delta * 100.0f << "%" << std::endl;
	//assert(std::abs(delta) < 0.05f);
      }
    a.wakeUp();
    b.wakeUp();
  }
}

#pragma once

namespace logic
{
  struct None;

  template<class LhInfo, class RhInfo>
  struct CollisionUserData
  {
    LhInfo lhInfo;
    RhInfo rhInfo;

    auto &getLhInfo() noexcept
    {
      return lhInfo;
    }

    auto &getRhInfo() noexcept
    {
      return rhInfo;
    }

    auto const &getLhInfo() const noexcept
    {
      return lhInfo;
    }

    auto const &getRhInfo() const noexcept
    {
      return rhInfo;
    }
  };

  template<class LhInfo>
  class CollisionUserData<LhInfo, None>
  {
    LhInfo lhInfo;

  public:
    auto &getLhInfo() noexcept
    {
      return lhInfo;
    }

    None &getRhInfo() const = delete;

    auto const &getLhInfo() const noexcept
    {
      return lhInfo;
    }
  };

  template<class RhInfo>
  class CollisionUserData<None, RhInfo>
  {
    RhInfo rhInfo;

  public:
    None &getLhInfo() const = delete;

    auto &getRhInfo() noexcept
    {
      return rhInfo;
    }

    auto const &getRhInfo() const noexcept
    {
      return rhInfo;
    }

  };

  template<>
  struct CollisionUserData<None, None>
  {
    None &getLhInfo() const = delete;
    None &getRhInfo() const = delete;
  };

  template<class _LhInfo, class _RhInfo>
  struct CollisionResult : public CollisionUserData<_LhInfo, _RhInfo>
  {
    using LhInfo = _LhInfo;
    using RhInfo = _RhInfo;
    std::array<float, 2u> point{0.0f, 0.0f};
    std::array<float, 2u> displacement{std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity()};
    bool which;

    using CollisionUserData<LhInfo, RhInfo>::getLhInfo;
    using CollisionUserData<LhInfo, RhInfo>::getRhInfo;
    auto &inverseView();

    void setDisplacement(std::array<float, 2u> displacement) noexcept
    {
      this->displacement = displacement;
    }

    void setWhich([[maybe_unused]] bool which) noexcept
    {
      this->which = which;
    }

    bool getWhich() const noexcept
    {
      return this->which;
    }
  };

  template<class _LhInfo, class _RhInfo>
  struct InvertedCollisionResult : public CollisionResult<_LhInfo, _RhInfo>
  {
    using RhInfo = _LhInfo;
    using LhInfo = _RhInfo;

    void setDisplacement(std::array<float, 2u> displacement) noexcept
    {
      CollisionResult<_LhInfo, _RhInfo>::setDisplacement(-displacement);
    }

    void setWhich(bool which) noexcept
    {
      CollisionResult<_LhInfo, _RhInfo>::setWhich(!which);
    }

    bool getWhich() const noexcept
    {
      return !CollisionResult<_LhInfo, _RhInfo>::which();
    }

    auto &getLhInfo() noexcept
    {
      return CollisionResult<LhInfo, RhInfo>::getRhInfo();
    }

    auto &getRhInfo() noexcept
    {
      return CollisionResult<LhInfo, RhInfo>::getLhInfo();
    }

    auto const &getLhInfo() const noexcept
    {
      return CollisionResult<LhInfo, RhInfo>::getRhInfo();
    }

    auto const &getRhInfo() const noexcept
    {
      return CollisionResult<LhInfo, RhInfo>::getLhInfo();
    }
  };

  template<class _LhInfo, class _RhInfo>
  auto &CollisionResult<_LhInfo, _RhInfo>::inverseView()
  {
    return static_cast<InvertedCollisionResult<LhInfo, RhInfo> &>(*this);
  }
};

#pragma once

#include <array>
#include "logic/ComponentContainer.hpp"

namespace logic
{
  class Entity;

  struct ColorComponent : public SingleEntityComponent<true>
  {
    ColorComponent(Entity *entity, std::array<float, 4u> color) noexcept
      : SingleEntityComponent{entity}
      , color(color)
    {}
      
    std::array<float, 4u> color;
  };
}

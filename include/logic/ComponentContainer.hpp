#pragma once

#include <utility>
#include <vector>
#include <algorithm>
#include <cassert>

#include "logic/Entity.hpp"

namespace logic
{
  template<bool _unique = true>
  struct ExternComponent
  {
    constexpr static bool const unique = _unique;

    template<class A>
    void update(A &&)
    {
    }
  };

  template<bool _unique = true>
  struct SingleEntityComponent : public ExternComponent<_unique>
  {
    Entity *entity;

    SingleEntityComponent(Entity *entity)
      : entity(entity)
    {
    }
  };

  template<class Container, bool _unique = true>
  struct MultiEntityComponent : public ExternComponent<_unique>
  {
    constexpr static bool const unique = _unique;
    Container entities;

    template<class... Params>
    MultiEntityComponent(Params &&... params)
      : entities{std::forward<Params>(params)...}
    {
    }
  };

  template<class _Component>
  class ComponentContainer
  {
  public:
    using Component = _Component;
    constexpr static bool const unique = Component::unique;
  private:
    std::vector<Component> components;

    template<bool _unique, class Func>
    void forEachField(SingleEntityComponent<_unique> &component, Func &&func)
    {
      func(component.entity);
    }

    template<class Container, bool _unique, class Func>
    void forEachField(MultiEntityComponent<Container, _unique> &component, Func &&func)
    {
      for (Entity *entity : component.entities)
	func(entity);
    }

  public:
    ComponentContainer() = default;
    ComponentContainer(ComponentContainer &&) = default;
    ComponentContainer(ComponentContainer const &) = delete;

    void removeComponent(TaggedIndex<Component> component)
    {
      forEachField(*component, [&](auto field) noexcept
			       {
				 if (field)
				   {
				     if constexpr (unique)
				       {
					 field->template unset<Component>();
				       }
				     else
				       {
					 field->removeComponentListElement(component);
				       }
				   }
			       });
      if (end() - 1 == component) // we're removing the last component
	{
	  components.pop_back();
	  return;
	}
      if constexpr (!unique)
	{
	  forEachField(components.back(), [&](Entity *field) noexcept
					  {
					    if (field)
					      {
						field->removeComponentListElement(TaggedIndex<Component>(uint16_t(components.size() - 1)));
					      }
					  });
	}
      {
	using std::swap;

	swap(*component, components.back());
      }
      components.pop_back();
      forEachField(*component, [&](auto field) noexcept
			       {
				 if (field)
				   {
				     if constexpr (unique)
				       {
					 field->update(component);
				       }
				     else
				       {
					 field->addComponentListElement(component);
				       }
				   }
			       });
    }

    template<class... Params>
    TaggedIndex<Component> createComponent(Params &&... params)
    {
      components.emplace_back(std::forward<Params>(params)...);
      assert(uint16_t(components.size()) != 0 && "Can't handle more than 65k components of the same size");

      auto componentIndex = TaggedIndex<Component>(uint16_t(components.size() - 1));
      forEachField(components.back(), [&](Entity *entity) noexcept
				      {
					if constexpr (unique)
				          {
					    entity->set(componentIndex);
					  }
					else
					  {
					    entity->addComponentListElement(componentIndex);
					  }
				      });
      return componentIndex;
    }

    template<class Func>
    void forEach(Func &&func)
    {
      std::for_each(components.begin(), components.end(), func);
    }

    template<class Func>
    void forEach(Func &&func) const
    {
      std::for_each(components.begin(), components.end(), func);
    }

    auto begin() noexcept
    {
      return TaggedIndex<Component>(0);
    }

    auto begin() const noexcept
    {
      return TaggedIndex<Component>(0);
    }

    auto end() noexcept
    {
      return TaggedIndex<Component>(uint16_t(components.size()));
    }

    auto end() const noexcept
    {
      return TaggedIndex<Component>(uint16_t(components.size()));
    }

    Component *data() noexcept
    {
      return components.data();
    }
  };
}

#pragma once

namespace logic
{
  // test struct
  struct HPComponent : public InlineComponent
  {
    uint64_t hp;
    HPComponent(uint64_t hp) noexcept : hp(hp) {};

    void damage(uint64_t amount)
    {
      if (amount > hp)
	hp = 0;
      else
	hp -= amount;
    }
  };

  // test struct
  struct DmgComponent : public InlineComponent
  {
    uint64_t dmg;
    DmgComponent(uint64_t dmg) noexcept : dmg(dmg) {};
  };

  struct JointComponent : public MultiEntityComponent<std::array<Entity *, 2>, false>
  {
    Joint joint;

    JointComponent(Entity *a, Entity *b, std::array<float, 2u> point, float strength) noexcept
      : MultiEntityComponent({a, b})
      , joint(a->get<FixtureComponent>()->fixture,
	      b->get<FixtureComponent>()->fixture,
	      point, strength)
    {
    };

    void update(logic::TaggedIndex<JointComponent>) noexcept
    {
      joint.update(entities[0]->get<FixtureComponent>()->fixture,
		   entities[1]->get<FixtureComponent>()->fixture);
    }
  };

  struct AIComponent : public MultiEntityComponent<std::vector<Entity *>, true>
  {
    std::unique_ptr<AI> aI;

    AIComponent(std::vector<Entity *> &&entities, std::unique_ptr<AI> &&aI) noexcept
      : MultiEntityComponent{std::move(entities)}
      , aI(std::move(aI))
    {
    }

    void update(logic::TaggedIndex<AIComponent>)
    {
      aI->update(entities);
    }
  };

  struct FireComponent : public SingleEntityComponent<true>
  {
    uint32_t permanent : 1;
    uint32_t removeEntity : 1;
    uint32_t sparkles : 1;

    FireComponent(Entity *a, bool permanent = false, bool removeEntity = false, bool sparkles = true) noexcept
      : SingleEntityComponent{a}
      , permanent(permanent)
      , removeEntity(removeEntity)
      , sparkles(sparkles)
    {
    }

    bool update(logic::TaggedIndex<FireComponent>) noexcept
    {
      if (auto hp = entity->get<HPComponent>())
	{
	  hp->damage(1);
	  if (!hp->hp)
	    entity->markForRemoval();
	}
      if (sparkles && (rand() % 30 == 0))
	{
	  auto newEntity = &g_entityManager->createEntity();

	  std::vector<std::array<float, 2>> circle;
	  int edgeCount = 6;

	  for (int i = 0; i < 6; ++i)
	    {
	      float angle = float(i) * 3.14159265358979f * 2.0f / float(edgeCount);
	      circle.push_back({{std::cos(angle) * 0.01f, std::sin(angle) * 0.01f}});
	    }
	  auto fixtureComponent = g_entityManager->createComponent<FixtureComponent>(newEntity, Fixture{std::move(circle), 1.0f});

	  fixtureComponent->fixture.transform = entity->get<FixtureComponent>()->fixture.transform;
	  float angle = float(rand() % 1024);
	  fixtureComponent->fixture.speed = {std::cos(angle) * 0.01f, std::sin(angle) * 0.01f};
	  g_entityManager->createComponent<ColorComponent>(newEntity, std::array<float, 4u>{1.0f, 0.2f, 0.05f, 1.0f});
	  g_entityManager->createComponent<FireComponent>(newEntity, false, true, false);
	}
      if (!permanent && (rand() % 40 == 0))
      	{
	  if (removeEntity)
	    entity->markForRemoval();
      	  return true; // remove component
      	}
      return false;
    }
  };

}

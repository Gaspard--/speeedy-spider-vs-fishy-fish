#pragma once

#include "DeformableFixture.hpp"

namespace logic
{
  struct DeformableEntity
  {
    DeformableFixture fixture;
    std::vector<std::array<float, 4u>> colors;
    std::vector<unsigned int> indices;

    uint32_t vertexCount() const noexcept
    {
      return static_cast<uint32_t>(fixture.nodes.size());
    }

    uint32_t indexCount() const noexcept
    {
      return static_cast<uint32_t>(indices.size());
    }

    uint32_t vertexSize() const noexcept
    {
      return static_cast<uint32_t>(vertexCount() * (fixture.nodes[0].position.size() + colors[0].size()));
    }

    uint32_t indexSize() const noexcept
    {
      return static_cast<uint32_t>(indices.size());
    }

    template<class It, class IndexIt>
    constexpr void writeData(It it, IndexIt indexIt, unsigned int indexOffset) const noexcept
    {
      for (unsigned int i(0u); i < fixture.nodes.size(); ++i)
	{
	  it = claws::copy(fixture.nodes[i].position.begin(), fixture.nodes[i].position.end(), it);
	  it = claws::copy(colors[i].begin(), colors[i].end(), it);
	}
      std::transform(indices.begin(), indices.end(), indexIt, [indexOffset](unsigned int index) constexpr noexcept
		     {
		       return index + indexOffset;
		     });
    }

    auto getRenderInfos() const
    {
      struct RenderInfos
      {
	DeformableEntity const *renderInfo;

	constexpr auto begin() const noexcept
	{
	  return renderInfo;
	}

	constexpr auto end() const noexcept
	{
	  return renderInfo;
	}
      };
      return RenderInfos{this};
    }

    auto getTransform() const noexcept
    {
      return Transform2d{{0.0f, 0.0f}, {1.0f, 0.0f}};
    }

    auto getFixtureViews() const noexcept
    {
      return fixture.getFixtureViews();
    }

    auto getFixtureViews() noexcept
    {
      return fixture.getFixtureViews();
    }
  };
};

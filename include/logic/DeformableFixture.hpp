#pragma once

#include <array>
#include "logic/Fixture.hpp"

namespace logic
{
  struct DeformableFixture
  {
    struct Node
    {
      std::array<float, 2u> position;
      std::array<float, 2u> speed{0.0f, 0.0f};
      float mass{1.0f};
      float friction{0.1f};

      void update() noexcept
      {
        position += speed;
	speed *= 1.0f - friction;
      }
    };

    struct FixtureViewData
    {
      unsigned short firstCornerIndex;
      unsigned short lastCornerIndex;
    };

    BoundingBox boundingBox;
    std::vector<Node> nodes;
    std::vector<unsigned short> corners;
    std::vector<FixtureViewData> views;

    Node &getCorner(unsigned short index)
    {
      return nodes[corners[index]];
    }

    Node const &getCorner(unsigned short index) const
    {
      return nodes[corners[index]];
    }

    struct Joint
    {
      unsigned short source;
      unsigned short dest;
      float length2;

      void apply([[maybe_unused]] Node &source, [[maybe_unused]] Node &dest) const
      {

      }
    };

    std::vector<Joint> joints;

    DeformableFixture() = default;

    DeformableFixture(std::vector<Node> &&nodes, std::vector<unsigned short> &&corners, std::vector<Joint> &&joints) noexcept // vectors are moved
      : nodes(std::move(nodes))
      , corners(std::move(corners))
      , joints(std::move(joints))
    {
      updateBoundingBox();
    }

    template<class Parent>
    struct FixtureView : public FixtureViewData
    {
      Parent *parent;

      auto getShape() const noexcept
      {
	struct Func
	{
	  Parent *parent;

	  decltype(auto) operator()(unsigned int index) noexcept
	  {
	    return parent->nodes[index].position;
	  }

	  decltype(auto) operator()(unsigned int index) const noexcept
	  {
	    return parent->nodes[index].position;
	  }
	};
	return claws::container_view(parent->corners.begin() + firstCornerIndex,
				     parent->corners.begin() + lastCornerIndex,
				     Func{parent});
      }

      template<class It>
      unsigned int getInfo(It it) const noexcept
      {
	return (firstCornerIndex + it - getShape().begin());
      }
    };


    void updateBoundingBox() noexcept;
    void update() noexcept;

    template<class ThisType>
    static auto getFixtureViews(ThisType *thisType)
    {
      return claws::container_view(thisType->views.begin(), thisType->views.end(), [thisType](FixtureViewData const &data)
				   {
				     return FixtureView<ThisType>{data, thisType};
				   });

    }

    auto getFixtureViews() const noexcept
    {
      return getFixtureViews(this);
    }

    auto getFixtureViews() noexcept
    {
      return getFixtureViews(this);
    }

    void applyImpulseAt(std::array<float, 2u> const &point, std::array<float, 2u> const &impulse, unsigned int index, bool which) noexcept;

    BoundingBox const &getBoundingBox() const noexcept
    {
      return boundingBox;
    }
  };
}

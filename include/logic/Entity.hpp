#pragma once

# include <unordered_map>
# include <typeindex>
# include <memory>

namespace logic
{
  template<class A>
  struct TaggedIndex;

  class InlineComponent
  {
  public:
    virtual ~InlineComponent() = default;
  };

  class Entity
  {
  private:
    std::unordered_map<std::type_index, uint16_t> components;
    std::unordered_map<std::type_index, std::vector<uint16_t>> componentLists;
    std::unordered_map<std::type_index, std::unique_ptr<InlineComponent>> inlineComponents;
    bool shouldBeRemoved = false;

  public:
    void markForRemoval() noexcept;
    bool isMarkedForRemoval() const noexcept
    {
      return shouldBeRemoved;
    }

    template<class A>
    A *get() const noexcept
    {
      if constexpr (std::is_base_of_v<InlineComponent, A>)
	{
	  auto it = inlineComponents.find(std::type_index(typeid(A)));

	  if (it != inlineComponents.end())
	    return &static_cast<A &>(*it->second);
	}
      else
        {
          auto it = components.find(std::type_index(typeid(A)));

          if (it != components.end())
            return &*TaggedIndex<A>(it->second);
        }
      return nullptr;
    }

    template<class A, class Func>
    void forComponentList(Func &&func) const noexcept
    {
      auto it = componentLists.find(std::type_index(typeid(A)));

      if (it != componentLists.end())
	for (auto elem : it->second)
	  func(*TaggedIndex<A>(elem));
    }

    template<class A>
    TaggedIndex<A> getComponentListElement(size_t index) const noexcept
    {
      auto it = componentLists.find(std::type_index(typeid(A)));

      assert(it != componentLists.end());
      return TaggedIndex<A>(it->second[index]);
    }

    template<class A>
    uint16_t getComponentListSize() const noexcept
    {
      auto it = componentLists.find(std::type_index(typeid(A)));

      if (it != componentLists.end())
	return uint16_t(it->second.size());
      return 0;
    }


    /// You shouldn't call this manually, it is called by ComponentContainer::createComponent
    template<class A>
    void addComponentListElement(TaggedIndex<A> a)
    {
      componentLists[std::type_index(typeid(A))].emplace_back(a.data);
    }

    /// You shouldn't call this manually, it is called by ComponentContainer::createComponent
    template<class A>
    void removeComponentListElement(TaggedIndex<A> a) noexcept
    {
      auto &list(componentLists.at(std::type_index(typeid(A))));

      list.erase(std::find(list.begin(), list.end(), a.data));
    }


    /// You shouldn't call this manually, it is called by ComponentContainer::createComponent
    template<class A>
    void set(TaggedIndex<A> a)
    {
      components.emplace(std::type_index(typeid(A)), a.data);
    }

    /// You shouldn't call this manually, it is called by ComponentContainer::createComponent
    template<class A>
    void update(TaggedIndex<A> a) noexcept
    {
      components.at(std::type_index(typeid(A))) = a.data;
    }


    /// You shouldn't call this manually, it is called by ComponentContainer::createComponent
    template<class A>
    void unset() noexcept
    {
      components.erase(components.find(std::type_index(typeid(A))));
    }

    template<class A>
    void addInlineComponent(std::unique_ptr<A> &&component)
    {
      inlineComponents.emplace(std::type_index(typeid(A)), std::move(component));
    }

    template<class A>
    void addListComponent(TaggedIndex<A> a)
    {
      componentLists[std::type_index(typeid(A))].emplace_back(a.data);
    }

    ~Entity() noexcept;
  };
};

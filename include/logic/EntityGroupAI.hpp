#pragma once

#include "claws/utils/lambda_ops.hpp"

#include "HurtBox.hpp"
#include "Ai.hpp"

namespace logic
{
  struct EntityGroupAI : public Ai
  {
    EntityGroupAI() = default;
    EntityGroupAI(EntityGroupAI const &) = delete;
    EntityGroupAI(EntityGroupAI &&) = delete;
    virtual ~EntityGroupAI() = default;

    virtual void update(logic::EntityManager &entityManager) override = 0;
  };

  template<size_t num>
  class EntityGroup : public EntityGroupAI
  {
  protected:
    std::array<logic::HurtBox::IndexType, num> hurtBoxes;

  public:
    template<class Access>
    auto getAccess(Access &&access) noexcept
    {
      return [this, access](auto index) -> auto &
	{
	  return access(hurtBoxes[index]);
	};
    }

    template<class Access>
    void translate(Access &&access, std::array<float, 2u> const &translation) noexcept
    {
      for (auto const &hurtBox : hurtBoxes)
	access(hurtBox).fixture.translate(translation);
    }
  };
}

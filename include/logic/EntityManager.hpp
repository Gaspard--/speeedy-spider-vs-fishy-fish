#pragma once

#include <memory>

#include "logic/TaggedIndex.hpp"
#include "Attack.hpp"
#include "Joint.hpp"
#include "DeformableEntity.hpp"
#include "SpawnInfo.hpp"
#include "BoundedVolumeHierarchy.hpp"
#include "logic/Portal.hpp"
#include "logic/Entity.hpp"
#include "logic/ComponentContainer.hpp"
#include "logic/AI.hpp"

#include "logic/FixtureComponent.hpp"

#include <deque>

namespace logic
{
  struct SpawnInfo;
  struct ColorComponent;
  struct JointComponent;
  struct AIComponent;
  struct FireComponent;
  class GunComponent;
  class PlayerComponent;
  struct PictureComponent;
  struct FlagComponent;
  class FishyComponent;
  class WaveComponent;
  struct BubbleComponent;

  class EntityManager
  {
  public:
    BoundedVolumeHierarchyTree<TaggedIndex<CollidableComponent>> boundedVolumeHierarchy;

    std::tuple<ComponentContainer<FixtureComponent>,
	       ComponentContainer<ColorComponent>,
	       ComponentContainer<JointComponent>,
	       ComponentContainer<FireComponent>,
	       ComponentContainer<GunComponent>,
	       ComponentContainer<AIComponent>,
	       ComponentContainer<PlayerComponent>,
	       ComponentContainer<PictureComponent>,
	       ComponentContainer<FlagComponent>,
	       ComponentContainer<FishyComponent>,
	       ComponentContainer<WaveComponent>,
	       ComponentContainer<BubbleComponent>,
	       ComponentContainer<CollidableComponent>> components;
    
    ComponentContainer<FixtureComponent> &fixtureComponents{ std::get<ComponentContainer<FixtureComponent>>(components) };

    std::vector<std::unique_ptr<Entity>> entities;

    template<class Component>
    auto &operator[](logic::TaggedIndex<Component> index) noexcept
    {
      return std::get<ComponentContainer<Component>>(components).data()[index.data];
    }

    std::vector<Portal> portals;

    auto const &operator[](Portal::IndexType index) const noexcept
    {
      return portals[index.data];
    }

    struct Accessor
    {
      EntityManager *entityManager;

      template<class Index>
      auto &operator()(Index index) const
      {
	return (*entityManager)[index];
      }
    };

    auto getAccessor() noexcept
    {
      return Accessor{this};
    }

    template<class Component, class Func>
    void forEachComponent(Func &&func) noexcept(noexcept(std::declval<Func>()(std::declval<Component &>())))
    {
      auto &componentList(std::get<ComponentContainer<Component>>(components));

      componentList.forEach(std::forward<Func>(func));
    }

    template<class Component, class Func>
    void forEachComponent(Func &&func) const noexcept(noexcept(std::declval<Func>()(std::declval<Component const &>())))
    {
      auto &componentList(std::get<ComponentContainer<Component>>(components));

      componentList.forEach(std::forward<Func>(func));
    }

    template<class Func>
    void forEachFixtureComponent(Func &&func) noexcept(noexcept(std::declval<Func>()(std::declval<FixtureComponent &>())))
    {
      fixtureComponents.forEach(std::forward<Func>(func));
    }

    EntityManager();

    void init();

    ~EntityManager() noexcept;

    EntityManager(EntityManager const &) = delete;
    EntityManager(EntityManager &&) = delete;
    EntityManager &operator=(EntityManager const &) = delete;
    EntityManager &operator=(EntityManager &&) = delete;

    Entity &createEntity()
    {
      entities.emplace_back(new Entity);
      return *entities.back();
    }

    template<class ComponentType, class... Params>
    TaggedIndex<ComponentType> createComponent(Params &&...params)
    {
      return std::get<ComponentContainer<ComponentType>>(components).createComponent(std::forward<Params>(params)...);
    }

    Entity &createEntity(Fixture const &fixture, std::array<float, 4u> const &color);
    void spawnLilypad(std::array<float, 2u> center, unsigned lilyPadQuantity);

    // TODO: remove ?
    std::unique_ptr<AI> spawnEntity(SpawnInfo const &spawnInfo);

    // Basic entity which deal damage. A bullet, for example
    Entity &createHurtingBox(Fixture const &fixture, std::array<float, 4u> const &color, unsigned int dmg);

    Entity &createHurtBox(Fixture const &fixture, std::array<float, 4u> const &color, unsigned int hp);

    void clear()
    {
      entities.clear();
    }

    void checkCollisions();
    void update();
    void removeOld();

    /// Used for debug
    template<class Func>
    void forEachBVHBoundingBox(Func &&func) const
    {
      boundedVolumeHierarchy.forEachBoundingBox(std::forward<Func>(func));
    }

    template<class Component>
    void removeComponent(TaggedIndex<Component> index) noexcept
    {
      return std::get<ComponentContainer<Component>>(components).removeComponent(index);
    }

    void notifyComponentEntityRemoval(std::type_index componentType, uint16_t index, Entity *entity);
  };
}

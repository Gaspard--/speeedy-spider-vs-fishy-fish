#pragma once

#include "logic/AI.hpp"

namespace logic
{
  struct ExplosiveSnakeAi : public AI
  {
    float timer = 0.0f;
  public:
    ExplosiveSnakeAi() = default;

    void update(std::vector<Entity *> &entities) noexcept;
  };
};

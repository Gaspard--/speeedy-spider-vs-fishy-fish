#pragma once

#include "logic/ComponentContainer.hpp"

namespace logic
{
  class Entity;

  class FishyComponent : public SingleEntityComponent<true>
  {
  public:
    uint32_t suck = 0;
    uint32_t phase = 0;
    bool attack = false;
    uint32_t maxHPs;
    uint32_t HPs;

    FishyComponent(Entity *e) noexcept;
    ~FishyComponent() noexcept;

    void update(logic::TaggedIndex<FishyComponent>);
  };
}

#pragma once

#include <limits>
#include <vector>
#include <algorithm>
#include <cmath>

#include <claws/utils/lambda_utils.hpp>
#include <claws/container/container_view.hpp>
#include <claws/container/iterator_pair.hpp>

#include "Transform2d.hpp"

namespace logic
{
  struct BoundingBox
  {
    std::array<float, 2u> min;
    std::array<float, 2u> max;
    bool sleep{false};

    constexpr BoundingBox() noexcept
      : min{0.0f, 0.0f}
      , max{0.0f, 0.0f}
    {}

    constexpr auto const &getMin() const noexcept
    {
      return min;
    }

    constexpr auto const &getMax() const noexcept
    {
      return max;
    }

    constexpr auto const &getSleep() const noexcept
    {
      return sleep;
    }

    template<class Shape>
    constexpr void update(Shape const &shape) noexcept
    {
      for (std::size_t i : {0ul, 1ul})
        {
          min[i] = std::numeric_limits<float>::infinity();
          max[i] = -std::numeric_limits<float>::infinity();
        }
      for (decltype(auto) point : shape)
        {
          for (std::size_t i : {0ul, 1ul})
            {
              min[i] = std::min(min[i], point[i]);
              max[i] = std::max(max[i], point[i]);
            }
        }
    }

    BoundingBox &operator|=(BoundingBox const &other) noexcept
    {
      for (std::size_t i : {0ul, 1ul})
	{
	  min[i] = std::min(min[i], other.getMin()[i]);
	  max[i] = std::max(max[i], other.getMax()[i]);
	}
      return *this;
    }

    BoundingBox operator|(BoundingBox const &other) const noexcept
    {
      return BoundingBox(*this) |= other;
    }

    float area()
    {
      return (max[0] - min[0]) * (max[1] - min[1]);
    }
  };

  template<class A>
  float area(A a)
  {
    return (a.getMax()[0] - a.getMin()[0]) * (a.getMax()[1] - a.getMin()[1]);
  }

  // This is the most basic fixture, which just contains a shape.
  // edgeCount indicates the amount of edges to consider.
  // if edgeCount is 0, the whole shape is considered, and the last point is connected to the first point
  // otherwise only the first edgeCount edges are taken into account
  template<class Shape, size_t edgeCount = 0>
  struct ShapeFixture : public Shape
  {
    constexpr ShapeFixture() = default;
    constexpr ShapeFixture(ShapeFixture const &) = default;
    constexpr ShapeFixture(ShapeFixture &&) noexcept = default;

    constexpr ShapeFixture &operator=(ShapeFixture const &) = default;
    constexpr ShapeFixture &operator=(ShapeFixture &&) noexcept = default;

    // shape has to be convex
    explicit constexpr ShapeFixture(Shape &&shape)
      : Shape(std::move(shape))
    {
    }

    constexpr auto const &getRelativeShape() const noexcept
    {
      return static_cast<Shape const &>(*this);
    }

    constexpr auto const &getShape() const noexcept
    {
      return getRelativeShape(); // no transform
    }

    constexpr auto getEdges() const noexcept
    {
      using ShapeIterator = decltype(getShape().begin());
      struct End
      {};

      if constexpr (!edgeCount)
	{
	  struct Iterator
	  {
	    ShapeIterator current;
	    ShapeIterator prev;
	    ShapeIterator begin;
	    ShapeIterator end;

	    Iterator &operator++()
	    {
	      ++prev;
	      ++current;
	      if (current == end)
		current = begin;
	      return *this;
	    }

	    std::array<std::array<float, 2u>, 2u> operator*()
	    {
	      return {*prev, *current};
	    }

	    bool operator!=(End)
	    {
	      return (prev != end);
	    }
	  };
	  auto begin(getShape().begin());
	  auto end(getShape().end());
	  auto it(begin);
	  if (it != end)
	    ++it;
	  return claws::iterator_pair<Iterator, End>{Iterator{it, begin, begin, end}, End{}};
	}
      else
	{
	  using ShapeRef = decltype(getShape());

	  struct Iterator
	  {
	    ShapeRef shape;
	    size_t index;

	    Iterator &operator++() noexcept
	    {
	      ++index;
	      return *this;
	    }

	    std::array<std::array<float, 2u>, 2u> operator*() noexcept
	    {
	      return {shape[index], shape[index + 1]};
	    }

	    bool operator!=(End) noexcept
	    {
	      return (index != edgeCount);
	    }
	  };
	  return claws::iterator_pair<Iterator, End>{Iterator{getShape(), 0u}, End{}};
	}
    }

    template<class Func>
    constexpr auto forEachEdge(Func &&func) const noexcept
    {
      auto it(getShape().begin());
      if constexpr (!edgeCount)
	{
	  if (it != getShape().end())
	    do
	      {
		auto prev(it);
		++it;
		if (it == getShape().end())
		  it = getShape().begin();
		func(*prev, *it);
	      }
	    while (it != getShape().begin());
	}
      else
	{
	  for (size_t i(0); i != edgeCount; ++i)
	    {
	      auto prev(it);
	      ++it;
	      func(*prev, *it);
	    }
	}
    }

    constexpr std::array<float, 2u> getImpulseAt(std::array<float, 2u> const &) const noexcept
    {
      return {0.0f, 0.0f};
    }

    constexpr std::array<float, 2u> getSpeedAt(std::array<float, 2u> const &) const noexcept
    {
      return {0.0f, 0.0f};
    }

    constexpr std::array<float, 2u> getSpeed() const noexcept
    {
      return {0.0f, 0.0f};
    }

    constexpr float getRotation() const noexcept
    {
      return 0.0f;
    }

    constexpr void applyImpulseAt(std::array<float, 2u> const &, std::array<float, 2u> const &) noexcept
    {}

    constexpr void applyForceAt(std::array<float, 2u> const &, std::array<float, 2u> const &) noexcept
    {}

    constexpr float getInverseMass() const noexcept
    {
      return 0.0f;
    }

    constexpr float getInverseAngularMass() const noexcept
    {
      return 0.0f;
    }
  };

  template<class Shape>
  ShapeFixture<Shape, 0> &asShapeFixture(Shape &shape)
  {
    return static_cast<ShapeFixture<Shape> &>(shape);
  }

  // edgeCount indicates the amount of edges to consider.
  // if edgeCount is 0, the whole shape is considered, and the last point is connected to the first point
  // otherwise only the first edgeCount edges are taken into account
  template<class Shape, size_t edgeCount = 0>
  struct NotMovingFixture
  {
    Shape shape;
    Transform2d transform;
    BoundingBox boundingBox;

    constexpr NotMovingFixture() = default;
    constexpr NotMovingFixture(NotMovingFixture const &) = default;
    constexpr NotMovingFixture(NotMovingFixture &&) noexcept = default;

    constexpr NotMovingFixture &operator=(NotMovingFixture const &) = default;
    constexpr NotMovingFixture &operator=(NotMovingFixture &&) noexcept = default;

    // shape has to be convex
    constexpr NotMovingFixture(Shape const &shape, Transform2d transform = {{0.0f, 0.0f}, {1.0f, 0.0f}})
      : shape(shape)
      , transform(transform)
    {
      updateBoundingBox();
    }

    constexpr void update() noexcept
    {
      if (boundingBox.sleep)
        return;
      updateBoundingBox();
      boundingBox.sleep = true;
    }

    constexpr void wakeUp() noexcept
    {
      boundingBox.sleep = false;
    }

    constexpr auto const &getRelativeShape() const noexcept
    {
      return shape;
    }

    constexpr auto getShape() const noexcept
    {
      struct Functor
      {
	Transform2d transform;

	constexpr std::array<float, 2u> operator()(std::array<float, 2u> relativePosition) const noexcept
	{
	  return transform.apply(relativePosition);
	}
      };
      return claws::container_view(shape, Functor{transform});
    }

    constexpr auto getEdges() const noexcept
    {
      using ShapeIterator = decltype(getShape().begin());
      struct End
      {};

      if constexpr (!edgeCount)
	{
	  struct Iterator
	  {
	    ShapeIterator current;
	    ShapeIterator prev;
	    ShapeIterator begin;
	    ShapeIterator end;

	    Iterator &operator++()
	    {
	      ++prev;
	      ++current;
	      if (current == end)
		current = begin;
	      return *this;
	    }

	    std::array<std::array<float, 2u>, 2u> operator*()
	    {
	      return {*prev, *current};
	    }

	    bool operator!=(End)
	    {
	      return (prev != end);
	    }
	  };
	  auto begin(getShape().begin());
	  auto end(getShape().end());
	  auto it(begin);
	  if (it != end)
	    ++it;
	  return claws::iterator_pair<Iterator, End>{Iterator{it, begin, begin, end}, End{}};
	}
      else
	{
	  using ShapeRef = decltype(getShape());
	  struct Iterator
	  {
	    ShapeRef shape;
	    size_t index;

	    Iterator &operator++() noexcept
	    {
	      ++index;
	      return *this;
	    }

	    std::array<std::array<float, 2u>, 2u> operator*() noexcept
	    {
	      return {shape[index], shape[index + 1]};
	    }

	    bool operator!=(End) noexcept
	    {
	      return (index != edgeCount);
	    }
	  };
	  return claws::iterator_pair<Iterator, End>{Iterator{getShape(), 0u}, End{}};
	}
    }

    template<class Func>
    constexpr auto forEachEdge(Func &&func) const noexcept
    {
      auto it(getShape().begin());
      if constexpr (!edgeCount)
	{
	  if (it != getShape().end())
	    do
	      {
		auto prev(it);
		++it;
		if (it == getShape().end())
		  it = getShape().begin();
		func(*prev, *it);
	      }
	    while (it != getShape().begin());
	}
      else
	{
	  for (size_t i(0); i != edgeCount; ++i)
	    {
	      auto prev(it);
	      ++it;
	      func(*prev, *it);
	    }
	}
    }

    constexpr void translate(std::array<float, 2u> const &translation) noexcept
    {
      wakeUp();
      transform.position += translation;
    }

    constexpr std::array<float, 2u> getImpulseAt(std::array<float, 2u> const &) const noexcept
    {
      return {0.0f, 0.0f};
    }

    constexpr std::array<float, 2u> getSpeedAt(std::array<float, 2u> const &) const noexcept
    {
      return {0.0f, 0.0f};
    }

    constexpr std::array<float, 2u> getSpeed() const noexcept
    {
      return {0.0f, 0.0f};
    }

    constexpr float getRotation() const noexcept
    {
      return 0.0f;
    }

    constexpr float getEnergy()
    {
      return 0.0f;
    }

    constexpr void applyImpulseAt(std::array<float, 2u> const &, std::array<float, 2u> const &) noexcept
    {}

    constexpr void applyForceAt(std::array<float, 2u> const &, std::array<float, 2u> const &) noexcept
    {}

    constexpr void rotate(std::array<float, 2u> dir) noexcept
    {
      wakeUp();
      transform.rotation *= dir;
    }

    constexpr void updateBoundingBox() noexcept
    {
      boundingBox.update(getShape());
    }

    constexpr BoundingBox const &getBoundingBox() const noexcept
    {
      return boundingBox;
    }

    constexpr float getInverseMass() const noexcept
    {
      return 0.0f;
    }

    constexpr float getInverseAngularMass() const noexcept
    {
      return 0.0f;
    }
  };

  struct Fixture : public NotMovingFixture<std::vector<std::array<float, 2u>>>
  {
    float inverseMass;        // 1 / g
    float inverseAngularMass; // 1 / (g * m^2)
    float friction;
    float timeSpeed;
    std::array<float, 2u> speed;
    float rotation;

    Fixture() = default;
    Fixture(Fixture const &) = default;
    Fixture(Fixture &&) noexcept = default;

    Fixture &operator=(Fixture const &) = default;
    Fixture &operator=(Fixture &&) noexcept = default;

    Fixture(std::vector<std::array<float, 2u>> const &cornerPositions, float inverseDensity, float friction = 0.05f);
    Fixture(std::vector<std::array<float, 2u>> const &cornerPositions, Transform2d centerPosition, Transform2d positionObject);

    void update() noexcept;

    std::array<float, 2u> getImpulseAt(std::array<float, 2u> const &point) const noexcept
    {
      auto const diff(point - transform.position);

      return ((octogonal(diff) / claws::length2(diff) * rotation / inverseAngularMass) // m / (m ^ 2) / s * g * m ^ 2 = m / s * g
              + speed / inverseMass);                                                  // m / s * g
    }

    std::array<float, 2u> getSpeedAt(std::array<float, 2u> const &point) const noexcept
    {
      auto const diff(point - transform.position);

      return (cross(rotation, diff) // m * s-1
              + speed); // m * s-1
    }

    constexpr std::array<float, 2u> getSpeed() const noexcept
    {
      return speed;
    }

    constexpr float getRotation() const noexcept
    {
      return rotation;
    }

    constexpr float getEnergy()
    {
      float energy = 0.0f;
      if (getInverseMass() != 0.0f)
	energy += 0.5f / getInverseMass() * claws::length2(speed);
      if (getInverseAngularMass() != 0.0f)
	energy += 0.5f / getInverseAngularMass() * rotation * rotation;
      return energy;

    }

    void applyImpulseAt(std::array<float, 2u> const &point, std::array<float, 2u> const &impulse) noexcept
    {
      auto const diff(point - transform.position);

      wakeUp();
      speed += impulse * getInverseMass();
      rotation += cross(impulse, diff) * getInverseAngularMass();
    }

    void applyForceAt(std::array<float, 2u> const &point, std::array<float, 2u> const &force) noexcept
    {
      applyImpulseAt(point, force / 60.0f);
    }

    void move(std::array<float, 2u> dir) noexcept
    {
      wakeUp();
      speed += dir;
    }

    constexpr float getInverseMass() const noexcept
    {
      return inverseMass;
    }

    constexpr float getInverseAngularMass() const noexcept
    {
      return inverseAngularMass;
    }
  };

  template<class FixtureType>
  struct SingleSubViewFixture : public FixtureType
  {
    auto getFixtureViews() noexcept
    {
      struct FixtureViewList
      {
        FixtureType *fixture;

        auto begin() noexcept
        {
          return fixture;
        }

        auto end() noexcept
        {
          return fixture + 1;
        }
      };

      return FixtureViewList{this};
    }
  };
};

#pragma once

#include "logic/ComponentContainer.hpp"
#include "logic/Fixture.hpp"

namespace logic
{
  template<class IndexType>
  struct BoundedVolumeHierarchyNode;

  struct FixtureComponent : public SingleEntityComponent<true>
  {
    Fixture fixture;

    FixtureComponent(Entity *entity, Fixture fixture)
      : SingleEntityComponent{entity}
      , fixture(fixture)
    {}

    FixtureComponent(FixtureComponent const &) = delete;
    FixtureComponent(FixtureComponent &&) = default;
    FixtureComponent& operator=(FixtureComponent const &) = default;
    FixtureComponent& operator=(FixtureComponent &&) = default;

    void update(logic::TaggedIndex<FixtureComponent> index);

    ~FixtureComponent() noexcept;
  };

  struct CollidableComponent : public SingleEntityComponent<false>
  {
    std::vector<std::array<float, 2>> shape;

    BoundedVolumeHierarchyNode<TaggedIndex<CollidableComponent>> *boundedVolumeHierarchyNode = nullptr;
    uint32_t used : 1;
    uint32_t permanent : 1;

    CollidableComponent(Entity *entity) noexcept
      : SingleEntityComponent(entity)
    {
    }
    CollidableComponent(CollidableComponent &&other) noexcept;

    CollidableComponent(CollidableComponent const &) = delete;    
    CollidableComponent& operator=(CollidableComponent &&) = delete;
    CollidableComponent& operator=(CollidableComponent const &) = delete;
    ~CollidableComponent() noexcept;

    bool available() const noexcept
    {
      return !used;
    }

    void setShape(logic::TaggedIndex<CollidableComponent> index, std::vector<std::array<float, 2>> &&shape, logic::BoundingBox boundingBox, bool permanent = false);
    void update(logic::TaggedIndex<CollidableComponent> index) noexcept;
    bool compatible(logic::BoundingBox const &boundingBox) const noexcept;
    void removeShape() noexcept;
  };

  void swap(CollidableComponent &lh, CollidableComponent &rh) noexcept;
}

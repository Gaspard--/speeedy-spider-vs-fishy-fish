#pragma once

#include "ComponentContainer.hpp"

namespace logic
{
  namespace flag
  {
    enum class Type {
      LilyPad,
      LilyPadAffected,
      UnderWater,
      Bullet,
      Unspecified
    };
  }
  //
  // this component allow an entity to be recognize as a specific entity
  struct FlagComponent : public SingleEntityComponent<true>
  {
    FlagComponent(Entity *entity, flag::Type type = flag::Type::Unspecified, bool dontRemove = false)
    : SingleEntityComponent{entity}
    , type(type)
    , dontRemove(dontRemove)
    {}

    flag::Type type;
    bool dontRemove;
  };
}

#pragma once

#include "logic/ComponentContainer.hpp"

namespace logic
{
  class Entity;

  class GunComponent : public SingleEntityComponent<false>
  {
    uint32_t cooldown;
    uint32_t bonus;

    uint32_t cooldownTime;
    bool bonusUsed;
    bool firing;
  public:
    GunComponent(Entity *e, uint32_t cooldown, uint32_t bonus) noexcept;
    ~GunComponent() noexcept;

    void update(logic::TaggedIndex<GunComponent>);
    void fire() noexcept;
    void stopFire() noexcept;
  };
}

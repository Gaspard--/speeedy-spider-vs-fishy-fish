#pragma once

#include "logic/Fixture.hpp"

namespace logic
{
  struct Joint
  {
    struct Anchor
    {
      std::array<float, 2u> point;
    };

    std::array<Anchor, 2u> anchors;
    float strength;

    constexpr Joint(Fixture const &a, Fixture const &b, std::array<float, 2u> point, float strength) noexcept
      : anchors{(-a.transform).apply(point), (-b.transform).apply(point)}
      , strength(strength)
    {}

    auto update(Fixture &a, Fixture &b) noexcept
    {
      std::array<float, 2u> pointA(a.transform.apply(anchors[0].point));
      std::array<float, 2u> pointB(b.transform.apply(anchors[1].point));
      auto diff(pointA - pointB);
      auto power(diff * strength);

      a.applyForceAt(pointA, -power);
      b.applyForceAt(pointB, power);
      return diff;
    }
  };
}

#pragma once

#include "EntityManager.hpp"

namespace input
{
  class Input;
};

namespace logic
{
  class Logic
  {
    EntityManager entityManager;
    unsigned nbFrameSinceLastLilypad{0};
    unsigned lilyPadQuantity{0};
    unsigned nbFrameSinceLastFishyUpdate{0};
    bool gameOver{false};
    bool win{false};
    bool intro{true};
    float screenShake;
    Transform2d camera;

    uint32_t animFrame = 0;
  public:
    Logic();
    Logic(Logic const &) = delete;
    Logic(Logic &&) = delete;
    Logic &operator=(Logic const &) = delete;
    Logic &operator=(Logic &&) = delete;

    bool isWin() const noexcept
    {
      return win;
    }

    bool isGameOver() const noexcept
    {
      return gameOver;
    }

    bool isIntro() const noexcept
    {
      return intro;
    }

    void handleEvents(input::Input &input);

    void tick(input::Input &input);

    auto const &getEntityManager() const noexcept
    {
      return entityManager;
    }

    auto getScale() const noexcept
    {
      return 1.0f;
    }
    
    Transform2d getCamera() const noexcept;

    uint32_t getAnimFrame() const noexcept
    {
      return animFrame;
    }
  };
}

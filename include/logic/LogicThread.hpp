#pragma once

#include <mutex>

template<class Func>
struct LogicThread
{
  using Clock = std::conditional<std::chrono::high_resolution_clock::is_steady, std::chrono::high_resolution_clock, std::chrono::steady_clock>::type;

  Func tick;
  bool isRunning;
  decltype(Clock::now()) lastUpdate;
  std::mutex &lock;

  LogicThread<Func>(Func tick, std::mutex &lock)
    : tick(tick)
    , isRunning(true)
    , lastUpdate(Clock::now())
    , lock(lock)
  {}

  static constexpr std::chrono::microseconds tickTime{1000000 / 240};

  void operator()()
  {
    while (true)
      {
        auto const now(Clock::now());

        if (now > lastUpdate + tickTime * 2)
          {
            lastUpdate = now;
            continue;
          }
        {
          std::lock_guard<std::mutex> scopedLock(lock);

          if (!isRunning)
            return;
          tick();
          lastUpdate += tickTime;
        }
        if (now < lastUpdate)
          std::this_thread::sleep_for(lastUpdate - now);
      }
  }

  void quit()
  {
    std::lock_guard<std::mutex> scopedLock(lock);

    isRunning = false;
  }
};

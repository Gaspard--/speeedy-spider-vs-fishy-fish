#pragma once

#include "ComponentContainer.hpp"
#include "display/SpriteType.hpp"
#include "logic/Transform2d.hpp"

namespace logic
{
  struct PictureComponent : public SingleEntityComponent<true>
  {
    float displayedWidth;
    float displayedHeight;
    uint32_t currentFrame{0};
    display::SpriteType sprite;
    Transform2d offset;
    std::array<float, 4u> mask;

    PictureComponent(Entity *e, float w, float h, display::SpriteType sprite, Transform2d const &offset = {{0.0f, 0.0f}, {1.0f, 0.0f}}, std::array<float, 4u> const & mask = {1.0f, 1.0f, 1.0f, 1.0f})
        : SingleEntityComponent<true>(e)
        , displayedWidth(w)
        , displayedHeight(h)
        , sprite(sprite)
	, offset(offset)
	, mask(mask)
    {}

    void resetFrame() {
      currentFrame = 0;
    }

    void update(TaggedIndex<PictureComponent>) {
      ++currentFrame;
      // need to do something like currentFrame = someFunction(sprite, currentFrame);
    }

  };
}

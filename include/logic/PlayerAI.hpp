#pragma once

#include "logic/EntityGroupAI.hpp"

// TODO: determine if useful
namespace logic
{
  template<class Index>
  class PlayerAI : public EntityGroupAI
  {
    Index body;
  public:
    PlayerAI(Index body) noexcept
      : body(body)
    {
    }

    PlayerAI(PlayerAI const &) noexcept = default;
    PlayerAI(PlayerAI &&) noexcept = default;
    PlayerAI operator=(PlayerAI const &) noexcept = default;
    PlayerAI operator=(PlayerAI &&) noexcept = default;

    virtual void update(logic::EntityManager &, std::array<float, 2u>) noexcept override
    {
    };
  };
}

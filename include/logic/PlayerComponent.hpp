#pragma once

#include "logic/ComponentContainer.hpp"

namespace logic
{
  class Entity;

  class PlayerComponent : public SingleEntityComponent<true>
  {
  public:
    PlayerComponent(Entity *e) noexcept;
    ~PlayerComponent() noexcept;

    void update(logic::TaggedIndex<PlayerComponent>);
  };
}

#pragma once

#include "logic/Fixture.hpp"

#include "logic/TaggedIndex.hpp"

#include <array>

namespace logic
{
  template<class A>
  struct BoundedVolumeHierarchyNode;

  class EntityManager;

  class Portal
  {
  public:
    struct Tag;

    using IndexType = TaggedIndex<Tag>;

  private:
    logic::NotMovingFixture<std::array<std::array<float, 2>, 2>> fixture;
    Portal::IndexType otherEnd;

  public:
    Portal(std::array<std::array<float, 2>, 2> const &shape) noexcept;
    void setLink(Portal::IndexType otherEnd) noexcept;
    logic::NotMovingFixture<std::array<std::array<float, 2>, 2>> const getFixture() const noexcept;
    Portal const getOtherPortal(EntityManager const &entityManager) const noexcept;
  };
}

#pragma once

# include <SFML/Audio.hpp>
# include <map>

namespace logic {
  class SoundHandler
  {
  public:

    static void initSoundHandler(std::string const &mainMusic, std::string const &secondMusic);

    static SoundHandler &getInstance();

    static void destroySoundHandler();

    float sfxPitch{100.0f};

    void launchMainMusic();
    void launchSecondaryMusic();

    void setMainVolume(float v);
    void setSecondaryVolume(float v);

    void setMainPitch(float pitch);
    void setSecondaryPitch(float pitch);

  private:
    SoundHandler();

    static std::unique_ptr<SoundHandler> _instance;

    sf::Music mainMusic;
    sf::Music secondMusic;
  };
}

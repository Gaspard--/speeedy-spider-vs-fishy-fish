#pragma once

#include <array>
#include <variant>

namespace logic
{
  struct SpiderBossSpawnInfo
  {};

  // struct SpiderSpawnInfo
  // {};

  struct ObstacleSpawnInfo
  {};

  struct SpawnInfo
  {
    std::array<float, 2u> position;
    float scale;
    std::variant<SpiderBossSpawnInfo> additionalInfo;
  };
}

#pragma once

#include <claws/utils/lambda_ops.hpp>
#include <array>
#include <variant>

#include "logic/TaggedIndex.hpp"
#include "logic/AI.hpp"
// #include "logic/EntityGroupAI.hpp"
// #include "HurtBox.hpp"

namespace logic
{
  void makeSpider(std::array<float, 2u> position = {0.0f, 0.0f}, float scale = 1.0f);
};

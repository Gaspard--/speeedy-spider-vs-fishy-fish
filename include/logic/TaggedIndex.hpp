#pragma once

#include <claws/utils/tagged_data.hpp>

#include <cstdint>

namespace logic
{
  class EntityManager;
  extern EntityManager *g_entityManager;

  /// \brief typify a type, helping with type safety
  /// \tparam _data_type the stored type
  /// \tparam _offset_type used for offsets (operators `+=`, `+`, `-=`, `-`)
  /// \tparam _tag a struct used as tag
  ///
  /// Stores a `TaggedIndex::data_type` value.
  /// If `offset_type` is the same as `data_type` this behaves like an index
  /// If `offset_type` is not the same as `data_type` this behaves like a pointer
  /// This class can't be assigned a `data_type` by default,
  /// helping with accidental promotion
  /// It should be typically used to help not making mistakes when handling
  /// different indexes or pointers of the same underlying type.
  /// The underling type should be cheap to copy and assign,
  /// and never throw in such operations. (Ideally, a primitive type)
  ///
  template<class _tag>
  struct TaggedIndex
  {
    /// the used as tag
    using tag = _tag;
    /// used for offsets (operators `+=`, `+`, `-=`, `-`)
    using offset_type = uint16_t;
    /// the stored type
    using data_type = uint16_t;
    /// \brief the actual data
    /// access this when you need the actual value (typically inside your api)
    data_type data;

    constexpr TaggedIndex(TaggedIndex const &) noexcept = default;
    constexpr TaggedIndex &operator=(TaggedIndex const &) noexcept = default;

    /// Construtor is explicit, to avoid conversion
    explicit constexpr TaggedIndex(data_type data = 0u) noexcept
      : data(data)
    {}

#define TAGGEDINDEX_PREFIX_OP(OP)			\
    constexpr TaggedIndex &operator OP() noexcept	\
    {							\
      OP data;						\
      return *this;					\
    }

    TAGGEDINDEX_PREFIX_OP(++);
    TAGGEDINDEX_PREFIX_OP(--);

#undef TAGGEDINDEX_PREFIX_OP

#define TAGGEDINDEX_SUFFIX_OP(OP)			\
    constexpr TaggedIndex operator OP(int) noexcept	\
    {							\
      auto copy(*this);					\
      OP *this;						\
      return copy;					\
    }

    TAGGEDINDEX_SUFFIX_OP(++);
    TAGGEDINDEX_SUFFIX_OP(--);

#undef TAGGEDINDEX_SUFFIX_OP

#define TAGGEDINDEX_COMPARE(OP)						\
    constexpr bool operator OP(TaggedIndex const &other) const noexcept \
    {                                                                   \
      return data OP other.data;                                        \
    }

    TAGGEDINDEX_COMPARE(==);
    TAGGEDINDEX_COMPARE(!=);
    TAGGEDINDEX_COMPARE(<=);
    TAGGEDINDEX_COMPARE(>=);
    TAGGEDINDEX_COMPARE(<);
    TAGGEDINDEX_COMPARE(>);

#undef TAGGEDINDEX_COMPARE

#define TAGGEDINDEX_BINARY_OP(OP)					\
    constexpr auto operator OP(offset_type const &other) const noexcept \
    {									\
      return TaggedIndex(static_cast<data_type>(data OP other));	\
    }

    TAGGEDINDEX_BINARY_OP(+);
    TAGGEDINDEX_BINARY_OP(-);

#undef TAGGEDINDEX_BINARY_OP


    tag &operator*() const noexcept
    {
      return (*g_entityManager)[*this];
    }

    tag *operator->() const noexcept
    {
      return &(*g_entityManager)[*this];
    }
  };
}

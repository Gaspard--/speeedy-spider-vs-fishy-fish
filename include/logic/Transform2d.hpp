#pragma once

#include "claws/container/array_ops.hpp"

// candidate to be moved into lib claw, if it becomes feature complete enough
namespace logic
{
  using claws::array_ops::operator+=;
  using claws::array_ops::operator-=;
  using claws::array_ops::operator+;
  using claws::array_ops::operator-;
  using claws::scalar_array_ops::operator*=;
  using claws::scalar_array_ops::operator*;
  using claws::scalar_array_ops::operator/=;
  using claws::scalar_array_ops::operator/;

  constexpr inline static auto extend(std::array<float, 2u> const &dir)
  {
    return std::array<float, 3u>{dir[0], dir[1], 1.0f};
  }

  template<size_t num, size_t... indexes>
  constexpr inline static auto cross3D_impl(std::array<float, num> const &v0, std::array<float, num> const &v1, std::index_sequence<indexes...>) noexcept
  {
    return std::array<float, num>{v0[(indexes + 1) % num] * v1[(indexes + 2) % num] - v0[(indexes + 2) % num] * v1[(indexes + 1) % num]...};
  }

  constexpr inline static auto cross3D(std::array<float, 3> const &v0, std::array<float, 3> const &v1) noexcept
  {
    return cross3D_impl(v0, v1, std::make_index_sequence<3>{});
  }


  constexpr inline static auto octogonal(std::array<float, 2u> const &dir) noexcept
  {
    return std::array<float, 2u>{-dir[1], dir[0]};
  }

  constexpr inline static auto cross(std::array<float, 2u> const &v, float z) noexcept
  {
    return std::array<float, 2u>{v[1] * z, -v[0] * z};
  }

  constexpr inline static auto cross(float z, std::array<float, 2u> const &v) noexcept
  {
    return std::array<float, 2u>{-z * v[1], z * v[0]};
  }

  constexpr inline static auto cross(std::array<float, 2u> const &v1, std::array<float, 2u> const &v2) noexcept
  {
    return v1[0] * v2[1] - v1[1] * v2[0];
  }

  template<class T>
  constexpr auto conjugate(std::array<T, 2u> lh) noexcept
  {
    lh[1] *= -1;
    return lh;
  }

  template<class T, class U>
  constexpr std::array<T, 2u> &operator*=(std::array<T, 2u> &lh, std::array<U, 2u> const &rh)
  {
    return lh = {lh[0] * rh[0] - lh[1] * rh[1], lh[0] * rh[1] + lh[1] * rh[0]};
  }

  template<class T, class U>
  constexpr std::array<T, 2u> operator*(std::array<T, 2u> lh, std::array<U, 2u> const &rh)
  {
    return lh *= rh;
  }

  struct Transform2d
  {
    std::array<float, 2u> position;
    std::array<float, 2u> rotation;

    constexpr std::array<float, 2u> apply(std::array<float, 2u> const &val) const noexcept
    {
      return val * rotation + position;
    }
  };

  constexpr Transform2d &operator+=(Transform2d &lh, Transform2d const &rh) noexcept
  {
    // with * as rotation
    // (x * lh.rotation + lh.position) + rh.rotation + rh.position =
    // x * lh.rotation * rh.rotation + lh.position * rh.rotation + rh.position;
    lh.position = lh.position * rh.rotation + rh.position;
    lh.rotation *= rh.rotation;
    return lh;
  }

  constexpr Transform2d operator+(Transform2d lh, Transform2d const &rh) noexcept
  {
    return lh += rh;
  }

  constexpr Transform2d operator-(Transform2d const &val) noexcept
  {
    // (x * val.rotation + val.position) * inv.rotation + inv.position == x;
    // x * val.rotation + val.position = x * inv.rotation^-1 - inv.position * inv.rotation^-1
    // x * val.rotation + val.position = x * inv.rotation^-1 - inv.position * inv.rotation^-1
    return {-val.position * conjugate(val.rotation), conjugate(val.rotation)};
  }

  constexpr Transform2d operator-=(Transform2d &lh, Transform2d const &rh)
  {
    return lh += -rh;
  }

  constexpr Transform2d operator-(Transform2d lh, Transform2d const &rh) noexcept
  {
    return lh -= rh;
  }
}

#pragma once

#include "logic/ComponentContainer.hpp"

namespace logic
{
  class Entity;
  struct FixtureComponent;

  class WaveComponent : public SingleEntityComponent<true>
  {
  public:
    float size;
    float speed;
    float strength;

    WaveComponent(Entity *e, float size, float speed, float strength) noexcept;
    ~WaveComponent() noexcept;

    void update(logic::TaggedIndex<WaveComponent>);

    void apply(FixtureComponent *targetFixtureComponent);
  };
}

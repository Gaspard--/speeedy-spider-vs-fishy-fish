#pragma once

#include "magma/Device.hpp"
#include "magma/Deleter.hpp"

#include "magma/Globals.hpp"

namespace magma
{
  template<class Deleter = Deleter>
  using Buffer = claws::handle<vk::Buffer, Deleter>;

  inline auto createBuffer(vk::BufferCreateFlags flags, vk::DeviceSize size, vk::BufferUsageFlags usage)
  {
    auto device(getDevice());

    return Buffer<>({},
                    device.createBuffer({flags, size, usage, vk::SharingMode::eExclusive, 0, nullptr}));
  }

  inline auto createBuffer(vk::BufferCreateFlags flags,
			   vk::DeviceSize size,
			   vk::BufferUsageFlags usage,
			   std::vector<uint32_t> const &queueFamilies)
  {
    if (queueFamilies.size() == 1)
      return createBuffer(flags, size, usage);
    auto device(getDevice());

    return Buffer<>({},
                    device.createBuffer({flags, size, usage, vk::SharingMode::eConcurrent, static_cast<uint32_t>(queueFamilies.size()), queueFamilies.data()}));
  }

};

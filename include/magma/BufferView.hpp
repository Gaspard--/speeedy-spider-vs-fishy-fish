#pragma once

#include "magma/Deleter.hpp"
#include "magma/Device.hpp"

namespace magma
{
  template<class Deleter = Deleter>
  using BufferView = claws::handle<vk::BufferView, Deleter>;

  inline auto createBufferView(vk::BufferViewCreateFlags flags, claws::handle<vk::Buffer, claws::no_delete> buffer, vk::Format format, vk::DeviceSize offset, vk::DeviceSize size)
  {
    auto device(getDevice());

    return BufferView<>(Deleter{}, device.createBufferView({flags, buffer, format, offset, size}));
  }
};

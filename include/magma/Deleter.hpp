#pragma once

#include "magma/Device.hpp"
#include "magma/Globals.hpp"

namespace magma
{
  struct Deleter
  {
    template<class T>
    void operator()(T const &obj) const
    {
      auto device(getDevice());

      if (device)
	device.destroy(obj);
    }
  };
};

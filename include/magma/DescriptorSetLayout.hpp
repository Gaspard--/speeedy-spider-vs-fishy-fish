#pragma once


#include "magma/Deleter.hpp"
#include "magma/Device.hpp"

#include "magma/Globals.hpp"

namespace magma
{
  template<class Deleter = Deleter>
  using DescriptorSetLayout = claws::handle<vk::DescriptorSetLayout, Deleter>;

  inline auto createDescriptorSetLayout(std::vector<vk::DescriptorSetLayoutBinding> const &bindings)
  {
    auto device(getDevice());

    return DescriptorSetLayout<>({},
				 device.createDescriptorSetLayout({{},
								   static_cast<uint32_t>(bindings.size()),
								   bindings.data()}));
  }
};

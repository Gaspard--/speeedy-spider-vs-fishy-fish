#pragma once

#include "magma/Deleter.hpp"
#include "magma/Globals.hpp"
#include "magma/Device.hpp"

namespace magma
{
  struct DescriptorSetsDeleter
  {
    vk::DescriptorPool descriptorPool;

    void operator()(std::vector<vk::DescriptorSet> const &descriptorSets) const
    {
      if (!descriptorSets.empty())
	{
	  auto device(getDevice());

	  device.freeDescriptorSets(descriptorPool, descriptorSets);
	}
    }
  };

  template<class Deleter = DescriptorSetsDeleter>
  using DescriptorSets = claws::group_handle<vk::DescriptorSet, std::vector<vk::DescriptorSet>, Deleter>;

  inline void updateDescriptorSets(vk::WriteDescriptorSet const *writeDescriptorSets, uint32_t writeDescriptorSetCount)
  {
    auto device(getDevice());

    device.updateDescriptorSets(writeDescriptorSetCount, writeDescriptorSets, 0, nullptr);
  }

  inline void updateDescriptorSets(vk::CopyDescriptorSet const *copyDescriptorSets, uint32_t copyDescriptorSetCount)
  {
    auto device(getDevice());

    device.updateDescriptorSets(0, nullptr, copyDescriptorSetCount, copyDescriptorSets);
  }


  template<class T>
  void updateDescriptorSets(T const &container)
  {
    updateDescriptorSets(container.data(), static_cast<uint32_t>(container.size()));
  }

  namespace impl
  {
    class DescriptorPool : public vk::DescriptorPool
    {
    public:
      ~DescriptorPool() = default;

      DescriptorPool()
        : vk::DescriptorPool(nullptr)
      {}

      DescriptorPool(vk::DescriptorPool descriptorPool)
        : vk::DescriptorPool(descriptorPool)
      {}

      void reset(vk::DescriptorPoolResetFlags flags) const
      {
	auto device(getDevice());

        device.resetDescriptorPool(*this, flags);
      }

      auto allocateDescriptorSets(std::vector<vk::DescriptorSetLayout> const &descriptorSetLayout)
      {
	auto device(getDevice());
        vk::DescriptorSetAllocateInfo info{*this, static_cast<uint32_t>(descriptorSetLayout.size()), descriptorSetLayout.data()};

        return DescriptorSets<>({*this}, device.allocateDescriptorSets(info));
      }

      void swap(DescriptorPool &other)
      {
        using std::swap;

        swap(static_cast<vk::DescriptorPool &>(*this), static_cast<vk::DescriptorPool &>(other));
      }

      struct Deleter
      {
	friend class DescriptorPool;

	void operator()(DescriptorPool const &descriptorPool)
	{
	  magma::Deleter{}(descriptorPool);
	}
      };
    };

    inline void swap(DescriptorPool &lh, DescriptorPool &rh)
    {
      lh.swap(rh);
    }
  }

  template<class Deleter = impl::DescriptorPool::Deleter>
  using DescriptorPool = claws::handle<impl::DescriptorPool, Deleter>;

  inline auto createDescriptorPool(std::uint32_t maxSets, std::vector<vk::DescriptorPoolSize> const &size)
  {
    auto device(getDevice());

    return magma::DescriptorPool<>({},
                                   device.createDescriptorPool({vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet, maxSets, static_cast<uint32_t>(size.size()), size.data()}));
  }
};

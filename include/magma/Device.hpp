#pragma once

#include <claws/utils/handle_types.hpp>
#include "magma/VulkanHandler.hpp"

namespace magma
{
  namespace impl
  {
    class Device : public vk::Device
    {
    protected:
      ~Device() = default;

    public:
      Device()
        : vk::Device(nullptr)
      {}

      Device(vk::PhysicalDevice physicalDevice,
             std::vector<vk::DeviceQueueCreateInfo> const &deviceQueueCreateInfos,
             std::vector<char const *> const &extensions = {})
        : vk::Device([](vk::PhysicalDevice physicalDevice,
                        std::vector<vk::DeviceQueueCreateInfo> const &deviceQueueCreateInfos,
                        std::vector<char const *> const &extensions) {
		       vk::DeviceCreateInfo deviceCreateInfo{{},
							     static_cast<unsigned>(deviceQueueCreateInfos.size()),
							     deviceQueueCreateInfos.data(),
							     0,
							     nullptr,
							     static_cast<unsigned>(extensions.size()),
							     extensions.data()};

		       return physicalDevice.createDevice(deviceCreateInfo);
		     }(physicalDevice, deviceQueueCreateInfos, extensions))
      {}


      void swap(Device &other)
      {
        using std::swap;

        swap(static_cast<vk::Device &>(*this), static_cast<vk::Device &>(other));
      }

      using vk::Device::destroy;

      using vk::Device::operator bool;
      using vk::Device::operator!;

      template<class... Params>
      decltype(std::declval<vk::Device>().destroy(std::declval<Params>()...)) destroy(Params &&...) = delete;

      struct DeviceDeleter
      {
        void operator()(vk::Device const &device) const
        {
          if (device)
            device.destroy();
        }
      };
    };

    static inline void swap(impl::Device &lh, impl::Device &rh) noexcept
    {
      lh.swap(rh);
    }
  }

  template<class Deleter = impl::Device::DeviceDeleter>
  using Device = claws::handle<impl::Device, Deleter>;
};

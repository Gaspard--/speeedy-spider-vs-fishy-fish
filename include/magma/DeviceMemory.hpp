#pragma once

#include "magma/Device.hpp"

namespace magma
{
  struct DeviceMemoryDeleter
  {
    void operator()(vk::DeviceMemory const &fence)
    {
      auto device(getDevice());

      device.freeMemory(fence);
    }
  };

  template<class Deleter = DeviceMemoryDeleter>
  using DeviceMemory = claws::handle<vk::DeviceMemory, Deleter>;

  inline auto createDeviceMemory(vk::DeviceSize size, uint32_t typeIndex)
  {
    auto device(getDevice());

    return DeviceMemory<>(DeviceMemoryDeleter{}, device.allocateMemory({size, typeIndex}));
  }

  inline auto selectDeviceMemoryType(vk::DeviceSize size,
				     vk::MemoryPropertyFlags memoryFlags,
				     uint32_t memoryTypeIndexMask)
  {
    auto const memProperties(getPhysicalDevice().getMemoryProperties());

    for (uint32_t i(0u); i < memProperties.memoryTypeCount; ++i)
      {
        auto const &type(memProperties.memoryTypes[i]);

        if (((memoryTypeIndexMask >> i) & 1u) && (type.propertyFlags & memoryFlags) && memProperties.memoryHeaps[type.heapIndex].size >= size)
          return i;
      }
    throw std::runtime_error("Couldn't find proper memory type");
  }

  inline auto selectAndCreateDeviceMemory(vk::DeviceSize size,
					  vk::MemoryPropertyFlags memoryFlags,
					  uint32_t memoryTypeIndexMask)
  {
    return createDeviceMemory(size, selectDeviceMemoryType(size, memoryFlags, memoryTypeIndexMask));
  }
};

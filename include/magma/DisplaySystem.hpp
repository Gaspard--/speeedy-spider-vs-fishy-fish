#pragma once

#include <memory>
#include <fstream>
#include <iostream>
#include <mutex>

#include "magma/VulkanHandler.hpp"
#include "magma/Device.hpp"
#include "magma/Swapchain.hpp"
#include "magma/CommandBuffer.hpp"
#include "magma/Semaphore.hpp"
#include "magma/ImageView.hpp"
#include "magma/CreateInfo.hpp"

namespace magma {

  ///
  /// A class that helps with swapchain-related data segmentation
  ///
  /// @tparam UserData will ba available on swapchain creation
  /// @tparam SwachainUserData will be recreated everytime the swapchain gets recreated
  /// @tparam FrameUserData will be (re)created for each swapchain image
  ///
  /// `UserData` must have a member `UserData::getExtent()` returning a value convertible to vk::Extent2D
  template<class UserData, class SwapchainUserData, class FrameUserData>
  class DisplaySystem
  {
  private:
    magma::Surface<claws::no_delete> surface;
    vk::Queue queue;
  public:
    UserData userData;
  private:
    magma::Swapchain<> swapchain;
  public:
    SwapchainUserData swapchainUserData;
  private:

    struct FrameData
    {
      magma::ImageView<> swapchainImageView;
      FrameUserData userData;

      FrameData(magma::Swapchain<claws::no_delete> swapchain, UserData &userData, SwapchainUserData &swapchainUserData, vk::Image swapchainImage, uint32_t index)
	: swapchainImageView(createImageView({},
					     swapchainImage,
					     vk::ImageViewType::e2D,
					     swapchain.getFormat(),
	    {vk::ComponentSwizzle::eIdentity,
	     vk::ComponentSwizzle::eIdentity,
	     vk::ComponentSwizzle::eIdentity,
	     vk::ComponentSwizzle::eIdentity},
	    {vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1}))
	, userData(swapchain, userData, swapchainUserData, swapchainImageView, index)
      {
      }
    };

    std::vector<FrameData> frames;

    template<class T, class = void>
    struct CallExtentOrReturnDefault
    {
      vk::Extent2D operator()(T const &)
      {
	return vk::Extent2D{0xFFFFFFFF, 0xFFFFFFFF};
      }
    };

    template<class T>
    struct CallExtentOrReturnDefault<T, decltype(std::declval<T>().getExtent(), void(0))>
    {
      vk::Extent2D operator()(T const &t)
      {
	return t.getExtent();
      }
    };


  public:
    DisplaySystem(DisplaySystem const &) = delete;
    DisplaySystem(DisplaySystem &&) = delete;

    template<class... T>
    explicit DisplaySystem(magma::Surface<claws::no_delete> surface,
			   vk::Queue queue,
			   uint32_t queueFamilyIndex,
			   T &&... userDataParams);

    void recreateSwapchain();

    std::pair<uint32_t, FrameUserData &>  getImage(magma::Semaphore<claws::no_delete> toBeSignaled);

    void presentImage(magma::Semaphore<claws::no_delete> wait, uint32_t index);

    magma::Swapchain<claws::no_delete> getSwapchain()
    {
      return swapchain;
    }


    ~DisplaySystem()
    {
      getDevice().waitIdle();
    }
  };
}

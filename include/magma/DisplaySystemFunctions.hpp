namespace magma
{

  template<class UserData, class SwapchainUserData, class FrameUserData>
  template<class... T>
  DisplaySystem<UserData, SwapchainUserData, FrameUserData>::
  DisplaySystem(magma::Surface<claws::no_delete> surface,
		vk::Queue queue,
		uint32_t queueFamilyIndex,
		T &&... userDataParams)
    : surface(surface)
    , queue(queue)
    , userData(queueFamilyIndex, std::forward<T>(userDataParams)...)
  {
    recreateSwapchain();
  }

  template<class UserData, class SwapchainUserData, class FrameUserData>
  void DisplaySystem<UserData, SwapchainUserData, FrameUserData>::recreateSwapchain()
  {
    auto device(getDevice());

    swapchain = magma::Swapchain<>(surface, swapchain, CallExtentOrReturnDefault<UserData>{}(userData));
    device.waitIdle();
    auto const &swapchainImages(swapchain.getImages());
    swapchainUserData = SwapchainUserData(swapchain, userData, static_cast<uint32_t>(swapchainImages.size()));

    frames.clear();
    frames.reserve(swapchainImages.size());

    for (uint32_t i(0u); i < swapchainImages.size(); ++i)
      {
	static_assert(std::is_same_v<decltype((userData)), UserData &> && !std::is_const_v<UserData>);

	frames.emplace_back(swapchain, userData, swapchainUserData, swapchainImages[i], i);
      }
  }

  template<class UserData, class SwapchainUserData, class FrameUserData>
  std::pair<uint32_t, FrameUserData &> DisplaySystem<UserData, SwapchainUserData, FrameUserData>::getImage(magma::Semaphore<claws::no_delete> toBeSignaled)
  {
  retry:
    auto[result, index] = swapchain.getImageIndex(toBeSignaled, ~0ul, nullptr);

    switch (result)
      {
      case vk::Result::eSuccess:
      case vk::Result::eSuboptimalKHR:
	break;
      case vk::Result::eErrorOutOfDateKHR:
	recreateSwapchain();
	goto retry;
      default:
	throw std::runtime_error("Problem occurred during swap chain image acquisition!");
      };
    return {index, frames[index].userData};
  }

  template<class UserData, class SwapchainUserData, class FrameUserData>
  void DisplaySystem<UserData, SwapchainUserData, FrameUserData>::presentImage(magma::Semaphore<claws::no_delete> wait, uint32_t index)
  {
    VkPresentInfoKHR presentInfoRaw(magma::StructBuilder<vk::PresentInfoKHR>::make
				    (magma::asListRef(wait), magma::asListRef(swapchain.raw()), &index, nullptr));
    switch (vk::Result(vkQueuePresentKHR(queue, &presentInfoRaw)))
      {
      case vk::Result::eSuccess:
	break;
      case vk::Result::eSuboptimalKHR:
      case vk::Result::eErrorOutOfDateKHR:
	recreateSwapchain();
	break;
      default:
	throw std::runtime_error("Problem occurred during image presentation!");
      }
  }
}

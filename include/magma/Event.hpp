#pragma once

#include "magma/Deleter.hpp"
#include "magma/Device.hpp"

namespace magma
{
  template<class Deleter = Deleter>
  using Event = claws::handle<vk::Event, Deleter>;

  inline auto createEvent(void)
  {
    auto device(getDevice());

    Event<>(Deleter{}, device.createEvent({}));
  }
};

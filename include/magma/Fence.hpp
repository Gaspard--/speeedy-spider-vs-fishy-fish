#pragma once

#include "magma/Deleter.hpp"
#include "magma/Device.hpp"

namespace magma
{
  template<class Deleter = Deleter>
  using Fence = claws::handle<vk::Fence, Deleter>;

  inline auto createFence(vk::FenceCreateFlags flags)
  {
    auto device(getDevice());

    return Fence<>(Deleter{}, device.createFence({flags}));
  }
};

#pragma once

#include "magma/Deleter.hpp"
#include "magma/Device.hpp"
#include "magma/RenderPass.hpp"

namespace magma
{
  template<class Deleter = Deleter>
  using Framebuffer = claws::handle<vk::Framebuffer, Deleter>;

  inline auto createFramebuffer(
    RenderPass<claws::no_delete> renderPass, std::vector<vk::ImageView> const &attachements, uint32_t width, uint32_t height, uint32_t layers)
  {
    auto device(getDevice());

    return magma::Framebuffer<>(Deleter{},
				device.createFramebuffer({{}, renderPass,
							  static_cast<uint32_t>(attachements.size()),
							  attachements.data(), width, height, layers}));
  }
};

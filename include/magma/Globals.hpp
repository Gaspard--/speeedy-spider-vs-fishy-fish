#pragma once

#include "magma/Device.hpp"

namespace magma
{
  vk::Device &getDevice() noexcept;
  vk::PhysicalDevice &getPhysicalDevice() noexcept;

  struct MagmaState
  {
    vk::Device device;
    vk::PhysicalDevice physicalDevice;
  };

  // use this to inherit thread state
  MagmaState &getMagmaState() noexcept;
}

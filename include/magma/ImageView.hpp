#pragma once

#include "magma/Deleter.hpp"
#include "magma/Device.hpp"

namespace magma
{
  template<class Deleter = Deleter>
  using ImageView = claws::handle<vk::ImageView, Deleter>;

  inline auto createImageView(vk::ImageViewCreateFlags flags,
			      vk::Image image,
			      vk::ImageViewType type,
			      vk::Format format,
			      vk::ComponentMapping components,
			      vk::ImageSubresourceRange subresourceRange)
  {
    auto device(getDevice());

    return ImageView<>(Deleter{},
                       device.createImageView({flags, image, type, format, components, subresourceRange}));
  }
};

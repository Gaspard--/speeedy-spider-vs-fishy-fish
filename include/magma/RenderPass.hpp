#pragma once

#include "magma/Deleter.hpp"
#include "magma/Device.hpp"

namespace magma
{
  struct RenderPassCreateInfo
  {
    vk::RenderPassCreateFlags flags;
    std::vector<vk::AttachmentDescription> attachements;
    std::vector<vk::SubpassDescription> subpasses;
    std::vector<vk::SubpassDependency> subpassDependencies;

    RenderPassCreateInfo(vk::RenderPassCreateFlags flags)
      : flags(flags)
    {}

    operator vk::RenderPassCreateInfo() const
    {
      return {flags,
              static_cast<uint32_t>(attachements.size()),
              attachements.data(),
              static_cast<uint32_t>(subpasses.size()),
              subpasses.data(),
              static_cast<uint32_t>(subpassDependencies.size()),
              subpassDependencies.data()};
    }

    RenderPassCreateInfo() = default;
    RenderPassCreateInfo(RenderPassCreateInfo const &) = delete;
    RenderPassCreateInfo(RenderPassCreateInfo &&) = default;
    ~RenderPassCreateInfo() = default;
  };

  template<class Deleter = Deleter>
  using RenderPass = claws::handle<vk::RenderPass, Deleter>;

  inline auto createRenderPass(vk::RenderPassCreateInfo const &renderPassCreateInfo)
  {
    auto device(getDevice());

    return RenderPass<>(Deleter{}, device.createRenderPass(renderPassCreateInfo));
  }

  inline auto getRenderAreaGranularity(RenderPass<claws::no_delete> renderPass)
  {
    auto device(getDevice());

    return device.getRenderAreaGranularity(renderPass);
  }
};

#pragma once

#include "magma/Deleter.hpp"
#include "magma/Device.hpp"

namespace magma
{
  template<class Deleter = Deleter>
  using Semaphore = claws::handle<vk::Semaphore, Deleter>;

  inline auto createSemaphore()
  {
    auto device(getDevice());

    return Semaphore<>(Deleter{},
		       device.createSemaphore({}));
  }
};

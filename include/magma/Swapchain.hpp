#pragma once

#include <algorithm>

#include "VulkanFormatsHandler.hpp"
#include "VulkanHandler.hpp"
#include "Surface.hpp"
#include "Globals.hpp"

namespace magma
{
  namespace impl
  {
    class Swapchain
    {
    protected:
      vk::Format format;
      vk::Extent2D currentExtent;

    public:
      ~Swapchain() = default;

      struct SwapchainDeleter
      {
        friend class Swapchain;

        void operator()(Swapchain const &swapchain)
        {
	  auto device(getDevice());

          if (swapchain.vkSwapchain)
            device.destroySwapchainKHR(swapchain.vkSwapchain);
        }
      };
      vk::SwapchainKHR vkSwapchain;

      template<class CONTAINER>
      static auto chooseImageFormat(Surface const &surface, CONTAINER &&formatChoices)
      {
        auto formats(magma::getPhysicalDevice().getSurfaceFormatsKHR(surface.vkSurface));

        if (formats[0].format == vk::Format::eUndefined)
          {
            formats[0].format = vk::Format::eR8G8B8Srgb; // yay!
            return formats[0];
          }
        FormatGroup possibillities = ([&formatChoices](FormatGroup possibillities) {
          for (auto const &pick : formatChoices)
            {
              if (auto best = (possibillities & pick))
                return best;
            }
          throw std::runtime_error("Vulkan Swapchain: No suitable format found.");
        })(FormatGroup{claws::container_view(formats.begin(), formats.end(), [](auto const &format) noexcept { return format.format; })});
        return *std::find_if(formats.begin(), formats.end(), [&possibillities](auto const &format) noexcept { return possibillities[format.format]; });
      }

      Swapchain()
        : format{}
        , currentExtent{}
        , vkSwapchain(nullptr)
      {}

      // TODO: refactor the fuck out of this.
      Swapchain(Surface const &surface,
                claws::handle<Swapchain, claws::no_delete> old,
		vk::Extent2D desiredSize = vk::Extent2D{0xFFFFFFFFu, 0xFFFFFFFFu},
		uint32_t desiredFrameCount = 2)
      {
	auto physicalDevice = getPhysicalDevice();
        constexpr std::array<magma::FormatGroup, 5> preferedFormatRanking{
          {((vulkanFormatGroups::R16G16B16) & vulkanFormatGroups::Srgb),
           ((vulkanFormatGroups::R16G16B16A16) & vulkanFormatGroups::Srgb),
	   ((vulkanFormatGroups::R8G8B8 | vulkanFormatGroups::B8G8R8) & vulkanFormatGroups::Srgb),
           ((vulkanFormatGroups::R8G8B8A8 | vulkanFormatGroups::B8G8R8A8) & vulkanFormatGroups::Srgb),
           (vulkanFormatGroups::R8G8B8A8 | vulkanFormatGroups::B8G8R8A8 | vulkanFormatGroups::R8G8B8A8 | vulkanFormatGroups::B8G8R8A8)}};
        auto format(chooseImageFormat(surface, preferedFormatRanking));
        auto capabilities(physicalDevice.getSurfaceCapabilitiesKHR(surface.vkSurface));
        auto presentModes(physicalDevice.getSurfacePresentModesKHR(surface.vkSurface));

        this->format = format.format;

        constexpr std::array<vk::PresentModeKHR, 2> order{{vk::PresentModeKHR::eFifoRelaxed, vk::PresentModeKHR::eFifo}};
        auto resultIt(std::find_if(order.begin(), order.end(), [&presentModes](auto presentMode) noexcept {
          return (std::find(presentModes.begin(), presentModes.end(), presentMode) != presentModes.end());
        }));
        if (resultIt == order.end())
          throw std::runtime_error("Vulkan Swapchain: No suitable presentation mode found.");
        auto presentMode(*resultIt);

        if (!(capabilities.supportedUsageFlags & vk::ImageUsageFlagBits::eTransferDst))
          throw std::runtime_error("Vulkan Swapchain: Mising eTransferDst for clear operation.");

	this->currentExtent = (desiredSize != vk::Extent2D{0xFFFFFFFFu, 0xFFFFFFFFu}) ? desiredSize : capabilities.currentExtent; // choose asked size, or default to current
	this->currentExtent.width = std::min(this->currentExtent.width,  capabilities.maxImageExtent.width);
	this->currentExtent.height = std::min(this->currentExtent.height,  capabilities.maxImageExtent.height);

        vk::SwapchainCreateInfoKHR createInfo({},
                                              surface.vkSurface,
                                              std::min(std::max(capabilities.minImageCount, desiredFrameCount), capabilities.maxImageCount - !capabilities.maxImageCount),
                                              format.format,
                                              format.colorSpace,
                                              this->currentExtent,
                                              1,
                                              vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eColorAttachment,
                                              vk::SharingMode::eExclusive, // next 2 params unused because eExclusive
                                              0,                           // unused
                                              nullptr,                     // unused
                                              capabilities.currentTransform,
                                              vk::CompositeAlphaFlagBitsKHR::eOpaque,
                                              presentMode,
                                              true,
                                              old.vkSwapchain); // old swapchain

	auto device(getDevice());

        vkSwapchain = device.createSwapchainKHR(createInfo);
      }

      auto const &getExtent() const
      {
        return this->currentExtent;
      }

      auto getImages()
      {
	auto device(getDevice());

        return device.getSwapchainImagesKHR(vkSwapchain);
      }

      auto getFormat() const
      {
        return this->format;
      }

      auto getImageIndex(vk::Semaphore sem, uint64_t timeout, vk::Fence fence) const
      {
	auto device(getDevice());
        uint32_t index;

        auto result = vkAcquireNextImageKHR(device, vkSwapchain, timeout, sem, fence, &index);
        return std::make_tuple(vk::Result{result}, index);
      }

      auto &raw()
      {
        return (vkSwapchain);
      }

      void swap(Swapchain &other)
      {
        using std::swap;

        swap(format, other.format);
        swap(vkSwapchain, other.vkSwapchain);
        swap(currentExtent, other.currentExtent);
      }

      operator bool()
      {
        return vkSwapchain;
      }
    };

    inline void swap(Swapchain &lh, Swapchain &rh)
    {
      lh.swap(rh);
    }
  }

  template<class Deleter = impl::Swapchain::SwapchainDeleter>
  using Swapchain = claws::handle<impl::Swapchain, Deleter>;
};

#pragma once

#include <type_traits>
#include <utility>

namespace rider
{
  template<auto &getter>
  struct Bind
  {
  private:
    using Data = std::remove_reference_t<decltype(getter())>;
    Data prev;
  public:
    Bind(Data &&newData) noexcept
    {
      prev = std::move(getter());
      getter() = std::move(newData);
    }

    Bind() = delete;
    Bind(Bind const &) = delete;
    Bind(Bind &&other) noexcept
      : prev(std::move(other.prev))
    {
      other.prev = Data{};
    }

    ~Bind() noexcept
    {
      if (prev)
	getter() = std::move(prev);
    }
  };
}

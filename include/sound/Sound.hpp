#pragma once
#include <soundio/soundio.h>

#include <exception>
#include <memory>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

namespace soundIo
{
  struct SoundIoError
  {
    int id;

    operator std::runtime_error() const
    {
      return std::runtime_error(std::string(soundio_strerror(id)));
    }
  };

  struct Instance : public std::unique_ptr<SoundIo, decltype(&soundio_destroy)>
  {
    Instance()
      : std::unique_ptr<SoundIo, decltype(&soundio_destroy)>(soundio_create(), &soundio_destroy)
    {
      if (!*this)
	throw std::runtime_error("soundIo: out of memory!");
    }

    void connect()
    {
      if (int err = soundio_connect(&**this))
	throw SoundIoError{err};
      // soundio_flush_events(&**this);
    }
  };

  struct Device : public std::unique_ptr<SoundIoDevice, decltype(&soundio_device_unref)>
  {
    Device(Instance &instance, int index)
      : std::unique_ptr<SoundIoDevice, decltype(&soundio_device_unref)>(soundio_get_output_device(&*instance, index), &soundio_device_unref)
    {
      if (!*this)
	throw std::runtime_error("soundIo: out of memory!");
    }

    Device(Instance &instance)
      : Device(instance, [](auto &instance)
	       {
		 int default_out_device_index(soundio_default_output_device_index(&*instance));

		 if (default_out_device_index < 0)
		   throw std::runtime_error("soundIo: no output device found\n");
		 std::cout << "selected index " << default_out_device_index << std::endl;
		 return default_out_device_index;
	       }(instance))
    {
    }
  };

  template<class OP>
  struct OutStream : public std::unique_ptr<SoundIoOutStream, decltype(&soundio_outstream_destroy)>
  {
    OP op;

    OutStream(Device &device, SoundIoFormat format, OP &&op)
      : std::unique_ptr<SoundIoOutStream, decltype(&soundio_outstream_destroy)>(soundio_outstream_create(&*device), &soundio_outstream_destroy)
      , op(std::forward<OP>(op))
    {
      if (!*this)
	throw std::runtime_error("soundIo: out of memory!");
      (*this)->format = format;
      (*this)->userdata = reinterpret_cast<void *>(this);
      (*this)->write_callback = [](SoundIoOutStream *outstream, int frame_count_min, int frame_count_max)
	{
	  auto &that(*reinterpret_cast<OutStream *>(outstream->userdata));

	  that.op(that, frame_count_min, frame_count_max);
	};
      if (int err = soundio_outstream_open(&**this))
	throw SoundIoError{err};
      // if ((*this)->layout_error)
      // 	throw SoundIoError{(*this)->layout_error};
      // std::cout << ":)" << std::endl;
    }

    void start()
    {
      if (int err = soundio_outstream_start(&**this))
	throw SoundIoError{err};
    }

    auto getWriteLock(SoundIoChannelArea *&areas, int &frame_count)
    {
      struct SoundIoStreamWriteLock
      {
	OutStream *out;

	SoundIoStreamWriteLock(OutStream *out)
	  : out(out)
	{
	}


	SoundIoStreamWriteLock(SoundIoStreamWriteLock &&other)
	  : out(other.out)
	{
	  other.out = nullptr;
	}

	~SoundIoStreamWriteLock() noexcept(false)
	{
	  if (out && !std::uncaught_exceptions())
	    if (int err = soundio_outstream_end_write(&**out))
	      throw SoundIoError{err};
	}
      };
      if (int err = soundio_outstream_begin_write(&**this, &areas, &frame_count))
	throw SoundIoError{err};

      return SoundIoStreamWriteLock{&*this};
    }
  };


  class Sound
  {
    soundIo::Instance soundIo;
    soundIo::Device device;

    struct Sin
    {
      float seconds_offset{0.0f};

      void operator()(soundIo::OutStream<Sin> &outstream, int // frame_count_min
		      , int frame_count_max)
      {
	SoundIoChannelLayout const &layout(outstream->layout);
	auto const float_sample_rate(static_cast<float>(outstream->sample_rate));
	float const seconds_per_frame(1.0f / float_sample_rate);
	SoundIoChannelArea *areas;
	int frames_left(frame_count_max);

	while (frames_left > 0) {
	  int frame_count(frames_left);
	  auto write_lock(outstream.getWriteLock(areas, frame_count));

	  if (!frame_count)
	    break;

	  constexpr float pitch(440.0f);
	  float radians_per_second = pitch * 2.0f * static_cast<float>(M_PI);

	  for (int frame(0); frame < frame_count; ++frame)
	    {
	      auto fframe(static_cast<float>(frame));
	      float sample((std::sin((seconds_offset + fframe * seconds_per_frame) * radians_per_second)
			    + std::sin((seconds_offset + fframe * seconds_per_frame) * radians_per_second * 0.5f)) * 0.5f);

	      for (int channel(0); channel < layout.channel_count; ++channel) {
		*reinterpret_cast<float *>(areas[channel].ptr + areas[channel].step * frame) = sample;
	      }
	    }
	  seconds_offset = std::fmod(seconds_offset + seconds_per_frame * static_cast<float>(frame_count), 1.0f);
	  frames_left -= frame_count;
	}

      }
    };

    soundIo::OutStream<Sin> outstream;

  public:
    Sound()
      : soundIo()
      , device((soundIo.connect(), soundio_flush_events(&*soundIo), soundIo))
      , outstream(device, SoundIoFormatFloat32NE, Sin{})
    {
      outstream.start();
      for (;;)
	soundio_wait_events(&*soundIo);
    }
  };
};

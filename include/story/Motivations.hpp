#ragma once

namespace story
{
  struct Stats
  {
    int8_t confidence; // initial power attributed to self
    int8_t niceness; // initial relation with anyone
    uint8_t fear; // how much threats to self are weighted [1, 4[
    uint8_t trust; // how much trust is given to others [1, 4[
    // TODO:
    // uint8_t anger; // how much vengeance is persued
  };

  using Other = unsigned int;

  struct Relation
  {
    int8_t power; // percieved power of first over second
    int8_t freindship; // 0 - 92 hated, 92 - 160 neutral, 160 - 256 liked
  };

  using Relations = std::unordered_map<std::pair<Other, Other>, Relation> relations;

  struct Status
  {
    Stats stats;
    std::map<Other, Relation> relations;

    int32_t relationValue(Relation const &relation) const
    {
      return (stats.trust * relation.freindship * (1 << std::max(stats.fear * relation.power + 8, 8)));

      if (relation.fraindship <= 0) // be scared of non-freinds
	return (relation.freindship - 1) * relation.power * stats.fear;

    }

    int32_t relationValue(Other other) const
    {
      return relationValue(relations[other]);
    }
  };

  struct IndirectStatus
  {
    Status status;
    std::map<Other, std::map<Other, Relation>> otherStatuses;

    int64_t relationValue(Other target)
    {
      int64_t result(status.relationValue(target));

      for (auto [other, otherStatus] : otherStatuses)
	if (other != target)
	  result += int64_t(status.relationValue(otherStatus[target])) * int64_t(status.relationValue(other));
      return result;
    }
  };
};

#version 450

layout (input_attachment_index = 0, set = 0, binding = 0) uniform subpassInput inputColor;
layout (location = 0) out vec4 outputColor;

void main()
{
  outputColor.rgb = subpassLoad(inputColor).rgb;
  outputColor.r = pow(outputColor.r, 2.2);
  outputColor.g = pow(outputColor.g, 2.2);
  outputColor.b = pow(outputColor.b, 2.2);
  outputColor.a = 1.0f;
}
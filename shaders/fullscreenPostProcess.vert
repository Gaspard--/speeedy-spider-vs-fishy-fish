#version 450

void main()
{
  vec2 pos[3] = vec2[3](vec2(-1.0f, -1.0f), vec2(-1.0f, 3.0f), vec2(3.0f, -1.0f));
	
  gl_Position = vec4(pos[gl_VertexIndex], 0.0f, 1.0f);
}

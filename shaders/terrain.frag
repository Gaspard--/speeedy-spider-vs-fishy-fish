#version 450

layout(location = 0) in vec2 pos;
layout(location = 0) out vec4 vertColor;

float test(vec2 uv)
{
  vec2 val = sin(3.1234 * uv) + sin(1.53546 * uv + uv.yx);
  return val.x * val.y;
}

void main()
{
  vertColor = vec4(test(pos * 20.0f), test(pos * 15.0f), test(pos * 10.0f), 1.0f);
}

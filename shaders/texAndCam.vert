#version 450

layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 tex;
layout(location = 2) in vec2 size;
layout(location = 3) in vec4 transform;

layout(push_constant) uniform Camera
{
  vec2 offset;
  vec2 rotation;
} cam;

layout(constant_id = 0) const float xscale = 1.0; // window x scaling
layout(constant_id = 1) const float yscale = 1.0; // window y scaling


layout(location = 0) out vec2 vertTex;

out gl_PerVertex
{
  vec4 gl_Position;
};

void main()
{
  const vec2 scale = vec2(xscale, -yscale);

  vertTex = tex;
  vec2 outPos = pos;
  outPos *= size;
  outPos = outPos * transform.zz + outPos.yx * transform.ww * vec2(-1.0f, 1.0f);
  outPos += transform.xy;
  outPos = outPos * cam.rotation.xx + outPos.yx * cam.rotation.yy * vec2(-1.0, 1.0);
  outPos += cam.offset;
  gl_Position = vec4(outPos * scale, 0.0, 1.0);
}

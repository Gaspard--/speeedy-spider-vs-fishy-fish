#version 450

layout(set = 0, binding = 0) uniform sampler2D texSampler;
layout(location = 0) in vec2 vertTex;
layout(location = 0) out vec4 outColor;

layout(push_constant) uniform Mask
{
  layout(offset = 16) vec4 mask;
} mask;



void main() {
  outColor = texture(texSampler, vertTex).rgba * mask.mask;
}
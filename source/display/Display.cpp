#include "display/Display.hpp"
#include "logic/Logic.hpp"
#include "display/RenderInfo.hpp"
#include "logic/Collision.hpp"
#include "logic/ColorComponent.hpp"
#include "logic/PictureComponent.hpp"
#include "logic/FishyComponent.hpp"

#include "display/Image.hpp"

#include "loaders/load_image.hpp"

#include <cassert>
#include <deque>

namespace
{
  static magma::ShaderModule<> loadShader(char const *path)
  {
    std::ifstream source(path);

    if (!source)
      throw std::runtime_error(std::string("Failed to load shader: ") + path);
    return magma::createShaderModule(static_cast<std::istream &>(source));
  }
}

namespace display
{
  struct AnimImageList
  {
    std::vector<Image> images;
    size_t baseDescriptorOffset;
  };

  void Display::UserData::loadImages(uint32_t queueFamilyIndex)
  {
    size_t baseDescriptorOffset = 0;
    {
      auto commandPool = magma::createCommandPool({vk::CommandPoolCreateFlagBits::eResetCommandBuffer}, queueFamilyIndex);
      auto cmdList = commandPool.allocatePrimaryCommandBuffers(1);
      auto cmd = cmdList[0];

    

      char const * paths[] = {
			      "resource/sand",
			      "resource/border",
			      "resource/intro",
			      "resource/gameOver",
			      "resource/win",
			      "resource/lilyPad01",
			      "resource/lilyPad02",
			      "resource/lilyPad03",
			      "resource/lilyPad04",
			      "resource/lilyPad05",
			      "resource/bullet",
			      "resource/speedySpider",
			      "resource/fishyFish",
			      "resource/hpBar",
			      "resource/wave",
      };

      cmd.begin(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);

      animImageList.reserve(sizeof(paths) / sizeof(*paths));
      for (size_t i = 0; i != sizeof(paths) / sizeof(*paths); ++i)
	{
	  animImageList.push_back({{}, baseDescriptorOffset});
	  auto pics = loaders::loadPicsFromDirectory(paths[i]);

	  assert(pics.size());
	  animImageList.back().images.reserve(pics.size());
	  baseDescriptorOffset += pics.size();
	  for (auto const &[data, size] : pics)
	    {
	      magma::DynamicBuffer dynBuffer =
		{
		 {},
		 vk::BufferUsageFlagBits::eTransferSrc,
		 vk::MemoryPropertyFlagBits::eHostVisible,
		 std::vector<uint32_t>{queueFamilyIndex}
		};

	      std::cout << "loading image " << size[0]  << ", "  << size[1] << "\n";

	      animImageList.back().images.emplace_back(size[0], size[1], vk::Format::eR8G8B8A8Unorm, vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled);
	      animImageList.back().images.back().uploadData(cmd, dynBuffer, &data[0], magma::getDevice().getQueue(queueFamilyIndex, 0));
	    }
	}
    }
 
    pictureSampler = magma::createSampler(
					  // {},
					  vk::Filter::eLinear,
					  vk::Filter::eLinear,
					  vk::SamplerMipmapMode::eNearest,
					  vk::SamplerAddressMode::eClampToEdge,
					  vk::SamplerAddressMode::eClampToEdge,
					  vk::SamplerAddressMode::eClampToEdge,
					  0.0f,
					  false,
					  0.0f,
					  false,
					  {},
					  0.0f,
					  0.0f,
					  vk::BorderColor::eFloatTransparentBlack,
					  false);

    repeatSampler = magma::createSampler(
					  // {},
					  vk::Filter::eLinear,
					  vk::Filter::eLinear,
					  vk::SamplerMipmapMode::eNearest,
					  vk::SamplerAddressMode::eRepeat,
					  vk::SamplerAddressMode::eRepeat,
					  vk::SamplerAddressMode::eRepeat,
					  0.0f,
					  false,
					  0.0f,
					  false,
					  {},
					  0.0f,
					  0.0f,
					  vk::BorderColor::eFloatTransparentBlack,
					  false);

    std::vector<vk::DescriptorImageInfo> imageInfos;

    bool first = true;
    for (auto &animImages : animImageList)
      for (auto const &image : animImages.images)
	{
	  imageInfos.push_back(vk::DescriptorImageInfo{first ? repeatSampler : pictureSampler, image.getView(), vk::ImageLayout::eShaderReadOnlyOptimal});
	  first = false;
	}

    assert(imageInfos.size() == baseDescriptorOffset);

    pictureDescriptorSetLayout =
      magma::createDescriptorSetLayout
      ({
	vk::DescriptorSetLayoutBinding{0, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment, nullptr}
      });

    pictureDescriptorPool = magma::createDescriptorPool(uint32_t(imageInfos.size()), {{vk::DescriptorType::eCombinedImageSampler, uint32_t(imageInfos.size())}});
    std::vector<vk::DescriptorSetLayout> setLayouts{};

    setLayouts.resize(imageInfos.size(), pictureDescriptorSetLayout);
    pictureDescriptorSets = pictureDescriptorPool.allocateDescriptorSets(setLayouts);
    std::vector<vk::WriteDescriptorSet> writes{};

    writes.reserve(imageInfos.size());
    for (size_t i = 0; i != imageInfos.size(); ++i)
      writes.push_back({pictureDescriptorSets[i],
			0,
			0,
			1,
			vk::DescriptorType::eCombinedImageSampler,
			&imageInfos[i],
			nullptr,
			nullptr});
    magma::updateDescriptorSets(writes);
  }
  
  FramebufferConfig::FramebufferConfig(magma::RenderPassCreateInfo const &renderPassCreateInfo)
  {
    for (auto const &attachement : renderPassCreateInfo.attachements)
      imageConfigs.push_back(ImageConfig{attachement.format, {}, {}});

    for (auto const &subpass : renderPassCreateInfo.subpasses)
      {
	for (uint32_t i = 0; i < subpass.inputAttachmentCount; ++i)
	  {
	    auto const &inputAttachment(subpass.pInputAttachments[i]);

	    imageConfigs[inputAttachment.attachment].flags |= vk::ImageUsageFlagBits::eInputAttachment;
	  }
	for (uint32_t i = 0; i < subpass.colorAttachmentCount; ++i)
	  {
	    auto const &colorAttachment(subpass.pColorAttachments[i]);

	    imageConfigs[colorAttachment.attachment].flags |= vk::ImageUsageFlagBits::eColorAttachment;
	  }
	if (auto *depthStencilAttachment = subpass.pDepthStencilAttachment)
	  {
	    imageConfigs[depthStencilAttachment->attachment].flags |= vk::ImageUsageFlagBits::eDepthStencilAttachment;
	  }
      }
  }

  FramebufferConfig &FramebufferConfig::setImage(uint32_t index, magma::ImageView<claws::no_delete> imageView) noexcept
  {
    imageConfigs[index].imageView = imageView;
    return *this;
  }

  magma::Framebuffer<> FramebufferConfig::createFrameBuffer(magma::RenderPass<claws::no_delete> renderPass, uint32_t width, uint32_t height, std::vector<Image> &imageStorage) const
  {
    std::vector<vk::ImageView> imageViews;

    for (auto const &imageConfig : imageConfigs)
      {
	if (imageConfig.imageView)
	  {
	    imageViews.push_back(imageConfig.imageView);
	  }
	else
	  {
	    imageStorage.emplace_back(width, height, imageConfig.format, imageConfig.flags);
	    imageViews.push_back(imageStorage.back().getView());
	  }
      }
    return magma::createFramebuffer(renderPass,
				    imageViews,
				    width,
				    height,
				    1);
  }

  class Framebuffer
  {
    std::vector<Image> images;
    magma::Framebuffer<> framebuffer;

  public:
    Framebuffer(magma::RenderPass<claws::no_delete> renderpass, uint32_t width, uint32_t height, FramebufferConfig const &config)
      : framebuffer(config.createFrameBuffer(renderpass, width, height, images))
    {
    }

    vk::ImageView getImageView(uint32_t index) const noexcept
    {
      return images[index].getView();
    }

    magma::Framebuffer<claws::no_delete> getFrameBuffer()
    {
      return framebuffer;
    }
  };

  struct FrameUserData
  {
    Framebuffer framebuffer;
    magma::Fence<> fence;
    std::vector<magma::DynamicBuffer::RangeId> ranges;

    FrameUserData(magma::Swapchain<claws::no_delete> swapchain, Display::UserData &, Display::SwapchainUserData &swapchainUserData, magma::ImageView<claws::no_delete> swapchainImageView, uint32_t index)
      : framebuffer(swapchainUserData.renderPass,
		    swapchain.getExtent().width,
		    swapchain.getExtent().height,
		    swapchainUserData.framebufferConfig.setImage(1, swapchainImageView))
      , fence(magma::createFence({vk::FenceCreateFlagBits::eSignaled}))
    {
      vk::DescriptorImageInfo imageInfo{nullptr, framebuffer.getImageView(0), vk::ImageLayout::eShaderReadOnlyOptimal};
      magma::updateDescriptorSets(std::array<vk::WriteDescriptorSet, 1>{{{
	      swapchainUserData.descriptorSets[index],
		0,
		0,
		1,
		vk::DescriptorType::eInputAttachment,
		&imageInfo,
		nullptr,
		nullptr}}});
    }
  };
}

#include "magma/DisplaySystemFunctions.hpp"

namespace display
{
  Display::UserData::UserData(uint32_t selectedQueueFamily)
  {
    loadImages(selectedQueueFamily);

    vertShaderModule = loadShader("shaders/colorAndCam.vert.spirv");
    positionOnlyVertShaderModule = loadShader("shaders/positionOnly.vert.spirv");
    fragShaderModule = loadShader("shaders/color.frag.spirv");
    fullscreenVertShaderModule = loadShader("shaders/fullscreenPostProcess.vert.spirv");
    fullscreenFragShaderModule = loadShader("shaders/fullscreenPostProcess.frag.spirv");
    pipelineLayout = magma::createPipelineLayout({}, {}, {vk::PushConstantRange{vk::ShaderStageFlagBits::eVertex, 0, sizeof(float) * 4u}});
    {
      descriptorSetLayout = magma::createDescriptorSetLayout({
	  vk::DescriptorSetLayoutBinding{0, vk::DescriptorType::eInputAttachment, 1, vk::ShaderStageFlagBits::eFragment, nullptr}
	});
      fullscreenPipelineLayout = magma::createPipelineLayout({}, {descriptorSetLayout}, {});
    }
    commandPool = magma::createCommandPool({vk::CommandPoolCreateFlagBits::eResetCommandBuffer}, selectedQueueFamily);
    imageAvailable = magma::createSemaphore();
    renderDone = magma::createSemaphore();
    dynamicBuffer = magma::DynamicBuffer({},
					 vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eIndexBuffer,
					 vk::MemoryPropertyFlagBits::eHostVisible,
					 std::vector<uint32_t>{selectedQueueFamily});
    vertTextureShaderModule = loadShader("shaders/texAndCam.vert.spirv");
    fragTextureShaderModule = loadShader("shaders/texture.frag.spirv");
    texturePipelineLayout = magma::createPipelineLayout({}, {pictureDescriptorSetLayout}, {vk::PushConstantRange{vk::ShaderStageFlagBits::eVertex, 0, sizeof(float) * 4u}, vk::PushConstantRange{vk::ShaderStageFlagBits::eFragment, sizeof(float) * 4u, sizeof(float) * 4u}});
  }

  enum RenderPassSubPass : uint32_t
    {
     world,
     compose
    };


  Display::SwapchainUserData::SwapchainUserData(magma::Swapchain<claws::no_delete> swapchain, UserData const &perSurfaceData, uint32_t imageCount)
    : dim{0, 0}
  {
    {
      magma::RenderPassCreateInfo renderPassCreateInfo{{}};

      renderPassCreateInfo.attachements.push_back({{},
						   vk::Format::eR32G32B32A32Sfloat,
						   vk::SampleCountFlagBits::e1,
						   vk::AttachmentLoadOp::eClear,
						   vk::AttachmentStoreOp::eStore,
						   vk::AttachmentLoadOp::eDontCare,
						   vk::AttachmentStoreOp::eDontCare,
						   vk::ImageLayout::eUndefined,
						   vk::ImageLayout::eShaderReadOnlyOptimal});

      renderPassCreateInfo.attachements.push_back({{},
						   swapchain.getFormat(),
						   vk::SampleCountFlagBits::e1,
						   vk::AttachmentLoadOp::eClear,
						   vk::AttachmentStoreOp::eStore,
						   vk::AttachmentLoadOp::eDontCare,
						   vk::AttachmentStoreOp::eDontCare,
						   vk::ImageLayout::eUndefined,
						   vk::ImageLayout::ePresentSrcKHR});


      vk::AttachmentReference colorAttachmentReferences(0, vk::ImageLayout::eColorAttachmentOptimal);
      vk::AttachmentReference colorAttachmentReferences_read(0, vk::ImageLayout::eShaderReadOnlyOptimal);
      vk::AttachmentReference outputAttachmentReferences(1, vk::ImageLayout::eColorAttachmentOptimal);

      renderPassCreateInfo.subpasses.push_back(magma::makeCreateInfoNoFlags<vk::SubpassDescription>(vk::PipelineBindPoint::eGraphics,
												    magma::EmptyList{},
												    magma::asListRef(colorAttachmentReferences),
												    nullptr,
												    nullptr,
												    magma::EmptyList{}));
      renderPassCreateInfo.subpasses.push_back(magma::makeCreateInfoNoFlags<vk::SubpassDescription>(vk::PipelineBindPoint::eGraphics,
												    magma::asListRef(colorAttachmentReferences_read),
												    magma::asListRef(outputAttachmentReferences),
												    nullptr,
												    nullptr,
												    magma::EmptyList{}));

      renderPassCreateInfo.subpassDependencies.push_back(vk::SubpassDependency
							 {
							   RenderPassSubPass::world,
							     RenderPassSubPass::compose,
							     vk::PipelineStageFlagBits::eColorAttachmentOutput,
							     vk::PipelineStageFlagBits::eFragmentShader,
							     vk::AccessFlagBits::eColorAttachmentWrite,
							     vk::AccessFlagBits::eInputAttachmentRead,
							       {}
							     });

      framebufferConfig = FramebufferConfig(renderPassCreateInfo);
      renderPass = magma::createRenderPass(renderPassCreateInfo);
    }
    commandBuffers = perSurfaceData.commandPool.allocatePrimaryCommandBuffers(imageCount);
    fullscreenPipeline = createPostProcessPipeline(swapchain, perSurfaceData);
    descriptorPool = magma::createDescriptorPool(imageCount, {{vk::DescriptorType::eInputAttachment, imageCount}});
    std::vector<vk::DescriptorSetLayout> sets;

    sets.resize(imageCount, perSurfaceData.descriptorSetLayout);
    descriptorSets = descriptorPool.allocateDescriptorSets(sets);
    texturePipeline = createTexturePipeline(swapchain, perSurfaceData);
  }

  magma::Pipeline<> Display::SwapchainUserData::createPostProcessPipeline(magma::Swapchain<claws::no_delete> swapchain, UserData const &userData)
  {
    std::vector<vk::PipelineShaderStageCreateInfo>
      shaderStageCreateInfos{{{}, vk::ShaderStageFlagBits::eVertex, userData.fullscreenVertShaderModule, "main", nullptr},
			     {{}, vk::ShaderStageFlagBits::eFragment, userData.fullscreenFragShaderModule, "main", nullptr}};

    auto vertexInputStateCreateInfo(magma::makeCreateInfoNoFlags<vk::PipelineVertexInputStateCreateInfo>(magma::EmptyList{}, magma::EmptyList{}));

    vk::PipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo{{}, vk::PrimitiveTopology::eTriangleList, false};

    vk::Viewport viewport(0.0f,
			  0.0f, // pos
			  static_cast<float>(swapchain.getExtent().width),
			  static_cast<float>(swapchain.getExtent().height), // size
			  0.0f,
			  1.0); // depth range

    vk::Rect2D scissor({0, 0}, swapchain.getExtent());

    auto viewportStateCreateInfo(magma::makeCreateInfoNoFlags<vk::PipelineViewportStateCreateInfo>(magma::asListRef(viewport),
												   magma::asListRef(scissor)));

    vk::PipelineRasterizationStateCreateInfo rasterizationStateCreateInfo{
      {},
	false,                            // VkBool32                                       depthClampEnable
	  false,                            // VkBool32                                       rasterizerDiscardEnable
	  vk::PolygonMode::eFill,           // VkPolygonMode                                  polygonMode
	  vk::CullModeFlagBits::eBack,      // VkCullModeFlags                                cullMode
	  vk::FrontFace::eCounterClockwise, // VkFrontFace                                    frontFace
	  false,                            // VkBool32                                       depthBiasEnable
	  0.0f,                             // float                                          depthBiasConstantFactor
	  0.0f,                             // float                                          depthBiasClamp
	  0.0f,                             // float                                          depthBiasSlopeFactor
	  1.0f                              // float                                          lineWidth
	  };

    vk::PipelineMultisampleStateCreateInfo multisampleStateCreateInfo{
      {},
	vk::SampleCountFlagBits::e1, // VkSampleCountFlagBits                          rasterizationSamples
	  false,                       // VkBool32                                       sampleShadingEnable
	  1.0f,                        // float                                          minSampleShading
	  nullptr,                     // const VkSampleMask                            *pSampleMask
	  false,                       // VkBool32                                       alphaToCoverageEnable
	  false                        // VkBool32                                       alphaToOneEnable
	  };

    vk::PipelineColorBlendAttachmentState
      colorBlendAttachmentState{false,                  // VkBool32                                       blendEnable
	vk::BlendFactor::eOne,  // VkBlendFactor                                  srcColorBlendFactor
	vk::BlendFactor::eZero, // VkBlendFactor                                  dstColorBlendFactor
	vk::BlendOp::eAdd,      // VkBlendOp                                      colorBlendOp
	vk::BlendFactor::eOne,  // VkBlendFactor                                  srcAlphaBlendFactor
	vk::BlendFactor::eZero, // VkBlendFactor                                  dstAlphaBlendFactor
	vk::BlendOp::eAdd,      // VkBlendOp                                      alphaBlendOp
	vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB
	| vk::ColorComponentFlagBits::eA};

    vk::PipelineColorBlendStateCreateInfo colorBlendStateCreateInfo{
      {},                 // VkPipelineColorBlendStateCreateFlags           flags
	false,              // VkBool32                                       logicOpEnable
	  vk::LogicOp::eCopy, // VkLogicOp                                      logicOp
	  1,
	  &colorBlendAttachmentState, // const VkPipelineColorBlendAttachmentState     *pAttachments
	    {0.0f, 0.0f, 0.0f, 0.0f}    // float                                          blendConstants[4]
    };

    magma::GraphicsPipelineConfig pipelineCreateInfo({},
						     std::move(shaderStageCreateInfos),
						     vertexInputStateCreateInfo,
						     inputAssemblyStateCreateInfo,
						     rasterizationStateCreateInfo,
						     userData.fullscreenPipelineLayout,
						     renderPass,
						     RenderPassSubPass::compose);

    pipelineCreateInfo.addRasteringColorAttachementInfo(viewportStateCreateInfo, multisampleStateCreateInfo, colorBlendStateCreateInfo);
    return magma::createPipeline(pipelineCreateInfo);
  }

  magma::Pipeline<> Display::SwapchainUserData::createPipeline(magma::Swapchain<claws::no_delete> swapchain, UserData const &userData, renderMode::RenderMode const &renderMode)
  {
    float val;

    vk::SpecializationMapEntry mapEntry(swapchain.getExtent().width < swapchain.getExtent().height, 0, sizeof(val));
    if (!mapEntry.constantID)
      val = static_cast<float>(swapchain.getExtent().height) / static_cast<float>(swapchain.getExtent().width);
    else
      val = static_cast<float>(swapchain.getExtent().width) / static_cast<float>(swapchain.getExtent().height);
    dim = {1.0f, 1.0f};
    dim[mapEntry.constantID] /= val;

    auto specialzationInfo(magma::makeCreateInfo<vk::SpecializationInfo>(magma::asListRef(mapEntry), sizeof(val), &val));
    std::vector<vk::PipelineShaderStageCreateInfo>
      shaderStageCreateInfos{{{}, vk::ShaderStageFlagBits::eVertex, userData.vertShaderModule, "main", &specialzationInfo},
	{{}, vk::ShaderStageFlagBits::eFragment, userData.fragShaderModule, "main", nullptr}};

    std::array<vk::VertexInputBindingDescription, 1u> vertexInputBindings{
      vk::VertexInputBindingDescription{0, 6 * sizeof(float), vk::VertexInputRate::eVertex}};
    std::array<vk::VertexInputAttributeDescription, 2u>
      vertexInputAttrib{vk::VertexInputAttributeDescription{0, vertexInputBindings[0].binding, vk::Format::eR32G32Sfloat, 0},
			vk::VertexInputAttributeDescription{1, vertexInputBindings[0].binding, vk::Format::eR32G32B32A32Sfloat, 2 * sizeof(float)}};
    auto vertexInputStateCreateInfo(magma::makeCreateInfoNoFlags<vk::PipelineVertexInputStateCreateInfo>(vertexInputBindings,
													 vertexInputAttrib));

    vk::PrimitiveTopology topology;
    switch (renderMode.primitiveType)
      {
      case renderMode::PrimitiveType::Line:
	topology = vk::PrimitiveTopology::eLineList;
	break;
      case renderMode::PrimitiveType::Triangle:
	topology = vk::PrimitiveTopology::eTriangleList;
	break;
      default:
	assert(!"Unhandled primitive type!");
      }
    vk::PipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo{{}, topology, false};

    vk::Viewport viewport(0.0f,
			  0.0f, // pos
			  static_cast<float>(swapchain.getExtent().width),
			  static_cast<float>(swapchain.getExtent().height), // size
			  0.0f,
			  1.0); // depth range

    vk::Rect2D scissor({0, 0}, swapchain.getExtent());

    auto viewportStateCreateInfo(magma::makeCreateInfoNoFlags<vk::PipelineViewportStateCreateInfo>(magma::asListRef(viewport),
												   magma::asListRef(scissor)));

    vk::PipelineRasterizationStateCreateInfo rasterizationStateCreateInfo{
      {},
	false,                            // VkBool32                                       depthClampEnable
	  false,                            // VkBool32                                       rasterizerDiscardEnable
	  vk::PolygonMode::eFill,           // VkPolygonMode                                  polygonMode
	  vk::CullModeFlagBits::eBack,      // VkCullModeFlags                                cullMode
	  vk::FrontFace::eCounterClockwise, // VkFrontFace                                    frontFace
	  false,                            // VkBool32                                       depthBiasEnable
	  0.0f,                             // float                                          depthBiasConstantFactor
	  0.0f,                             // float                                          depthBiasClamp
	  0.0f,                             // float                                          depthBiasSlopeFactor
	  1.0f                              // float                                          lineWidth
	  };

    vk::PipelineMultisampleStateCreateInfo multisampleStateCreateInfo{
      {},
	vk::SampleCountFlagBits::e1, // VkSampleCountFlagBits                          rasterizationSamples
	  false,                       // VkBool32                                       sampleShadingEnable
	  1.0f,                        // float                                          minSampleShading
	  nullptr,                     // const VkSampleMask                            *pSampleMask
	  false,                       // VkBool32                                       alphaToCoverageEnable
	  false                        // VkBool32                                       alphaToOneEnable
	  };

    vk::PipelineColorBlendAttachmentState
      colorBlendAttachmentState{true,                  // VkBool32                                       blendEnable
	vk::BlendFactor::eSrcAlpha,  // VkBlendFactor                                  srcColorBlendFactor
	vk::BlendFactor::eOneMinusSrcAlpha, // VkBlendFactor                                  dstColorBlendFactor
	vk::BlendOp::eAdd,      // VkBlendOp                                      colorBlendOp
	vk::BlendFactor::eOne,  // VkBlendFactor                                  srcAlphaBlendFactor
	vk::BlendFactor::eZero, // VkBlendFactor                                  dstAlphaBlendFactor
	vk::BlendOp::eAdd,      // VkBlendOp                                      alphaBlendOp
	vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA};

    vk::PipelineColorBlendStateCreateInfo colorBlendStateCreateInfo{
      {},                 // VkPipelineColorBlendStateCreateFlags           flags
	false,              // VkBool32                                       logicOpEnable
	  vk::LogicOp::eCopy, // VkLogicOp                                      logicOp
	  1,
	  &colorBlendAttachmentState, // const VkPipelineColorBlendAttachmentState     *pAttachments
	    {0.0f, 0.0f, 0.0f, 0.0f}    // float                                          blendConstants[4]
    };

    magma::GraphicsPipelineConfig pipelineCreateInfo({},
						     std::move(shaderStageCreateInfos),
						     vertexInputStateCreateInfo,
						     inputAssemblyStateCreateInfo,
						     rasterizationStateCreateInfo,
						     userData.pipelineLayout,
						     renderPass,
						     RenderPassSubPass::world);

    pipelineCreateInfo.addRasteringColorAttachementInfo(viewportStateCreateInfo, multisampleStateCreateInfo, colorBlendStateCreateInfo);

    return magma::createPipeline(pipelineCreateInfo);
  }

  magma::Pipeline<> Display::SwapchainUserData::createTexturePipeline(magma::Swapchain<claws::no_delete> swapchain, UserData const &userData)
  {
    float val;

    vk::SpecializationMapEntry mapEntry(swapchain.getExtent().width < swapchain.getExtent().height, 0, sizeof(val));
    if (!mapEntry.constantID)
      val = static_cast<float>(swapchain.getExtent().height) / static_cast<float>(swapchain.getExtent().width);
    else
      val = static_cast<float>(swapchain.getExtent().width) / static_cast<float>(swapchain.getExtent().height);
    dim = {1.0f, 1.0f};
    dim[mapEntry.constantID] /= val;

    auto specialzationInfo(magma::makeCreateInfo<vk::SpecializationInfo>(magma::asListRef(mapEntry), sizeof(val), &val));
    std::vector<vk::PipelineShaderStageCreateInfo>
      shaderStageCreateInfos{{{}, vk::ShaderStageFlagBits::eVertex, userData.vertTextureShaderModule, "main", &specialzationInfo},
			     {{}, vk::ShaderStageFlagBits::eFragment, userData.fragTextureShaderModule, "main", nullptr}};

    std::array<vk::VertexInputBindingDescription, 2u> vertexInputBindings =
      {
       vk::VertexInputBindingDescription{0, 4 * sizeof(float), vk::VertexInputRate::eVertex},
       vk::VertexInputBindingDescription{1, 6 * sizeof(float), vk::VertexInputRate::eInstance}
      };
    std::array<vk::VertexInputAttributeDescription, 4u>
      vertexInputAttrib{vk::VertexInputAttributeDescription{0, vertexInputBindings[0].binding, vk::Format::eR32G32Sfloat, 0},
			vk::VertexInputAttributeDescription{1, vertexInputBindings[0].binding, vk::Format::eR32G32Sfloat, 2 * sizeof(float)},
			vk::VertexInputAttributeDescription{2, vertexInputBindings[1].binding, vk::Format::eR32G32Sfloat, 0 * sizeof(float)},
			vk::VertexInputAttributeDescription{3, vertexInputBindings[1].binding, vk::Format::eR32G32B32A32Sfloat, 2 * sizeof(float)}
    };
    auto vertexInputStateCreateInfo(magma::makeCreateInfoNoFlags<vk::PipelineVertexInputStateCreateInfo>(vertexInputBindings,
													 vertexInputAttrib));

    vk::PrimitiveTopology topology = vk::PrimitiveTopology::eTriangleList;
    vk::PipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo{{}, topology, false};

    vk::Viewport viewport(0.0f,
			  0.0f, // pos
			  static_cast<float>(swapchain.getExtent().width),
			  static_cast<float>(swapchain.getExtent().height), // size
			  0.0f,
			  1.0); // depth range

    vk::Rect2D scissor({0, 0}, swapchain.getExtent());

    auto viewportStateCreateInfo(magma::makeCreateInfoNoFlags<vk::PipelineViewportStateCreateInfo>(magma::asListRef(viewport),
												   magma::asListRef(scissor)));

    vk::PipelineRasterizationStateCreateInfo rasterizationStateCreateInfo{
      {},
	false,                            // VkBool32                                       depthClampEnable
	  false,                            // VkBool32                                       rasterizerDiscardEnable
	  vk::PolygonMode::eFill,           // VkPolygonMode                                  polygonMode
	  vk::CullModeFlagBits::eNone,      // VkCullModeFlags                                cullMode
	  vk::FrontFace::eCounterClockwise, // VkFrontFace                                    frontFace
	  false,                            // VkBool32                                       depthBiasEnable
	  0.0f,                             // float                                          depthBiasConstantFactor
	  0.0f,                             // float                                          depthBiasClamp
	  0.0f,                             // float                                          depthBiasSlopeFactor
	  1.0f                              // float                                          lineWidth
	  };

    vk::PipelineMultisampleStateCreateInfo multisampleStateCreateInfo{
      {},
	vk::SampleCountFlagBits::e1, // VkSampleCountFlagBits                          rasterizationSamples
	  false,                       // VkBool32                                       sampleShadingEnable
	  1.0f,                        // float                                          minSampleShading
	  nullptr,                     // const VkSampleMask                            *pSampleMask
	  false,                       // VkBool32                                       alphaToCoverageEnable
	  false                        // VkBool32                                       alphaToOneEnable
	  };

    vk::PipelineColorBlendAttachmentState
      colorBlendAttachmentState{true,                  // VkBool32                                       blendEnable
	vk::BlendFactor::eSrcAlpha,  // VkBlendFactor                                  srcColorBlendFactor
	vk::BlendFactor::eOneMinusSrcAlpha, // VkBlendFactor                                  dstColorBlendFactor
	vk::BlendOp::eAdd,      // VkBlendOp                                      colorBlendOp
	vk::BlendFactor::eOne,  // VkBlendFactor                                  srcAlphaBlendFactor
	vk::BlendFactor::eZero, // VkBlendFactor                                  dstAlphaBlendFactor
	vk::BlendOp::eAdd,      // VkBlendOp                                      alphaBlendOp
	vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA};

    vk::PipelineColorBlendStateCreateInfo colorBlendStateCreateInfo{
      {},                 // VkPipelineColorBlendStateCreateFlags           flags
	false,              // VkBool32                                       logicOpEnable
	  vk::LogicOp::eCopy, // VkLogicOp                                      logicOp
	  1,
	  &colorBlendAttachmentState, // const VkPipelineColorBlendAttachmentState     *pAttachments
	    {0.0f, 0.0f, 0.0f, 0.0f}    // float                                          blendConstants[4]
    };

    magma::GraphicsPipelineConfig pipelineCreateInfo({},
						     std::move(shaderStageCreateInfos),
						     vertexInputStateCreateInfo,
						     inputAssemblyStateCreateInfo,
						     rasterizationStateCreateInfo,
						     userData.texturePipelineLayout,
						     renderPass,
						     RenderPassSubPass::world);

    pipelineCreateInfo.addRasteringColorAttachementInfo(viewportStateCreateInfo, multisampleStateCreateInfo, colorBlendStateCreateInfo);

    return magma::createPipeline(pipelineCreateInfo);
  }

  magma::Pipeline<claws::no_delete> Display::getOrCreatePipeline(renderMode::RenderMode const &renderMode)
  {
    try
      {
	return displaySystem.swapchainUserData.pipelines.at(renderMode);
      }
    catch (...)
      {
	assert(displaySystem.swapchainUserData.pipelines.size() < 2);
	return displaySystem.swapchainUserData.pipelines[renderMode] =
	  displaySystem.swapchainUserData.createPipeline(displaySystem.getSwapchain(), displaySystem.userData, renderMode);
      }

  }

  Display::Display(InitInfo &&initInfo)
    : instance(std::move(initInfo.instance))
    , surface(std::move(initInfo.surface))
    , device(std::move(initInfo.device))
    , bindDevice(std::move(*initInfo.bindDevice))
    , bindPhysicalDevice(std::move(*initInfo.bindPhysicalDevice))
    , queue(device.getQueue(initInfo.queueFamilyIndex, 0))
    , displaySystem(surface, queue, initInfo.queueFamilyIndex)
  {
  }

  Display::~Display() = default;

  void Display::renderFrame(logic::Logic const &logic, std::mutex &lock)
  {
    auto [index, frame] = displaySystem.getImage(displaySystem.userData.imageAvailable);
    auto device = magma::getDevice();

    device.waitForFences({frame.fence}, true, 1000000000);
    device.resetFences({frame.fence});
    {
      std::lock_guard<std::mutex> logicLock(lock);
      recordCommandBuffer(index, frame, logic);
    }
    vk::CommandBuffer cmdBuffer(displaySystem.swapchainUserData.commandBuffers[index].raw());
    vk::PipelineStageFlags wait_dst_stage_mask(vk::PipelineStageFlagBits::eColorAttachmentOutput);

    queue.submit(magma::makeCreateInfo<vk::SubmitInfo>(magma::asListRef(displaySystem.userData.imageAvailable),
						       &wait_dst_stage_mask,
						       magma::asListRef(cmdBuffer),
						       magma::asListRef(displaySystem.userData.renderDone)),
		 frame.fence);
    displaySystem.presentImage(displaySystem.userData.renderDone, index);
  }


  enum class RenderDataBinId : size_t
    {
     background,
     entities,
     debug,
     size
    };

  struct RenderDataBin
  {
    renderMode::RenderMode renderMode;
    uint32_t vertexCount{0u};
    uint32_t indexCount{0u};
    uint32_t indexOffset{0u};
  };

  struct RenderDataInfos
  {
    std::array<RenderDataBin, size_t(RenderDataBinId::size)> renderBins;
    uint32_t vertexSize{0u};
    uint32_t indexCount{0u};
    magma::DynamicBuffer::RangeId range;

    template<RenderDataBinId renderBinId>
    void addToBin(uint32_t vertexCount, uint32_t indexCount, uint32_t vertexSize) noexcept
    {
      this->vertexSize += vertexSize;
      this->indexCount += indexCount;

      std::get<size_t(renderBinId)>(renderBins).vertexCount += vertexCount;
      std::get<size_t(renderBinId)>(renderBins).indexCount += indexCount;
    }

    uint32_t vertexDataByteSize() const noexcept
    {
      return vertexSize * static_cast<uint32_t>(sizeof(float));
    }

    uint32_t indexDataByteSize() const noexcept
    {
      return indexCount * static_cast<uint32_t>(sizeof(unsigned int));
    }

    uint32_t dataByteSize() const noexcept
    {
      return vertexDataByteSize() + indexDataByteSize();
    }

    void computeOffsets() noexcept
    {
      uint32_t offset = 0;
      for (auto &renderBin : renderBins)
	{
	  renderBin.indexOffset = offset;
	  offset += renderBin.indexCount;
	}
    }

    template<class Func>
    void forEachBin(Func &&func)
    {
      for (auto &renderBin : renderBins)
	{
	  func(renderBin);
	}
    }
  };

  struct RenderArea
  {
    logic::Transform2d transform;
    std::vector<std::array<float, 2u>> area;
  };


  RenderDataInfos Display::uploadData(FrameUserData &frame, logic::Logic const &logic, std::vector<std::array<float, 2u>> const &area)
  {
    using namespace logic;
    RenderDataInfos renderDataInfos;
    uint32_t vertexOffset(0u);

    // if (logic.getEntityManager().empty())
    //   {
    // 	renderDataInfos.range = magma::DynamicBuffer::nullId;
    // 	return renderDataInfos;
    //   };

    logic::SingleSubViewFixture<logic::NotMovingFixture<decltype(area)>> fixture{{{area}}};

    renderMode::RenderMode &entityRenderMode(std::get<size_t(RenderDataBinId::entities)>(renderDataInfos.renderBins).renderMode);

    entityRenderMode.primitiveType = renderMode::PrimitiveType::Line;

    logic.getEntityManager().boundedVolumeHierarchy.forEachCollision(fixture.getBoundingBox(),
								     [&](auto & fixtureIt) noexcept
								     {
								       Entity *entity = fixtureIt->entity;

								       if (auto *colorComponent = entity->get<logic::ColorComponent>())
									 entity->forComponentList<logic::CollidableComponent>([&](auto const &component)
											 {
											   auto renderInfo(makeConvexPolygonRenderInfo<Pos2Color4>(makeShape(component.shape, &colorComponent->color)));
											   renderDataInfos.addToBin<RenderDataBinId::entities>(renderInfo.vertexCount(),
																	       renderInfo.indexCount(entityRenderMode.primitiveType),
																	       renderInfo.vertexSize());

											 });
								     });

    static bool enableBoundedVolumeHierarchyDebugRendering = 0;
    static bool enableDebugRenderAreas = 0;
    renderMode::RenderMode &bboxRenderMode(std::get<size_t(RenderDataBinId::debug)>(renderDataInfos.renderBins).renderMode);

    bboxRenderMode.primitiveType = renderMode::PrimitiveType::Line;


    if (enableBoundedVolumeHierarchyDebugRendering)
      {
	logic.getEntityManager().forEachBVHBoundingBox([&](auto &bbox)
						       {
							 std::array<std::array<float, 2u>, 4u> rect =
							   {
							    bbox.getMin(),
							    {bbox.getMin()[0], bbox.getMax()[1]},
							    bbox.getMax(),
							    {bbox.getMax()[0], bbox.getMin()[1]},
							   };

							 std::array<float, 4u> color{0.0f, 0.0f, 0.0f, 1.0f};
							 auto shape(makeShape(rect, &color));
							 auto renderInfo(makeConvexPolygonRenderInfo<Pos2Color4>(shape));

							 renderDataInfos.addToBin<RenderDataBinId::debug>(renderInfo.vertexCount(),
													  renderInfo.indexCount(bboxRenderMode.primitiveType),
													  renderInfo.vertexSize());
						       });

      }
    if (enableDebugRenderAreas)
      {
	std::array<float, 4u> color{1.0f, 0.0f, 0.0f, 1.0f};
	auto shape(makeShape(area, &color));
	auto renderInfo(makeConvexPolygonRenderInfo<Pos2Color4>(shape));

	renderDataInfos.addToBin<RenderDataBinId::debug>(renderInfo.vertexCount(),
							 renderInfo.indexCount(bboxRenderMode.primitiveType),
							 renderInfo.vertexSize());
      }

    renderDataInfos.computeOffsets();
    renderDataInfos.range = displaySystem.userData.dynamicBuffer.allocate(renderDataInfos.dataByteSize());
    frame.ranges.push_back(renderDataInfos.range);

    auto const data(displaySystem.userData.dynamicBuffer.getMemory<char[]>(renderDataInfos.range));
    auto vertexIt(reinterpret_cast<float *>(data.get()));
    auto indexIt(reinterpret_cast<unsigned int *>(data.get() + renderDataInfos.vertexDataByteSize()));

    logic.getEntityManager().boundedVolumeHierarchy.forEachCollision(fixture.getBoundingBox(),
								     [&](auto & fixtureIt) noexcept
								     {
								       Entity *entity = fixtureIt->entity;

								       if (auto *colorComponent = entity->get<logic::ColorComponent>())
									 entity->forComponentList<logic::CollidableComponent>([&](auto const &component)
															      {
																auto renderInfo(makeConvexPolygonRenderInfo<Pos2Color4>(makeShape(component.shape, &colorComponent->color)));
																renderInfo.writeData(vertexIt, indexIt, vertexOffset, entityRenderMode.primitiveType);
																vertexIt += renderInfo.vertexSize();
																indexIt += renderInfo.indexSize(entityRenderMode.primitiveType);
																vertexOffset += renderInfo.vertexCount();

															      });

								     });
    if (enableBoundedVolumeHierarchyDebugRendering)
      {
	logic.getEntityManager().forEachBVHBoundingBox([&](auto &bbox)
						       {
							 std::array<std::array<float, 2u>, 4u> rect =
							   {
							    bbox.getMin(),
							    {bbox.getMin()[0], bbox.getMax()[1]},
							    bbox.getMax(),
							    {bbox.getMax()[0], bbox.getMin()[1]},
							   };

							 std::array<float, 4u> color{0.0f, 1.0f, 1.0f, 1.0f};
							 auto shape(makeShape(rect, &color));
							 auto renderInfo(makeConvexPolygonRenderInfo<Pos2Color4>(shape));

							 renderInfo.writeData(vertexIt, indexIt, vertexOffset, bboxRenderMode.primitiveType);
							 vertexIt += renderInfo.vertexSize();
							 indexIt += renderInfo.indexSize(bboxRenderMode.primitiveType);
							 vertexOffset += renderInfo.vertexCount();
						       });
      }
    if (enableDebugRenderAreas)
      {
	std::array<float, 4u> color{1.0f, 0.0f, 0.0f, 1.0f};
	auto shape(makeShape(area, &color));
	auto renderInfo(makeConvexPolygonRenderInfo<Pos2Color4>(shape));

	renderInfo.writeData(vertexIt, indexIt, vertexOffset, bboxRenderMode.primitiveType);
	vertexIt += renderInfo.vertexSize();
	indexIt += renderInfo.indexSize(bboxRenderMode.primitiveType);
	vertexOffset += renderInfo.vertexCount();
      }

    return renderDataInfos;
  }

  void Display::renderInArea(uint32_t index, FrameUserData &frame, logic::Logic const &logic, magma::RenderPassExecLock &lock, RenderArea const &area)
  {
    magma::PrimaryCommandBuffer cmdBuffer(displaySystem.swapchainUserData.commandBuffers[index]);
    RenderDataInfos renderDataInfos(uploadData(frame, logic, area.area));

    if (renderDataInfos.dataByteSize())
      {
	cmdBuffer.bindVertexBuffers(0, {displaySystem.userData.dynamicBuffer.getBuffer(renderDataInfos.range)}, {renderDataInfos.range.second});
	cmdBuffer.bindIndexBuffer(displaySystem.userData.dynamicBuffer.getBuffer(renderDataInfos.range), renderDataInfos.range.second + renderDataInfos.vertexDataByteSize(), vk::IndexType::eUint32);
      }
    {
      using namespace logic; // operator/

      auto transform(-area.transform);

      cmdBuffer.pushConstants(displaySystem.userData.pipelineLayout, vk::ShaderStageFlagBits::eVertex, 0, transform.position / logic.getScale());
      cmdBuffer.pushConstants(displaySystem.userData.pipelineLayout, vk::ShaderStageFlagBits::eVertex, 2, transform.rotation / logic.getScale());
    }

    renderDataInfos.forEachBin([&](auto const &renderBin)
			       {
				 if (renderBin.indexCount)
				   {
				     lock.bindGraphicsPipeline(getOrCreatePipeline(renderBin.renderMode));
				     lock.drawIndexed(renderBin.indexCount, 1, renderBin.indexOffset, 0, 0);
				   }
			       });
  }

  void Display::recordTillingImageDisplay(FrameUserData &frame, magma::PrimaryCommandBuffer cmdBuffer, magma::RenderPassExecLock &lock, SpriteType sprite, uint32_t animFrame, logic::Transform2d const &transform, std::array<float, 2> size, std::array<float, 4u> const &mask)
  {
    {
      auto range = displaySystem.userData.dynamicBuffer.allocate(sizeof(float) * 4 * 6); // float * (tex + pos) * corner count
      frame.ranges.push_back(range);

      using namespace logic; // array_ops
      auto memory = displaySystem.userData.dynamicBuffer.getMemory<float[]>(range);
      auto it = &memory[0];
      constexpr std::array<std::array<float, 2u>, 6> const corners{{{0.0f, 0.0f},
								    {1.0f, 0.0f},
								    {0.0f, 1.0f},
								    {0.0f, 1.0f},
								    {1.0f, 0.0f},
								    {1.0f, 1.0f}}};

      for (auto const &corner : corners)
	{
	  auto position(corner);

	  for (size_t i = 0; i != 2; ++i)
	    position[i] -= 0.5f;
	  it = std::copy(position.begin(), position.end(), it);

	  for (size_t i = 0; i != 2; ++i)
	    position[i] *= size[i];
	  position = transform.apply(position);
	  it = std::copy(position.begin(), position.end(), it);
	}
      cmdBuffer.bindVertexBuffers(0, {displaySystem.userData.dynamicBuffer.getBuffer(range)}, {range.second});
    }
    recordSingleImageDisplay(frame, cmdBuffer, lock, sprite, animFrame, transform, size, mask);
  }

  void Display::recordSingleImageDisplay(FrameUserData &frame, magma::PrimaryCommandBuffer cmdBuffer, magma::RenderPassExecLock &lock, SpriteType sprite, uint32_t animFrame, logic::Transform2d const &transform, std::array<float, 2> size, std::array<float, 4u> const &mask)
  {
    auto rangeInstance = displaySystem.userData.dynamicBuffer.allocate(sizeof(float) * 6); // float * (scale + transform)
    frame.ranges.push_back(rangeInstance);
    {
      auto memory = displaySystem.userData.dynamicBuffer.getMemory<float[]>(rangeInstance);
      auto it = &memory[0];

      it = std::copy(size.begin(), size.end(), it);
      it = std::copy(transform.position.begin(), transform.position.end(), it);
      it = std::copy(transform.rotation.begin(), transform.rotation.end(), it);
    }
    {
      cmdBuffer.bindVertexBuffers(1,
				  {displaySystem.userData.dynamicBuffer.getBuffer(rangeInstance)},
				  {rangeInstance.second});
    }

    
    auto set = displaySystem.userData.pictureDescriptorSets[displaySystem.userData.animImageList[size_t(sprite)].baseDescriptorOffset + animFrame % displaySystem.userData.animImageList[size_t(sprite)].images.size()];

    cmdBuffer.raw().bindDescriptorSets(vk::PipelineBindPoint::eGraphics, displaySystem.userData.texturePipelineLayout,
				       0, 1, &set, 0, nullptr);
    cmdBuffer.pushConstants(displaySystem.userData.texturePipelineLayout, vk::ShaderStageFlagBits::eFragment, 4, mask);
    lock.draw(6, 1, 0, 0);
  }

  struct ImageTransform
  {
    std::array<float, 2> size;
    logic::Transform2d transform;
  };

  void Display::recordMultiImageDisplay(FrameUserData &frame, magma::PrimaryCommandBuffer cmdBuffer, magma::RenderPassExecLock &lock, SpriteType sprite, uint32_t animFrame, std::vector<ImageTransform> const &imageTransforms, std::array<float, 4u> const &mask)
  {
    auto rangeInstance = displaySystem.userData.dynamicBuffer.allocate(sizeof(float) * 6 * imageTransforms.size()); // float * (scale + transform)
    frame.ranges.push_back(rangeInstance);

    {
      auto memory = displaySystem.userData.dynamicBuffer.getMemory<float[]>(rangeInstance);
      auto it = &memory[0];

      for (auto const &imageTransform : imageTransforms)
	{
	  it = std::copy(imageTransform.size.begin(), imageTransform.size.end(), it);
	  it = std::copy(imageTransform.transform.position.begin(), imageTransform.transform.position.end(), it);
	  it = std::copy(imageTransform.transform.rotation.begin(), imageTransform.transform.rotation.end(), it);
	}
    }
    {
      cmdBuffer.bindVertexBuffers(1,
				  {displaySystem.userData.dynamicBuffer.getBuffer(rangeInstance)},
				  {rangeInstance.second});
    }

    
    auto set = displaySystem.userData.pictureDescriptorSets[displaySystem.userData.animImageList[size_t(sprite)].baseDescriptorOffset + animFrame % displaySystem.userData.animImageList[size_t(sprite)].images.size()];

    cmdBuffer.raw().bindDescriptorSets(vk::PipelineBindPoint::eGraphics, displaySystem.userData.texturePipelineLayout,
				       0, 1, &set, 0, nullptr);
    cmdBuffer.pushConstants(displaySystem.userData.texturePipelineLayout, vk::ShaderStageFlagBits::eFragment, 4, mask);
    lock.draw(6, imageTransforms.size(), 0, 0);
  }


  void Display::recordImageDisplay(FrameUserData &frame, logic::Logic const &logic, magma::PrimaryCommandBuffer cmdBuffer, magma::RenderPassExecLock &lock, RenderArea const &area)
  {
    logic::SingleSubViewFixture<logic::NotMovingFixture<decltype(area.area)>> fixture{{{area.area}}};

    lock.bindGraphicsPipeline(displaySystem.swapchainUserData.texturePipeline);
    {
      using namespace logic; // operator/

      auto transform(-area.transform);
      cmdBuffer.pushConstants(displaySystem.userData.texturePipelineLayout, vk::ShaderStageFlagBits::eVertex, 0, transform.position / logic.getScale());
      cmdBuffer.pushConstants(displaySystem.userData.texturePipelineLayout, vk::ShaderStageFlagBits::eVertex, 2, transform.rotation / logic.getScale());
    }
    // TODO: reintroduce collision check
    // logic.getEntityManager()
    //   .boundedVolumeHierarchy
    //   .forEachCollision(fixture.getBoundingBox(),
    // 			[&](auto & fixtureIt) noexcept
    // 			{
    // 			  logic::Entity *entity = fixtureIt->entity;
    // 			  {
    // 			    if (auto *pictureComponent = entity->get<logic::PictureComponent>())
    {
      using claws::scalar_array_ops::operator*;
      recordTillingImageDisplay(frame, cmdBuffer, lock,
    			       SpriteType::Sand,
    			       0,
    			       area.transform, displaySystem.swapchainUserData.dim * 2.0f);
    }

    std::array<std::vector<logic::PictureComponent const *>, 5> componentLayers;
    logic.getEntityManager().
      forEachComponent<logic::PictureComponent>
      ([&](auto const &pictureComponent)
       {
	 if (pictureComponent.sprite == SpriteType::FishyFish)
	   componentLayers[0].emplace_back(&pictureComponent);
	 else if (pictureComponent.sprite != SpriteType::SpeedySpider && pictureComponent.sprite != SpriteType::FishyFish && pictureComponent.sprite != SpriteType::Wave && pictureComponent.sprite != SpriteType::Bullet)
	   componentLayers[1].emplace_back(&pictureComponent);
	 else if (pictureComponent.sprite == SpriteType::Wave)
	   componentLayers[2].emplace_back(&pictureComponent);
	 else if (pictureComponent.sprite == SpriteType::Bullet)
	   componentLayers[3].emplace_back(&pictureComponent);
	 else if (pictureComponent.sprite == SpriteType::SpeedySpider)
	   componentLayers[4].emplace_back(&pictureComponent);
       });
    {
      auto rangeVert = displaySystem.userData.dynamicBuffer.allocate(sizeof(float) * 4 * 6); // float * (tex + pos) * corner count
      frame.ranges.push_back(rangeVert);
      {
	using namespace logic; // array_ops
	auto memory = displaySystem.userData.dynamicBuffer.getMemory<float[]>(rangeVert);
	auto it = &memory[0];
	constexpr std::array<std::array<float, 2u>, 6> const corners{{{0.0f, 0.0f},
								      {1.0f, 0.0f},
								      {0.0f, 1.0f},
								      {0.0f, 1.0f},
								      {1.0f, 0.0f},
								      {1.0f, 1.0f}}};

	for (auto const &corner : corners)
	  {
	    auto position(corner);

	    for (size_t i = 0; i != 2; ++i)
	      position[i] -= 0.5f;
	    it = std::copy(position.begin(), position.end(), it);
	    it = std::copy(corner.begin(), corner.end(), it);
	  }
      }
      cmdBuffer.bindVertexBuffers(0,
				  {displaySystem.userData.dynamicBuffer.getBuffer(rangeVert)},
				  {rangeVert.second});
    }
    for (size_t index = 0; index != componentLayers.size(); ++index)
      {
	if (index == 3 && componentLayers[index].size())
	  {
	    std::vector<ImageTransform> imageTransforms;

	    for (auto const *pictureComponent : componentLayers[index])
	      if (auto *fixtureComponent = pictureComponent->entity->get<logic::FixtureComponent>())
		imageTransforms.push_back(ImageTransform{{pictureComponent->displayedWidth, pictureComponent->displayedHeight}, pictureComponent->offset + fixtureComponent->fixture.transform});
	    auto const *pictureComponent = componentLayers[index].front();
	    recordMultiImageDisplay(frame, cmdBuffer, lock, pictureComponent->sprite, pictureComponent->currentFrame / 8, imageTransforms, pictureComponent->mask);
	  }
	else
	  for (auto const *pictureComponent : componentLayers[index])
	    if (auto *fixtureComponent = pictureComponent->entity->get<logic::FixtureComponent>())
	      recordSingleImageDisplay(frame, cmdBuffer, lock, pictureComponent->sprite, pictureComponent->currentFrame / 8, pictureComponent->offset + fixtureComponent->fixture.transform, {pictureComponent->displayedWidth, pictureComponent->displayedHeight}, pictureComponent->mask);
      }
    {
      using logic::operator*; // array_ops
      SpriteType sprite = logic.isIntro() ? SpriteType::Intro : (logic.isWin() ? SpriteType::Win : (logic.isGameOver() ? SpriteType::GameOver : SpriteType::Border));
      uint32_t animFrame = std::min<uint32_t>(uint32_t(displaySystem.userData.animImageList[size_t(sprite)].images.size() - 1),
					      logic.getAnimFrame() / (logic.isIntro() ? 40 : (logic.isWin() ? 360 : 120)));

      recordSingleImageDisplay(frame, cmdBuffer, lock,
      			       sprite,
      			       animFrame,
      			       area.transform, displaySystem.swapchainUserData.dim * 2.0f);
      logic.getEntityManager()
      	.forEachComponent<logic::FishyComponent>([&](logic::FishyComponent const &fishyTheFish) noexcept
      						 {
      						   using claws::array_ops::operator*=;
      						   using claws::scalar_array_ops::operator*;
      						   std::array<float, 2u> size{float(fishyTheFish.HPs) / float(fishyTheFish.maxHPs), 30.0f / 1080.0f};

      						   size *= displaySystem.swapchainUserData.dim * 0.9f;

      						   recordSingleImageDisplay(frame, cmdBuffer, lock,
      									    SpriteType::HpBar, 0,
      									    logic::Transform2d{{-displaySystem.swapchainUserData.dim[0] * 0.9f + size[0],
      												    displaySystem.swapchainUserData.dim[1] * 0.9f - size[1]}, {1.0f, 0.0f}}
      									    + area.transform,
      									    size * 2.0f);
      						 });
    }
  }

  void Display::recordCommandBuffer(uint32_t index, FrameUserData &frame, logic::Logic const &logic)
  {
    using namespace logic; // array_ops
    for (auto &range : frame.ranges)
      displaySystem.userData.dynamicBuffer.free(range);
    frame.ranges.clear();

    magma::PrimaryCommandBuffer cmdBuffer(displaySystem.swapchainUserData.commandBuffers[index]);

    cmdBuffer.begin(vk::CommandBufferUsageFlagBits::eOneTimeSubmit); 
    //vk::ClearValue backgroundClearValue = {vk::ClearColorValue(std::array<float, 4>{192.0f / 255.0f, 225.0f / 255.0f, 235.0f / 255.0f, 0.0f})};
    vk::ClearValue backgroundClearValue = {vk::ClearColorValue(std::array<float, 4>{142.0f / 255.0f, 175.0f / 255.0f, 185.0f / 255.0f, 0.0f})};
    vk::ClearValue backBufferClearValue = {vk::ClearColorValue(std::array<float, 4>{0.0f, 0.0f, 0.0f, 0.0f})};
    {
      magma::RenderPassExecLock lock(cmdBuffer.beginRenderPass(displaySystem.swapchainUserData.renderPass, frame.framebuffer.getFrameBuffer(), {{0, 0}, displaySystem.getSwapchain().getExtent()}, {backgroundClearValue, backBufferClearValue}, vk::SubpassContents::eInline));

      {
	std::vector<std::array<float, 2u>> cameraBounds{{-displaySystem.swapchainUserData.dim,
							 std::array<float, 2u>{displaySystem.swapchainUserData.dim[0], -displaySystem.swapchainUserData.dim[1]},
							 displaySystem.swapchainUserData.dim,
							 std::array<float, 2u>{-displaySystem.swapchainUserData.dim[0], displaySystem.swapchainUserData.dim[1]}
	  }};
	std::array<float, 2> center = {0.0f, 0.0f};
	{
	  auto transform(-logic.getCamera());

	  transform.rotation *= logic.getScale() * 0.95f;
	  for (auto &bound : cameraBounds)
	    bound = transform.apply(bound);
 	  center = transform.apply(center);
	}

	RenderArea const area{-logic.getCamera(), {{cameraBounds}}};

	recordImageDisplay(frame, logic, cmdBuffer, lock, area);
	renderInArea(index, frame, logic, lock, area);
      }

      lock.next(vk::SubpassContents::eInline);
      cmdBuffer.raw().bindDescriptorSets(vk::PipelineBindPoint::eGraphics, displaySystem.userData.fullscreenPipelineLayout, 0, 1, displaySystem.swapchainUserData.descriptorSets.data(), 0, nullptr);
      lock.bindGraphicsPipeline(displaySystem.swapchainUserData.fullscreenPipeline);
      lock.draw(3, 1, 0, 0);
    }
    cmdBuffer.end();
  }
}

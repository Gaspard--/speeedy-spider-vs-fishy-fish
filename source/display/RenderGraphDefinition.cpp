namespace display
{
  #if 0
  void buildRenderGraph(RenderGraph &renderGraph)
  {
    auto depthStencil = renderGraph().createDepthStencil();

    // stencil rendering
    auto stencilRender = renderGraph.createNode();
    stencilRender.addStencilTarget(depthStencil);
    // world rendering
    // post process
  }
  #endif
}

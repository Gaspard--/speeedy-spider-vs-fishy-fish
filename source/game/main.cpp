#define GLFW_INCLUDE_VULKAN

#include <iostream>
#include <thread>

#include <claws/utils/on_scope_exit.hpp>

#include "my_glfw.hpp"
#include "display/Display.hpp"
#include "logic/Logic.hpp"
#include "logic/LogicThread.hpp"
#include "logic/Input.hpp"
#include "logic/SoundHandler.hpp"
#include <cstdlib>
#include <ctime>

#include "pthread.h"

int main()
{
  srand(time(NULL));
  logic::SoundHandler::initSoundHandler("resource/speedy spider (part 1).wav",
					"resource/speedy spider (part 2).wav");

  claws::on_scope_exit quitMusic([]() { logic::SoundHandler::destroySoundHandler(); });

  logic::SoundHandler::getInstance().launchMainMusic();
  logic::SoundHandler::getInstance().launchSecondaryMusic();
  logic::SoundHandler::getInstance().setMainVolume(30.0f);
  logic::SoundHandler::getInstance().setSecondaryVolume(30.0f);
  //  try
    {
      glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
      GlfwContext glfwContext{};

      input::Input input(glfwContext.createWindow({1920u, 1080u}, std::string{"Wasted Prophecies"}));
      display::Display display([]() {
	  unsigned int glfwExtensionCount(0);
	  const char **glfwExtensions(glfwGetRequiredInstanceExtensions(&glfwExtensionCount));

	  return std::vector<char const *>(glfwExtensions, glfwExtensions + glfwExtensionCount);
	}(), [&input](magma::Instance const &instance){
	  VkSurfaceKHR rawSurface[1];
	  glfwCreateWindowSurface(instance.vkInstance, &input.getWindow(), nullptr, rawSurface);

	  return *rawSurface;
	});
      logic::Logic logic{};
      std::mutex lock;
      auto const logicTickFunc([&logic, &input]() {
	  glfwPollEvents();
	  logic.tick(input);
	});
      LogicThread<decltype(logicTickFunc)> logicTick(logicTickFunc, lock);

      std::thread thread([&logicTick]
			 {
			   logicTick();
			   pthread_setname_np(pthread_self(), "LOGIC");
			 });
      {
	claws::on_scope_exit quitOnDeath([&logicTick]() { logicTick.quit(); });
	// try
	  {
	    while (!input.windowShouldClose())
	      display.renderFrame(logic, lock);
	  }
	// catch (std::runtime_error const &e)
	//   {
	//     std::cerr << "render thread encountered a runtime error:\n" << e.what() << std::endl;
	//   }
      }
      thread.join();
    }
  // catch (std::runtime_error const &e)
  //   {
  //     std::cerr << "program encoutered runtime error:\n" << e.what() << std::endl;
  //     return -1;
  //   }
}

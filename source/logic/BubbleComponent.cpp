#include "logic/BubbleComponent.hpp"
#include "logic/ColorComponent.hpp"
#include "logic/TaggedIndex.hpp"
#include "logic/PictureComponent.hpp"
#include "logic/EntityManager.hpp"

#include <cmath>

namespace logic
{
  void BubbleComponent::update(TaggedIndex<BubbleComponent>)
  {
    time += 1.0f / 240.f;
    if (time >= 1.0)
      entity->markForRemoval();

    if (ColorComponent *colorComponent = entity->get<ColorComponent>())
      colorComponent->color[3] = std::sin(time * 3.14f);
    if (PictureComponent *pictureComponent = entity->get<PictureComponent>())
      pictureComponent->mask[3] = std::sin(time * 3.14f);
  }

}

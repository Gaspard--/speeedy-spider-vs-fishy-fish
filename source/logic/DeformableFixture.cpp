#include "logic/DeformableFixture.hpp"

namespace logic
{
  class EndTag
  {};

  template<class Parent>
  class SubviewProvider : protected DeformableFixture::FixtureView<Parent>
  {
  protected:
    void calculateNext() noexcept
    {
      auto &firstCornerIndex(DeformableFixture::FixtureView<Parent>::firstCornerIndex);
      auto &lastCornerIndex(DeformableFixture::FixtureView<Parent>::lastCornerIndex);
      auto const *parent(DeformableFixture::FixtureView<Parent>::parent);

      firstCornerIndex = lastCornerIndex;
      ++lastCornerIndex;
      std::array<float, 2u> orginalDirection(parent->getCorner(lastCornerIndex).position - parent->getCorner(firstCornerIndex).position);
      std::array<float, 2u> lastDirection(orginalDirection);

      claws::inject_self([&](auto retry) noexcept
			 {
			   ++lastCornerIndex;
			   if (lastCornerIndex != parent->corners.size())
			     {
			       std::array<float, 2u> currentDirection(parent->getCorner(lastCornerIndex).position - parent->getCorner(firstCornerIndex).position);

			       if (cross(currentDirection, orginalDirection) > 0 &&
				   cross(currentDirection, lastDirection) > 0)
				 {
				   lastDirection = parent->getCorner(lastCornerIndex).position - parent->getCorner(firstCornerIndex).position;
				   retry();
				 }
			     }
			 });
    }
  public:
    SubviewProvider(Parent *parent) noexcept
      : DeformableFixture::FixtureView<Parent>{{0u, 0u}, parent}
    {
    }

    auto &operator++() noexcept
    {
      calculateNext();
      return *this;
    }

    DeformableFixture::FixtureView<Parent> &operator*() noexcept
    {
      return *this;
    }

    bool operator!=(EndTag) const noexcept
    {
      auto &lastCornerIndex(DeformableFixture::FixtureView<Parent>::lastCornerIndex);
      auto const *parent(DeformableFixture::FixtureView<Parent>::parent);

      return lastCornerIndex != parent->corners.size();
    }
  };

  void DeformableFixture::update() noexcept
  {
    for (auto &joint : joints)
      joint.apply(nodes[joint.source], nodes[joint.dest]);
    for (auto &node : nodes)
      node.update();
    views.clear();
    for (auto provider{SubviewProvider{this}}; provider != EndTag{}; ++provider)
      views.push_back(*provider);
    updateBoundingBox();
  }

  void DeformableFixture::updateBoundingBox() noexcept
  {
    boundingBox.update(claws::container_view(corners, [this](unsigned int index) noexcept
					     {
					       return nodes[index].position;
					     }));
  }

  void DeformableFixture::applyImpulseAt([[maybe_unused]] std::array<float, 2u> const &point, std::array<float, 2u> const &impulse, unsigned int index, bool which) noexcept
  {
    if (which)
      {
	nodes[corners[index]].speed += impulse;
      }
    else
      {
	if (index + 1 == corners.size())
	  index = 0;
	nodes[corners[index]].speed += impulse * 0.5f;
	nodes[corners[index + 1]].speed += impulse * 0.5f;
      }
  }

}

#include "logic/EntityManager.hpp"

namespace logic
{
  void Entity::markForRemoval() noexcept
  {
    shouldBeRemoved = true;
  }

  Entity::~Entity() noexcept
  {
    while (!components.empty())
      {
	auto &[key, value] = *components.begin();
	g_entityManager->notifyComponentEntityRemoval(key, value, this);
      }
    for (auto &[key, list] : componentLists)
      while (!list.empty())
	{
	  auto &value = *list.begin();
	  g_entityManager->notifyComponentEntityRemoval(key, value, this);
	}
  }
}


#include <functional>
#include <logic/Collision.hpp>

#include "logic/EntityManager.hpp"
#include "logic/ExplosiveSnakeAI.hpp"
#include "logic/SpawnInfo.hpp"
#include "logic/SpiderBoss.hpp"
#include "logic/Components.hpp"
#include "logic/GunComponent.hpp"
#include "logic/ColorComponent.hpp"
#include "logic/PlayerComponent.hpp"
#include "logic/PictureComponent.hpp"
#include "logic/FlagComponent.hpp"
#include "logic/FishyComponent.hpp"
#include "logic/WaveComponent.hpp"
#include "logic/BubbleComponent.hpp"

#include "claws/utils/tuple_helper.hpp"

namespace logic
{
  namespace
  {
    template<size_t nbrCorners>
    void generateCircle(std::array<std::array<float, 2u>, nbrCorners> &corners, float ray) noexcept
    {
      for (unsigned i = 0 ; i < nbrCorners ; ++i)
	{
	  float angle = 2.f * M_PIf32 * float(i) / float(nbrCorners);
	  corners[i] = {ray * cosf(angle), ray * sinf(angle)};
	}
    }
  }

  EntityManager *g_entityManager;

  EntityManager::EntityManager()
  {
    assert(!g_entityManager);
    g_entityManager = this;
  }

  void EntityManager::init()
  {
    clear();
    std::array<std::array<float, 2u>, 8> corners;

    generateCircle(corners, 0.1f);

    Transform2d centerPosition = {std::array<float, 2u>{0.0f, 0.5f},
                                  std::array<float, 2u>{1.0f, 0.0f}};
    Transform2d positionObject = {std::array<float, 2u>{0.0f, 2.0f},
                                  std::array<float, 2u>{1.0f, 0.0f}};

    Fixture triangle(std::vector<std::array<float, 2u>>{corners.begin(), corners.end()}, centerPosition, positionObject);
    triangle.inverseAngularMass = 0.00f;
    triangle.rotation = -0.006f;

    std::array<float, 4u> color{{0.2f, 0.2f, 0.0f, 1.0f}};
    createHurtBox(triangle, color, 1000u);
    createComponent<PlayerComponent>(entities.front().get());
    createComponent<FlagComponent>(entities.front().get(), flag::Type::LilyPadAffected, true);
    centerPosition.position *= 1.05f;
    createComponent<PictureComponent>(entities.front().get(), 0.1f * 8.0f, 0.1f * 8.0f, display::SpriteType::SpeedySpider, centerPosition);
    createComponent<GunComponent>(entities.front().get(), 20, 10);

    // Fishy the Fish, Genesys

    std::array<std::array<float, 2u>, 8> fishyCorners;

    generateCircle(fishyCorners, 0.15f);

    Fixture fishyShape(std::vector<std::array<float, 2u>>{fishyCorners.begin(), fishyCorners.end()}, 1.0f, 0.f);
    fishyShape.transform.position = {-1.5f, .5f};

    Entity &fishyEntity = createHurtBox(fishyShape, color, 1000u);
    createComponent<FishyComponent>(&fishyEntity);
    createComponent<PictureComponent>(&fishyEntity, 0.3f * 10.5f, -0.3f * 10.5f, display::SpriteType::FishyFish, Transform2d{{-0.9f, 0.0f}, {0.0f, 1.0f}});
    createComponent<FlagComponent>(&fishyEntity, flag::Type::UnderWater, true);
  }

  EntityManager::~EntityManager() noexcept = default;

#if 0
  std::unique_ptr<Ai> EntityManager::spawnEntity(SpawnInfo const &spawnInfo)
  {
    return std::visit([&](SpiderBossSpawnInfo const &) {
	return std::make_unique<SpiderBoss>(getAccessor(),
					    getCreator(),
					    spawnInfo.position,
					    spawnInfo.scale);
      }, spawnInfo.additionalInfo);
  }
#endif

  void EntityManager::update()
  {
    claws::tuple_helper::for_each_tuple_element(components, [](auto &container) noexcept
							    {
							      for (auto it = container.begin(); it != container.end();)
								{
								  if constexpr (!std::is_same_v<decltype(it->update(it)), void>)
								    {
								      if (it->update(it))
									container.removeComponent(it);
								      else
									++it;
								    }
								  else
								    {
								      it->update(it);
								      ++it;
								    }
								}
							    });
  }

  void EntityManager::removeOld()
  {
    entities.erase(std::remove_if(entities.begin(), entities.end(), [](auto const &entity) noexcept
								    {
								      return entity->isMarkedForRemoval();
								    }), entities.end());
  }

  static void dealDamage(Entity &dealer, Entity &receiver)
  {
    if (dealer.get<DmgComponent>()->dmg >= receiver.get<HPComponent>()->hp) {
        receiver.get<HPComponent>()->hp = 0;
        receiver.get<ColorComponent>()->color = {0.0f, 0.0f, 0.0f, 0.0f};
      }
    receiver.get<HPComponent>()->hp -= dealer.get<DmgComponent>()->dmg;
  }

  static void checkCollision(TaggedIndex<CollidableComponent> a, TaggedIndex<CollidableComponent> b)
  {
    static constexpr float lilypadSpeedAlterationFactor = 0.8f;
    auto entityA = a->entity;
    auto entityB = b->entity;

    auto *flagComponentA = entityA->get<FlagComponent>();
    auto *flagComponentB = entityB->get<FlagComponent>();

    if (flagComponentA && flagComponentB)
      {
	if (checkCollision(asShapeFixture(a->shape), asShapeFixture(b->shape))) {
	  {
	    if (FishyComponent *fishComponentA = entityA->get<FishyComponent>())
	      {
		if (entityB->get<PlayerComponent>())
		  entityB->markForRemoval();
		if (fishComponentA->suck && flagComponentB->type == flag::Type::LilyPad)
		  {
		    entityB->markForRemoval();
		    fishComponentA->suck -= 1;
		    fishComponentA->HPs += 1;
		  }
	      }
	    if (FishyComponent *fishComponentB = entityB->get<FishyComponent>())
	      {
		if (entityA->get<PlayerComponent>())
		  entityA->markForRemoval();
		if (fishComponentB->suck && flagComponentA->type == flag::Type::LilyPad)
		  {
		    entityA->markForRemoval();
		    fishComponentB->suck -= 1;
		    fishComponentB->HPs += 1;
		  }
	      }

	    auto *fixtureComponentA = entityA->get<FixtureComponent>();
	    auto *fixtureComponentB = entityB->get<FixtureComponent>();
	    if (fixtureComponentA && fixtureComponentB)
	      {
		auto result(minDisplacement(asShapeFixture(a->shape), asShapeFixture(b->shape)));
		if (flagComponentA->type == flag::Type::LilyPadAffected &&
		    flagComponentB->type == flag::Type::LilyPad)
		  {
		    fixtureComponentA->fixture.transform.position += fixtureComponentB->fixture.getSpeedAt(result.point) * lilypadSpeedAlterationFactor;
		    fixtureComponentA->fixture.wakeUp();
		  }
		else if (flagComponentB->type == flag::Type::LilyPadAffected &&
			 flagComponentA->type == flag::Type::LilyPad)
		  {
		    fixtureComponentB->fixture.transform.position += fixtureComponentA->fixture.getSpeedAt(result.point) * lilypadSpeedAlterationFactor;
		    fixtureComponentB->fixture.wakeUp();
		  }	
		else if (flagComponentB->type == flag::Type::LilyPad &&
			 flagComponentA->type == flag::Type::LilyPad)
		  {
		    collisionResponse(fixtureComponentA->fixture, fixtureComponentB->fixture, result);
		  }
	      }
	    if (flagComponentA->type == flag::Type::Bullet)
	      if (FishyComponent *fishyComponentB = entityB->get<FishyComponent>())
		{
		  fishyComponentB->HPs -= !!fishyComponentB->HPs;	
		  entityB->get<PictureComponent>()->mask[0] = 2.0f;
		  entityB->get<PictureComponent>()->mask[1] = 0.0f;
		  entityB->get<PictureComponent>()->mask[2] = 0.0f;
		  entityA->markForRemoval();
		}
	    if (flagComponentB->type == flag::Type::Bullet)
	      if (FishyComponent *fishyComponentA = entityA->get<FishyComponent>())
		{
		  fishyComponentA->HPs -= !!fishyComponentA->HPs;
		  entityA->get<PictureComponent>()->mask[0] = 2.0f;
		  entityA->get<PictureComponent>()->mask[1] = 0.0f;
		  entityA->get<PictureComponent>()->mask[2] = 0.0f;
		  entityB->markForRemoval();
		}
	  }
	{
	  auto *waveComponentA = entityA->get<WaveComponent>();
	  auto *waveComponentB = entityB->get<WaveComponent>();

	  if (flagComponentA && flagComponentA->type == flag::Type::LilyPad && waveComponentB)
	    if (auto *fixtureComponentA = entityA->get<FixtureComponent>())
	      waveComponentB->apply(fixtureComponentA);
	  if (flagComponentB && flagComponentB->type == flag::Type::LilyPad && waveComponentA)
	    if (auto *fixtureComponentB = entityB->get<FixtureComponent>())
	      waveComponentA->apply(fixtureComponentB);
	}
      }
    }
  }

  void EntityManager::checkCollisions()
  {
    void (*func)(TaggedIndex<CollidableComponent> a, TaggedIndex<CollidableComponent> b) = logic::checkCollision;
    boundedVolumeHierarchy.forEachInnerCollision(func);
  }

  void EntityManager::notifyComponentEntityRemoval(std::type_index componentType, uint16_t index, Entity *entity)
  {
    claws::tuple_helper::for_each_tuple_element(components, [&](auto &container)
							    {
							      using Component = typename std::decay_t<decltype(container)>::Component;
							      if (std::type_index(typeid(Component)) == componentType)
								{
								  container.removeComponent(TaggedIndex<Component>{index});
								}
							    });
  }
  // Basic entity which deal damage. A bullet, for example
  Entity &EntityManager::createHurtingBox(Fixture const &fixture, std::array<float, 4u> const &color, unsigned int dmg)
  {
    auto &result(createEntity(fixture, color));

    result.addInlineComponent(std::make_unique<DmgComponent>(dmg));
    return result;
  }

  Entity &EntityManager::createHurtBox(Fixture const &fixture, std::array<float, 4u> const &color, unsigned int hp)
  {
    auto &result(createEntity(fixture, color));

    result.addInlineComponent(std::make_unique<HPComponent>(hp));
    // TODO: add ai component
    return result;
  }

  Entity &EntityManager::createEntity(Fixture const &fixture, std::array<float, 4u> const &color)
  {
    auto &result(createEntity());

    createComponent<FixtureComponent>(&result, fixture);
    createComponent<ColorComponent>(&result, color);
    return result;
  }

  void EntityManager::spawnLilypad(std::array<float, 2u> center, unsigned lilyPadQuantity)
  {
    static constexpr unsigned nbrCorners = 16;
    static constexpr float spawnCircleRay = 2.5f;
    static constexpr unsigned randomPosConst = unsigned(2.f * M_PIf32 * 1000.f);
    float initialSpeedDiviser = 550.f / ((lilyPadQuantity > 15 ? static_cast<float>(lilyPadQuantity) : 10.f) / 10.f + 1.f);
    auto &result(createEntity());

    createComponent<FlagComponent>(&result, flag::Type::LilyPad);

    std::array<std::array<float, 2u>, nbrCorners> corners = {};
    float ray = 0.2f + float(rand()) / float(RAND_MAX) / 6.f;

    generateCircle(corners, ray);

    Fixture circle(std::vector<std::array<float, 2u>>{corners.begin(), corners.end()}, 1.0f / (ray * 2.f), 0.f);

    circle.transform.rotation = {1.f, 0.f};
    float angle = float(rand() % randomPosConst) / 1000.f;
    circle.transform.position = {spawnCircleRay * sinf(angle) + center[0],
				 spawnCircleRay * cosf(angle) + center[1]};
    float angleDir = float(rand() % randomPosConst) / 2000.f - M_PIf32 / 2.f;
    std::array<float, 2u> baseSpeedVect{center[0] - circle.transform.position[0], center[1] - circle.transform.position[1]};
    float norm = sqrtf(powf(baseSpeedVect[0], 2) + powf(baseSpeedVect[1], 2));
    circle.speed = {sinf(angleDir + asinf(baseSpeedVect[0] / norm)) / initialSpeedDiviser,
                    cosf(angleDir + acosf(baseSpeedVect[1] / norm)) / initialSpeedDiviser};

    createComponent<FixtureComponent>(&result, circle);
    createComponent<PictureComponent>(&result, ray * 2.15f, ray * 2.15f, display::SpriteType((rand() % 5) + size_t(display::SpriteType::LilyPad01)));
  }
}

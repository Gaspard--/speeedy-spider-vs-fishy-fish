#include "logic/ExplosiveSnakeAI.hpp"
#include "logic/EntityManager.hpp"

namespace logic
{
  void ExplosiveSnakeAi::update(std::vector<Entity *> &entities) noexcept
  {
    timer += 0.01f;
    FixtureComponent *fixtureComponent = entities[0]->get<FixtureComponent>();
    assert(fixtureComponent);

    float amplitude = 0.005f * std::sin(timer);

    std::array<float, 2> rotation({std::cos(amplitude), std::sin(amplitude)});
    fixtureComponent->fixture.rotate(rotation);

    fixtureComponent->fixture.move(fixtureComponent->fixture.transform.rotation * 0.0008f);
  }
}

#include "logic/TaggedIndex.hpp"
#include "logic/FishyComponent.hpp"

namespace logic
{
  FishyComponent::FishyComponent(Entity *e) noexcept
      : SingleEntityComponent<true>(e)
    , maxHPs(80)
    , HPs(maxHPs)
  {}

  FishyComponent::~FishyComponent() noexcept = default;

  void FishyComponent::update(logic::TaggedIndex<FishyComponent>)
  {
  }
}

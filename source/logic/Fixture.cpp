#include "logic/Fixture.hpp"

namespace logic
{

  Fixture::Fixture(std::vector<std::array<float, 2u>> const &cornerPositions, Transform2d centerPosition, Transform2d positionObject)
      : friction(0.0f)
      , timeSpeed(1.0f)
  {
    this->shape.resize(cornerPositions.size());
    std::transform(cornerPositions.begin(), cornerPositions.end(), shape.begin(), [&](std::array<float, 2u> point) {
      return centerPosition.apply(point);
    });
    this->inverseMass = 0;
    this->inverseAngularMass = 0.0f;
    this->transform = positionObject;
    this->speed = {0.0f, 0.0f};
    this->rotation = 0.0f;
    updateBoundingBox();
  }

  Fixture::Fixture(std::vector<std::array<float, 2u>> const &cornerPositions, float inverseDensity, float friction)
    : friction(friction)
    , timeSpeed(1.0f)
  {
    std::array<float, 2u> center{0.0f, 0.0f};
    float signedArea(0.0f);

    for (auto i(0ul); i < cornerPositions.size(); ++i)
      {
	auto nexti((i + 1) % cornerPositions.size());
	auto val(cross(cornerPositions[i], cornerPositions[nexti]));
	for (auto k : std::array<std::size_t, 2u>{0ul, 1ul})
	  center[k] += (cornerPositions[i][k] + cornerPositions[nexti][k]) * val;
	signedArea += val;
      }
    signedArea *= 0.5f;
    center /= 6.0f * signedArea;
    this->shape.resize(cornerPositions.size());
    if (signedArea < 0.0f)
      {
	signedArea *= -1.0f;
	std::transform(cornerPositions.rbegin(), cornerPositions.rend(), this->shape.begin(),
		       [center](auto const &position) { return position - center; });
      }
    else
      {
	std::transform(cornerPositions.begin(), cornerPositions.end(), this->shape.begin(),
		       [center](auto const &position) { return position - center; });
      }
    this->inverseMass = inverseDensity / signedArea;
    float invRotationCoef(0.0f);
    float rotationalCoef(0.0f);
    for (auto i(0ul); i < cornerPositions.size(); ++i)
      {
	auto nexti = (i + 1) % cornerPositions.size();
	auto val(cross(shape[i], shape[nexti]));

	rotationalCoef += val * (claws::length2(shape[i]) + claws::scalar(shape[i], shape[nexti]) + claws::length2(shape[nexti]));
	invRotationCoef += val;
      }
    this->inverseAngularMass = inverseMass * invRotationCoef * 6.0f / rotationalCoef;
    this->transform = Transform2d{center, {1.0f, 0.0f}};
    this->speed = {0.0f, 0.0f};
    this->rotation = 0.0f;
    updateBoundingBox();
  }

  void Fixture::update() noexcept
  {
    if (boundingBox.sleep)
      return;

    // speed += impulsePosition * inverseMass;
    // impulsePosition = {0.0f, 0.0f};
    // rotation += impulseRotation * inverseAngularMass;
    // impulseRotation = 0.0f;
    transform.position += speed;
    transform.rotation *= std::array<float, 2u>{std::cos(rotation), std::sin(rotation)};

    speed *= 1.0f - friction;
    rotation *= 1.0f - friction;
    updateBoundingBox();
    constexpr float const sleepThreshold(0.0000001f);

    boundingBox.sleep = (claws::length2(speed) < sleepThreshold * sleepThreshold && rotation < sleepThreshold);
  }

}

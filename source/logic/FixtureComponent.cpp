#include "logic/TaggedIndex.hpp"
#include "logic/FixtureComponent.hpp"
#include "logic/BoundedVolumeHierarchy.hpp"
#include "logic/EntityManager.hpp"

namespace logic
{
  namespace
  {
    void setShape(Entity *entity, std::vector<std::array<float, 2>> &&shape, logic::BoundingBox boundingBox, bool permanent = false)
    {
      for (size_t i = 0; i < entity->getComponentListSize<CollidableComponent>(); ++i)
	{
	  auto collidable = entity->getComponentListElement<CollidableComponent>(i);

	  if (collidable->compatible(boundingBox))
	    {
	      collidable->setShape(collidable, std::move(shape), boundingBox, permanent);
	      return ;
	    }
	}
      constexpr int collidableCountSoftMax = 1; // soft, because more will be allocated if it isn't enough
      if (entity->getComponentListSize<CollidableComponent>() >= collidableCountSoftMax)
	{
	  for (size_t i = 0; i < entity->getComponentListSize<CollidableComponent>(); ++i)
	    {
	      auto collidable = entity->getComponentListElement<CollidableComponent>(i);

	      if (collidable->available())
		{
		  collidable->setShape(collidable, std::move(shape), boundingBox, permanent);
		  return ;
		}
	    }
	}
      else
	{
	  auto collidable = g_entityManager->createComponent<CollidableComponent>(entity);

	  collidable->setShape(collidable, std::move(shape), boundingBox, permanent);
	}
    }
  }

  void FixtureComponent::update(logic::TaggedIndex<FixtureComponent>)
  {
    fixture.update();
    setShape(entity, std::vector<std::array<float, 2>>(fixture.getShape().begin(), fixture.getShape().end()), fixture.boundingBox);
  }

  CollidableComponent::CollidableComponent(CollidableComponent &&other) noexcept
    : SingleEntityComponent{other}
    , shape(std::move(other.shape))
    , boundedVolumeHierarchyNode(other.boundedVolumeHierarchyNode)
    , used(other.used)
    , permanent(other.permanent)
  {
    other.boundedVolumeHierarchyNode = nullptr;
  }

  void CollidableComponent::update(logic::TaggedIndex<CollidableComponent>) noexcept
  {
    if (!boundedVolumeHierarchyNode)
      return ;
    // if (!used)
    //   {
    // 	g_entityManager->boundedVolumeHierarchy.remove(boundedVolumeHierarchyNode);
    // 	boundedVolumeHierarchyNode = nullptr;
    //   }
    // else
      {
	used = permanent;
      }
  }

  bool CollidableComponent::compatible(logic::BoundingBox const &boundingBox) const noexcept
  {
    if (boundedVolumeHierarchyNode)
      {
	for (int j : {0, 1})
	  if (boundedVolumeHierarchyNode->boundingBox.getMin()[j] > boundingBox.getMin()[j] ||
	      boundedVolumeHierarchyNode->boundingBox.getMax()[j] < boundingBox.getMax()[j])
	    {
	      return false;
	    }
	return true;
      }
    return false;
  }

  void CollidableComponent::setShape(logic::TaggedIndex<CollidableComponent> index, std::vector<std::array<float, 2>> &&shape, logic::BoundingBox boundingBox, bool permanent)
  {
    BoundedVolumeHierarchyNode<logic::TaggedIndex<CollidableComponent>> *hint = nullptr;
    if (boundedVolumeHierarchyNode && !compatible(boundingBox))
      {
	if (boundedVolumeHierarchyNode->parent)
	  hint = boundedVolumeHierarchyNode->parent->parent;
        g_entityManager->boundedVolumeHierarchy.remove(boundedVolumeHierarchyNode);
	boundedVolumeHierarchyNode = nullptr;
      }
    if (!boundedVolumeHierarchyNode)
      {
	for (int j : {0, 1})
	  {
	    boundingBox.min[j] -= 0.03f;
	    boundingBox.max[j] += 0.03f;
	  }
	boundedVolumeHierarchyNode = g_entityManager->boundedVolumeHierarchy.addChild(boundingBox, index, hint);
      }
    used = true;
    this->shape = std::move(shape);
    this->permanent = permanent;
  }

  void CollidableComponent::removeShape() noexcept
  {
    if (boundedVolumeHierarchyNode)
      {
	g_entityManager->boundedVolumeHierarchy.remove(boundedVolumeHierarchyNode);
	boundedVolumeHierarchyNode = nullptr;
      }    
  }

  CollidableComponent::~CollidableComponent() noexcept
  {
    removeShape();
  }

  FixtureComponent::~FixtureComponent() noexcept
  {
    // TODO: remove collidable components
  }

  void swap(CollidableComponent &lh, CollidableComponent &rh) noexcept
  {
    if (&rh == &lh)
      return ;
    using std::swap;
    //TODO
    swap(static_cast<SingleEntityComponent<false> &>(lh), static_cast<SingleEntityComponent<false> &>(rh));
    swap(lh.boundedVolumeHierarchyNode->data, rh.boundedVolumeHierarchyNode->data);
    swap(lh.boundedVolumeHierarchyNode, rh.boundedVolumeHierarchyNode);
    swap(lh.shape, rh.shape);
  }
}

#include "logic/GunComponent.hpp"
#include "logic/ColorComponent.hpp"
#include "logic/TaggedIndex.hpp"
#include "logic/PictureComponent.hpp"
#include "logic/FlagComponent.hpp"
#include "logic/EntityManager.hpp"

#include "display/SpriteType.hpp"

namespace logic
{
  GunComponent::GunComponent(Entity *e, uint32_t cooldown, uint32_t bonus) noexcept
    : SingleEntityComponent<false>(e)
    , cooldown(cooldown)
    , bonus(bonus)
  {
    cooldownTime = cooldown;
    bonusUsed = false;
    firing = true;
  }

  GunComponent::~GunComponent() noexcept = default;

  void GunComponent::update(logic::TaggedIndex<GunComponent>)
  {
    cooldownTime -= !!cooldownTime;
    if (!cooldownTime && (firing || bonusUsed))
      {
	// fire bullet
	std::vector<std::array<float, 2>> circle;
	int edgeCount = 8;

	for (int i = 0; i < edgeCount; ++i)
	  {
	    float angle = float(i) * 3.14159265358979f * 2.0f / float(edgeCount);
	    circle.push_back({{std::cos(angle) * 0.06f, std::sin(angle) * 0.03f}});
	  }

	auto newEntity = &g_entityManager->createEntity();
	auto fixtureComponent = g_entityManager->createComponent<FixtureComponent>(newEntity, Fixture{std::move(circle), 0.001f, 0.0f});

	fixtureComponent->fixture.transform.rotation = entity->get<FixtureComponent>()->fixture.transform.rotation;
	fixtureComponent->fixture.transform.position = entity->get<FixtureComponent>()->fixture.transform.apply(entity->get<PictureComponent>()->offset.position);
	fixtureComponent->fixture.speed = -fixtureComponent->fixture.transform.rotation * 0.01f;

	auto pictureComponsnt = g_entityManager->createComponent<PictureComponent>(newEntity, 0.12f, 0.06f, display::SpriteType::Bullet);	
	if (entity->get<FixtureComponent>()->fixture.rotation > 0.0f)
	  {
	    fixtureComponent->fixture.speed *= -1.0f;
	    pictureComponsnt->displayedWidth *= -1;
	  }

	g_entityManager->createComponent<FlagComponent>(newEntity, flag::Type::Bullet);


	// reset cooldown
	cooldownTime = cooldown;
	bonusUsed = false;
      }
  }

  void GunComponent::fire() noexcept
  {
    if (firing)
      return;
    if (!bonusUsed)
      {
	if (bonus < cooldownTime)
	  cooldownTime -= bonus;
	else
	  cooldownTime = 0;
	bonusUsed = true;
      }
    firing = true;
  }

  void GunComponent::stopFire() noexcept
  {
    firing = false;
  }
}

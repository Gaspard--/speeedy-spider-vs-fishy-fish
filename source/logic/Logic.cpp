#include <cmath>
#include <claws/utils/lambda_ops.hpp>

#include "logic/Logic.hpp"
#include "logic/Input.hpp"
#include "logic/PlayerComponent.hpp"
#include "logic/WaveComponent.hpp"
#include "logic/GunComponent.hpp"
#include "logic/FlagComponent.hpp"
#include "logic/FixtureComponent.hpp"
#include "logic/FishyComponent.hpp"
#include "logic/WaveComponent.hpp"
#include "logic/ColorComponent.hpp"
#include "logic/PictureComponent.hpp"
#include "logic/BubbleComponent.hpp"
#include "logic/SoundHandler.hpp"

namespace logic
{
  Logic::Logic()
  {
  }

  Transform2d Logic::getCamera() const noexcept
  {
    return camera;
  }


  void Logic::handleEvents(input::Input &input)
  {
    enum {left, right, swap, nothing} action = nothing;
    while (auto e = input.pollEvent()) {
      if (auto k = std::get_if<input::Key>(&e.data)) {
        if (k->action == GLFW_PRESS) {
          switch (k->key) {
            case GLFW_KEY_LEFT:
              action = left;
              break;
            case GLFW_KEY_RIGHT:
              action = right;
              break;
//            case GLFW_KEY_UP:
//              action = swap;
//              break;
	    case GLFW_KEY_SPACE:
	      if (gameOver || win)
		{
		  intro = true;
		  entityManager.clear();
		  animFrame = 0;
		} else {
	        action = swap;
	      }
	      break;
            case GLFW_KEY_R:
	      intro = false;
	      gameOver = false;
	      entityManager.init();
	      lilyPadQuantity = 0;
	      break;
	    default:
              action = nothing;
              break;
          }
        }
      }
    }
    g_entityManager->forEachComponent<PlayerComponent>([&](PlayerComponent const &playerComponent) noexcept {
      Entity *e = playerComponent.entity;
      auto *fixtureComponent = e->get<FixtureComponent>();
      auto *pictureComponent = e->get<PictureComponent>();

      assert(fixtureComponent && pictureComponent);
      bool fire = false;

      if (action == swap)
	{
	  fixtureComponent->fixture.rotation *= -1.f;
	  pictureComponent->displayedWidth *= -1;
	  fire = true;
	}
      else if (action == left && fixtureComponent->fixture.rotation < 0)
	{
	  fixtureComponent->fixture.rotation *= -1.f;
	  pictureComponent->displayedWidth *= -1;
	  fire = true;
	}
      else if (action == right && fixtureComponent->fixture.rotation > 0)
	{
	  fixtureComponent->fixture.rotation *= -1.f;
	  pictureComponent->displayedWidth *= -1;
	  fire = true;
	}
      fixtureComponent->fixture.wakeUp();
      if (fire)
	e->forComponentList<GunComponent>([&](GunComponent &gunComponent)
					  {
					    gunComponent.fire();
					  });
    });
  }

  void Logic::tick(input::Input &input)
  {
    camera.position = {0.0f, 0.0f};
    camera.rotation = {1.0f, 0.0f};

    handleEvents(input);
    if (intro)
      {
	SoundHandler::getInstance().setMainVolume(30.0f);
	SoundHandler::getInstance().setSecondaryVolume(0.0f);

	++animFrame;
	if (animFrame == 51 * 40)
	  {
	    intro = false;
	    gameOver = false;
	    win = false;
	    entityManager.init();
	    lilyPadQuantity = 0;
	    screenShake = 0.0f;
	  }
	return ;
      }
    if (win)
      {
	SoundHandler::getInstance().setMainVolume(30.0f);
	SoundHandler::getInstance().setSecondaryVolume(0.0f);

	++animFrame;
	return;
      }
    if (gameOver)
      {
	SoundHandler::getInstance().setMainVolume(0.0f);
	SoundHandler::getInstance().setSecondaryVolume(30.0f);

	++animFrame;
	return;
      }
    gameOver = true;
    entityManager.forEachComponent<PlayerComponent>([&](PlayerComponent &) noexcept
						    {
						      gameOver = false;
						    });
    if (gameOver)
      {
	animFrame = 0;
	entityManager.clear();
	return;
      }
    win = true;
    entityManager.forEachComponent<FishyComponent>([&](FishyComponent &fishyComponent) noexcept
						   {
						     win = fishyComponent.HPs == 0;
						   });
    if (win)
      {
	animFrame = 0;
	entityManager.clear();
	return;
      }
    if (++nbFrameSinceLastLilypad > 60 + std::min<uint32_t>(lilyPadQuantity * 10, 120)) {
      entityManager.spawnLilypad((*(entityManager.fixtureComponents.begin())).fixture.transform.position, lilyPadQuantity);
      lilyPadQuantity++;
      nbFrameSinceLastLilypad = 0;
      entityManager.forEachComponent<FlagComponent>([&](FlagComponent &component) noexcept {
        if (!component.dontRemove) {
          std::array<float, 2u> position = component.entity->get<FixtureComponent>()->fixture.transform.position;
          if (powf((*(entityManager.fixtureComponents.begin())).fixture.transform.position[0] - position[0], 2)
	      + powf((*(entityManager.fixtureComponents.begin())).fixture.transform.position[1] - position[1], 2) > 9.f) {
	    if (component.type == flag::Type::LilyPad)
	      lilyPadQuantity--;
            component.entity->markForRemoval();
          }
        }
      });
    }
    entityManager.forEachComponent<FixtureComponent>([&](FixtureComponent const &fixtureComponent) noexcept {
						       if (rand() % 480 == 0)
							 {
							   std::vector<std::array<float, 2>> circle;
							   int edgeCount = 10;

							   for (int i = 0; i < edgeCount; ++i)
							     {
							       float angle = float(i) * 3.14159265358979f * 2.0f / float(edgeCount);
							       circle.push_back({{std::cos(angle) * 0.05f, std::sin(angle) * 0.05f}});
							     }
							   auto newEntity = &g_entityManager->createEntity();
							   auto position = fixtureComponent.fixture.transform.position;
							   auto newFixtureComponent = g_entityManager->createComponent<FixtureComponent>(newEntity, Fixture{std::move(circle), 0.001f, 0.0f});

							   newFixtureComponent->fixture.transform.position = position;
							   newFixtureComponent->fixture.speed[0] = float(rand() % 10) * 0.1f * 0.001f;
							   newFixtureComponent->fixture.speed[1] = float(rand() % 10) * 0.1f * 0.001f;

							   std::array<float, 4u> color =
							     {
							      1.0f - float(rand() % 5) * 0.1f,
							      1.0f - float(rand() % 5) * 0.1f,
							      1.0f - float(rand() % 5) * 0.1f,
							      1.0f
							     };
							   g_entityManager->createComponent<ColorComponent>(newEntity, color);
							   g_entityManager->createComponent<PictureComponent>(newEntity, 0.1f, 0.1f, display::SpriteType::Wave, Transform2d{{0.0f, 0.0f}, {1.0f, 0.0f}}, color);
							   g_entityManager->createComponent<BubbleComponent>(newEntity);
							 }
						     });
    entityManager.forEachComponent<FishyComponent>([&](FishyComponent &fishyTheFish) noexcept {
      Fixture &fixFish = fishyTheFish.entity->get<FixtureComponent>()->fixture;

      fishyTheFish.entity->get<PictureComponent>()->mask[0] += 0.1f * (1.0f - float(fishyTheFish.phase) * 0.2f);
      fishyTheFish.entity->get<PictureComponent>()->mask[1] += 0.1f * (1.0f - float(fishyTheFish.phase) * 0.2f);
      fishyTheFish.entity->get<PictureComponent>()->mask[2] += 0.1f * (1.0f - float(fishyTheFish.phase) * 0.2f);
      fishyTheFish.entity->get<PictureComponent>()->mask[0] *= 0.9f;
      fishyTheFish.entity->get<PictureComponent>()->mask[1] *= 0.9f;
      fishyTheFish.entity->get<PictureComponent>()->mask[2] *= 0.9f;
      if (fishyTheFish.phase < (fishyTheFish.maxHPs - fishyTheFish.HPs) * 4 / fishyTheFish.maxHPs)
	{
	  ++fishyTheFish.phase;
	  fishyTheFish.suck = 6 + fishyTheFish.phase * 5;
	  {
	    screenShake += 5.0f * float(2 + fishyTheFish.phase) * 0.2f;
	    std::vector<std::array<float, 2>> circle;
	    int edgeCount = 30;

	    for (int i = 0; i < edgeCount; ++i)
	      {
		float angle = float(i) * 3.14159265358979f * 2.0f / float(edgeCount);
		circle.push_back({{std::cos(angle) * 0.05f, std::sin(angle) * 0.05f}});
	      }
	    auto newEntity = &g_entityManager->createEntity();
	    auto fixtureComponent = g_entityManager->createComponent<FixtureComponent>(newEntity, Fixture{std::move(circle), 0.001f, 0.0f});

	    fixtureComponent->fixture.transform.position = fixFish.transform.position;

	    g_entityManager->createComponent<ColorComponent>(newEntity, std::array<float, 4u>{1.0f, 1.0f, 1.0f, 1.0f});
	    g_entityManager->createComponent<PictureComponent>(newEntity, 0.1f, 0.1f, display::SpriteType::Wave);
	    g_entityManager->createComponent<WaveComponent>(newEntity, 0.05f, 0.8f, 9.0f);
	  }
	}
      if (fishyTheFish.suck)
	{
	  fishyTheFish.attack = false;
	  screenShake += 0.02f * float(2 + fishyTheFish.phase) * 0.2f;
	  entityManager.forEachComponent<FlagComponent>([&](FlagComponent &flagComponent) noexcept
	   {
	     FixtureComponent *fixtureComponent = flagComponent.entity->get<FixtureComponent>();

	     if (flagComponent.type == flag::Type::LilyPad)
	       {
		 auto diff = fixFish.transform.position - fixtureComponent->fixture.transform.position;
		 fixtureComponent->fixture.speed *= 0.99f;
		 fixtureComponent->fixture.rotation *= 0.96f;
		 fixtureComponent->fixture.speed += diff / claws::length2(diff) * 0.00001f * (float(fishyTheFish.phase) + 1.5f);
	       }
	   });
	  entityManager.forEachComponent<BubbleComponent>([&](BubbleComponent &bubbleComponent) noexcept
	   {
	     FixtureComponent *fixtureComponent = bubbleComponent.entity->get<FixtureComponent>();

	     auto diff = fixFish.transform.position - fixtureComponent->fixture.transform.position;
	     fixtureComponent->fixture.speed += diff / claws::length2(diff) * 0.00004f * (float(fishyTheFish.phase) + 1.5f);
	   });
	}
      entityManager.forEachComponent<PlayerComponent>([&](PlayerComponent &speeedyTheSpider) noexcept {
	std::array<float, 2u> posSpid = speeedyTheSpider.entity->get<FixtureComponent>()->fixture.transform.apply(speeedyTheSpider.entity->get<PictureComponent>()->offset.position);
	{
	  auto diff = posSpid - fixFish.transform.position;

	  SoundHandler::getInstance().setMainVolume(30.0f);
	  SoundHandler::getInstance().setSecondaryVolume(std::min(30.0f, 8.0f * float(fishyTheFish.phase + 2) / std::sqrt(claws::length2(diff))));
	}
	for (int i = 0; i < 10; ++i)
	  {
	    posSpid += speeedyTheSpider.entity->get<FixtureComponent>()->fixture.getSpeedAt(posSpid) * 6.0f; // anticipate a few frames in the future
	  }

	using logic::operator*;
	using logic::operator+;

	auto diff = posSpid - fixFish.transform.position;
	auto dir = diff / std::sqrt(claws::length2(diff));

	if (fishyTheFish.suck)
	  {
	    fixFish.rotation = 0.0f;
	    fixFish.transform.rotation = dir;
	    fixFish.speed *= 0.8f;
	    fixFish.speed += dir * 0.00025f * 0.2f * float(2 + fishyTheFish.phase) * 0.2f;
	    return;
	  }
	if (claws::length2(diff) > 1.0f)
	  {
	    fixFish.rotation *= 0.98f;
	    fixFish.rotation += logic::cross(fixFish.transform.rotation, dir) * 0.01f * 0.03f;
	  }
	else
	  {
	    fixFish.rotation *= 0.96f;
	  }
	if (++nbFrameSinceLastFishyUpdate > 20) {
	  nbFrameSinceLastFishyUpdate = 0;

	  if ((claws::length2(diff) > 1.0f && !fishyTheFish.attack) || claws::length2(diff) > 1.2f)
	    {
	      fishyTheFish.attack = false;
	      fixFish.speed *= 0.8f;
	      fixFish.speed += (fixFish.transform.rotation * std::max(claws::scalar(fixFish.transform.rotation, dir), 0.0f)) * 0.004f * 0.2f;
	    }
	  else
	    {
	      if (!fishyTheFish.attack)
		{
		  screenShake += float(2 + fishyTheFish.phase) * 0.2f * 1.5f;
		  fixFish.speed = -dir * 0.002f * float(2 + fishyTheFish.phase) * 0.2f;
		  fishyTheFish.attack = true;

		  std::vector<std::array<float, 2>> circle;
		  int edgeCount = 30;

		  for (int i = 0; i < edgeCount; ++i)
		    {
		      float angle = float(i) * 3.14159265358979f * 2.0f / float(edgeCount);
		      circle.push_back({{std::cos(angle) * 0.05f, std::sin(angle) * 0.05f}});
		    }
		  
		  auto newEntity = &g_entityManager->createEntity();
		  auto fixtureComponent = g_entityManager->createComponent<FixtureComponent>(newEntity, Fixture{std::move(circle), 0.001f, 0.0f});
		  
		  fixtureComponent->fixture.transform.position = fixFish.transform.position;
		  
		  g_entityManager->createComponent<ColorComponent>(newEntity, std::array<float, 4u>{1.0f, 1.0f, 1.0f, 1.0f});
		  g_entityManager->createComponent<PictureComponent>(newEntity, 0.1f, 0.1f, display::SpriteType::Wave);
		  g_entityManager->createComponent<WaveComponent>(newEntity, 0.05f, 0.4f, 9.0f);
		}
	      fixFish.speed += fixFish.transform.rotation * 0.004f * (float(2 + fishyTheFish.phase) * 0.2f) * 0.2f;
	    }
	}
	fixFish.wakeUp();
      });
    });
    entityManager.update();
    entityManager.checkCollisions();
    // for (auto &ai : Ais)
    //   ai->update(entityManager);
    entityManager.removeOld();

    {
      entityManager.forEachComponent<PlayerComponent>
	([&](PlayerComponent const &speeedyTheSpider) noexcept {
	   using logic::operator*;
	   using logic::operator+;
	   auto entity(speeedyTheSpider.entity);
      

	   camera.position = (entity->get<FixtureComponent>()->fixture.transform.apply(entity->get<PictureComponent>()->offset.position)
			      + entity->get<FixtureComponent>()->fixture.transform.position) * 0.5f;
	 });
      camera = -camera;
      for (size_t i = 0; i != 2; ++i)
	{
	  camera.position[i] += screenShake * (float(rand() % 1000) * 0.001f - 0.5f) * 0.1f;
	  camera.rotation[i] += screenShake * (float(rand() % 1000) * 0.001f) * 0.01f;
	}
      screenShake *= 0.9f;
    }
  }
}

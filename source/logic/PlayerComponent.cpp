#include "logic/TaggedIndex.hpp"
#include "logic/PlayerComponent.hpp"

namespace logic
{
  PlayerComponent::PlayerComponent(Entity *e) noexcept
    : SingleEntityComponent<true>(e)
  {}

  PlayerComponent::~PlayerComponent() noexcept = default;

  void PlayerComponent::update(logic::TaggedIndex<PlayerComponent>)
  {
  }
}

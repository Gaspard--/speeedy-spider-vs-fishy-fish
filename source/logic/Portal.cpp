#include "logic/Portal.hpp"
#include "logic/EntityManager.hpp"

namespace logic
{
  Portal::Portal(std::array<std::array<float, 2>, 2> const &shape) noexcept
    : fixture(shape)
    , otherEnd(-1)
  {
  }

  void Portal::setLink(Portal::IndexType otherEnd) noexcept
  {
    this->otherEnd = otherEnd;
  }

  logic::NotMovingFixture<std::array<std::array<float, 2>, 2>> const Portal::getFixture() const noexcept
  {
    return fixture;
  }

  Portal const Portal::getOtherPortal(EntityManager const &entityManager) const noexcept
  {
    assert(otherEnd.data != -1);
    return entityManager[otherEnd];
  }
}

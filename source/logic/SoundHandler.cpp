#include <memory>
#include "logic/SoundHandler.hpp"

namespace logic
{
  std::unique_ptr<SoundHandler> SoundHandler::_instance(nullptr);

  SoundHandler::SoundHandler() {}

  void SoundHandler::initSoundHandler(std::string const &mainMusic, std::string const &secondMusic)
  {
    _instance.reset(new SoundHandler());
    if (!_instance->mainMusic.openFromFile(mainMusic))
      throw ("main music not charged");
    if (!_instance->secondMusic.openFromFile(secondMusic))
      throw ("second music not charged");
  }

  SoundHandler &SoundHandler::getInstance()
  {
    return *_instance;
  }

  void SoundHandler::destroySoundHandler()
  {
    _instance.reset(nullptr);
  }

  void SoundHandler::launchMainMusic()
  {
    mainMusic.setLoop(true);
    mainMusic.setVolume(30);
    mainMusic.play();
  }

  void SoundHandler::launchSecondaryMusic()
  {
    secondMusic.setLoop(true);
    secondMusic.setVolume(0);
    secondMusic.play();
  }

  void SoundHandler::setMainVolume(float volume)
  {
    mainMusic.setVolume(volume);
  }

  void SoundHandler::setSecondaryVolume(float volume)
  {
    secondMusic.setVolume(volume);
  }

  void SoundHandler::setMainPitch(float pitch)
  {
    sfxPitch = pitch;
    mainMusic.setPitch(pitch);
  }

  void SoundHandler::setSecondaryPitch(float pitch)
  {
    sfxPitch = pitch;
    secondMusic.setPitch(pitch);
  }
}

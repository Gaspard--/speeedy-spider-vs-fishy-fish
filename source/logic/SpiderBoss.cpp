#include "logic/SpiderBoss.hpp"
#include "logic/EntityManager.hpp"
#include "logic/Components.hpp"
#include "logic/ColorComponent.hpp"

namespace logic
{
  struct AIComponent;

  struct Half
  {
    unsigned int fang;
    std::array<unsigned int, 3u> legs;

    constexpr Half(unsigned int index)
    : fang(index)
      , legs{index + 1, index + 2, index + 3}
    {
    }

    constexpr unsigned int next() const noexcept
    {
      return legs.back() + 1;
    }
  };

  namespace spiderInfo
  {
    static constexpr unsigned int body{0u};
    static constexpr unsigned int belly{body + 1};

    static constexpr std::array<Half, 2u> halves{ []() {
						    Half first(belly + 1);
						    Half second(first.next());
						    return decltype(halves){first, second};
						  }()};
    static constexpr unsigned int entityCount{halves.back().next()};
  };

  class SpiderBossAI : public logic::AI
  {
  public:
    struct Spin
    {
      float dir;
      unsigned int time;
    };

    struct Dash
    {
      unsigned int time;
    };

    std::variant<Spin, Dash> state{Spin{1.0f, 30}};

    virtual void update(std::vector<Entity *> &entities) override;
  };

  void makeSpider(std::array<float, 2u> position, float scale)
  {
    EntityManager &entityManager(*g_entityManager);
    std::vector<Entity *> entities;

    entities.resize(spiderInfo::entityCount);

    entities[spiderInfo::body] = &entityManager.createHurtBox({{
								{0.2f * scale, 0.2f * scale},
								{-0.2f * scale, 0.2f * scale},
								{-0.2f * scale, -0.2f * scale},
								{0.2f * scale, -0.2f * scale}},
							       2.0f,
							       0.003f},
      {0.013f, 0.004f, 0.013f, 1.0f},
      uint32_t(1000 * scale));
    entities[spiderInfo::belly] = &entityManager.createHurtBox({{
						     {0.4f * scale, 0.2f * scale},
						     {0.2f * scale, 0.4f * scale},
						     {-0.2f * scale, 0.4f * scale},
						     {-0.4f * scale, 0.2f * scale},
						     {-0.4f * scale, -0.2f * scale},
						     {-0.2f * scale, -0.4f * scale},
						     {0.2f * scale, -0.4f * scale},
						     {0.4f * scale, -0.2f * scale}},
						    10.0f,
						    0.01f},
      {0.013f, 0.008f, 0.013f, 1.0f},
      uint32_t(1000 * scale));

    entities[spiderInfo::belly]->get<FixtureComponent>()->fixture.transform.position += std::array<float, 2u>{0.0f * scale, 0.6f * scale};
    for (std::size_t j(0ul); j != 2; ++j)
      {
	auto side(std::array<float, 2u>{-1.0f, 1.0f}[j]);

	// TODO
	entityManager.createComponent<JointComponent>(entities[spiderInfo::belly], entities[spiderInfo::body], std::array<float, 2>{0.2f * scale * side, -0.1f * scale}, 0.1f * scale /*, 0.01f * scale */);
	for (std::size_t i(0ul); i < 3; ++i)
	  {
	    entities[spiderInfo::halves[j].legs[i]] = &entityManager.createHurtBox({{
									 {0.2f * scale * side, (-0.2f + 0.4f / 3.0f * static_cast<float>(i)) * scale},
									 {0.2f * scale * side, (-0.2f + 0.4f / 3.0f * static_cast<float>(i + 1)) * scale},
									 {0.5f * scale * side, (-0.2f + 0.8f / 3.0f * static_cast<float>(i)) * scale},
									 {1.2f * scale * side, (-0.6f + 1.2f / 3.0f * static_cast<float>(i)) * scale}},
									10.0f},
	      {0.0f, 0.0f, 0.03f, 1.0f},
	      uint32_t(600 * scale));
	    entityManager.createComponent<JointComponent>(entities[spiderInfo::halves[j].legs[i]], entities[spiderInfo::body], std::array<float, 2>{0.2f * scale * side, (-0.2f + 0.4f / 3.0f * static_cast<float>(i)) * scale}, 0.02f * scale /*, 0.001f * scale */);
	    entityManager.createComponent<JointComponent>(entities[spiderInfo::halves[j].legs[i]], entities[spiderInfo::body], std::array<float, 2>{0.2f * scale * side, (-0.2f + 0.4f / 3.0f * static_cast<float>(i + 1)) * scale}, 0.02f * scale /*, 0.001f * scale */);
	  }
	{
	  entities[spiderInfo::halves[j].fang] = &entityManager.createHurtBox({{
								    {0.2f * scale * side, -0.2f * scale},
								    {0.21f * scale * side, -0.4f * scale},
								    {0.15f * scale * side, -0.6f * scale},
								    {0.0f * scale, -0.2f * scale}},
								   1.0f},
	    {0.01f, 0.0f, 0.003f, 20.0f},
	    uint32_t(1000 * scale));

	  entityManager.createComponent<JointComponent>(entities[spiderInfo::halves[j].fang], entities[spiderInfo::body], std::array<float, 2>{0.2f * scale * side, -0.2f * scale}, 0.015f * scale /*, 0.01f * scale */);
	  entityManager.createComponent<JointComponent>(entities[spiderInfo::halves[j].fang], entities[spiderInfo::body], std::array<float, 2>{0.0f * scale, -0.2f * scale}, 0.015f * scale /*, 0.01f * scale */);
	}
      }
    auto centerPosition = entities[spiderInfo::body]->get<FixtureComponent>()->fixture.transform.position;
    for (auto *entity : entities)
      {
	entity->get<FixtureComponent>()->fixture.transform.position += position - centerPosition;
      }
    entityManager.createComponent<AIComponent>(std::move(entities), std::make_unique<SpiderBossAI>());
  }

  void SpiderBossAI::update(std::vector<Entity *> &entities)
  {
    std::array<float, 2u> target({0.0f, -0.0f});
    using claws::lambda_ops::operator+;
    using claws::lambda_ops::operator*;
    using logic::operator+;
    using logic::operator*;

    // if (!entities.operator[](spiderInfo::body).hp)
    //   return ;
    // for (std::size_t j(0ul); j != 2; ++j)
    //   for (std::size_t i(0ul); i < 3; ++i)
    // 	if (!entities.operator[](spiderInfo::halves[j].legs[i]).joints.empty())
    // 	  goto ok;

    // entities.operator[](spiderInfo::body).hp = 0;
    // return ;
  ok:
    auto *bodyFixtureComponent = entities[spiderInfo::body]->get<FixtureComponent>();
    std::visit([&](Spin &spin) noexcept {
		 for (std::size_t j(0ul); j != 2; ++j)
		   {
		     auto side(std::array<float, 2u>{-1.0f, 1.0f}[j]);
		     for (std::size_t i(0ul); i < 3; ++i)
		       if (entities.operator[](spiderInfo::halves[j].legs[i]))
			 {
			   auto &leg(entities.operator[](spiderInfo::halves[j].legs[i]));
			   auto *legFixtureComponent = leg->get<FixtureComponent>();

			   legFixtureComponent->fixture.move((legFixtureComponent->fixture.transform.rotation * side) * 0.0001f);
			 }
		   }

		 bodyFixtureComponent->fixture.rotation += 0.01f * spin.dir;
		 if (spin.time)
		   --spin.time;
		 else if (claws::scalar((target - bodyFixtureComponent->fixture.transform.position), octogonal(bodyFixtureComponent->fixture.transform.rotation)) < 0)
		   state = Dash{160};
	       } +
      [&](Dash & dash) noexcept {
	for (std::size_t j(0ul); j != 2; ++j)
	  if (entities.operator[](spiderInfo::halves[j].fang))
	    {
	      auto *fangFixtureComponent = entities[spiderInfo::halves[j].fang]->get<FixtureComponent>();
	      auto diff((target - fangFixtureComponent->fixture.transform.position));

	      diff /= std::sqrt(claws::length2(diff));
	      fangFixtureComponent->fixture.move(diff * 0.001f);
	    }
	auto diff((target - bodyFixtureComponent->fixture.transform.position));

	diff /= std::sqrt(claws::length2(diff));
	bodyFixtureComponent->fixture.move(diff * 0.001f);

	if (dash.time)
	  --dash.time;
	else
	  state = Spin{static_cast<float>(((claws::scalar((target - bodyFixtureComponent->fixture.transform.position), bodyFixtureComponent->fixture.transform.rotation) > 0) * 2) - 1), 240};
      },
      state);
  }
}

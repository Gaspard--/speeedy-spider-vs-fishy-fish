#include "logic/TaggedIndex.hpp"
#include "logic/WaveComponent.hpp"
#include "logic/FixtureComponent.hpp"
#include "logic/PictureComponent.hpp"
#include "logic/ColorComponent.hpp"
#include "logic/EntityManager.hpp"

namespace logic
{
  WaveComponent::WaveComponent(Entity *e, float size, float speed, float strength) noexcept
    : SingleEntityComponent<true>(e)
    , size(size)
    , speed(speed)
    , strength(strength)
  {}

  WaveComponent::~WaveComponent() noexcept = default;

  void WaveComponent::update(logic::TaggedIndex<WaveComponent>)
  {
    FixtureComponent *fixtureComponent = entity->get<FixtureComponent>();
    assert(fixtureComponent);

    float ratio = (speed + size) / size;

    for (auto &corner : fixtureComponent->fixture.shape)
      corner *= ratio;
    if (PictureComponent *pictureCompoent = entity->get<PictureComponent>())
      {
	pictureCompoent->displayedWidth *= ratio;
	pictureCompoent->displayedHeight *= ratio;
      }
    size += speed;
    speed /= ratio * ratio;
    fixtureComponent->fixture.wakeUp();
    if (speed < 0.001f)
      {
	entity->markForRemoval();
      }
    if (ColorComponent *colorComponent = entity->get<ColorComponent>())
      colorComponent->color[3] = std::min(speed * 300.0f, 1.0f);
    if (PictureComponent *pictureComponent = entity->get<PictureComponent>())
      pictureComponent->mask[3] = std::min(speed * 300.0f, 1.0f);
  }

  void WaveComponent::apply(FixtureComponent *targetFixtureComponent)
  {
    FixtureComponent *fixtureComponent = entity->get<FixtureComponent>();
    assert(fixtureComponent);
    auto diff(targetFixtureComponent->fixture.transform.position - fixtureComponent->fixture.transform.position);
    
    targetFixtureComponent->fixture.speed += diff / std::sqrt(claws::length2(diff)) * 0.001f * speed;
  }
}

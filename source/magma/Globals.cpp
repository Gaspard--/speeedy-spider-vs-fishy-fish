#include "magma/Globals.hpp"

namespace magma
{
  MagmaState &getMagmaState() noexcept
  {
    thread_local MagmaState state;

    return state;
  }

  vk::Device &getDevice() noexcept
  {
    return getMagmaState().device;
  }

  vk::PhysicalDevice &getPhysicalDevice() noexcept
  {
    return getMagmaState().physicalDevice;
  }
}
